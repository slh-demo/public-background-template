<?php

namespace app\common\taglib;

use app\common\model\ImageModel;
use app\common\model\UserModel;
use app\common\model\WebMenuModel;
use think\template\TagLib;

class SelfTag extends TagLib
{
    /**
     * 定义标签列表
     */
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'image' => ['attr' => 'id,type,name,limit', 'close' => 1],
        'menu'  => ['attr' => '', 'close' => 0],
        'multi' => ['attr' => 'id,name,limit,table,where,order,viewModel', 'close' => 1],
        'multifun' => ['attr' => 'name,funName', 'close' => 1],
        'multidetail' => ['attr' => 'id,name,varName,table,where,order,viewModel', 'close' => 0],
        'multilist' => ['attr' => 'id,name,limit,table,where,order,viewModel', 'close' => 0],
    ];



    /**
     * 图片标签
     * 这是一个非闭合标签的简单演示
     */
    public function tagImage($tag, $content)
    {
        $limit = empty($tag['limit']) ? null : $tag['limit'];
        $type = empty($tag['type']) ? 0 : $tag['type']; // 这个type目的是为了区分类型，一般来源是数据库
        $name = empty($tag['name']) ? '__LIST__' : $tag['name']; //变量名称

        //查询数据
        $id = empty($tag['id']) ? 'vo' : $tag['id']; // name是必填项，这里不做判断了
        $parse = '<?php ';
        $parse .= '$' . $name . ' = \app\common\model\ImageModel::where(["status"=>1])->where(["type"=>' . $type . '])->whereNull("delete_time")->order("sort asc")->paginate(' . $limit . ');';
        $parse .= ' ?>';
        $parse .= '{volist name="' . $name . '" id="' . $id . '"}';
        $parse .= $content;
        $parse .= '{/volist}';
        return $parse;
    }

    //获取菜单栏
    public function tagMenu($tag, $content)
    {
        $name = empty($tag['name']) ? '__LIST__' : $tag['name']; //变量名称
        $navName = empty($tag['navName']) ? '__LIST__' : $tag['navName']; //变量名称
        $id = empty($tag['id']) ? 'vo' : $tag['id']; // name是必填项，这里不做判断了

//        $active_url = app()->request->baseUrl();
        $parse = '<?php ';
        $parse .= 'if(!empty($init_up_cate_url)) { $active_url = $init_up_cate_url; } else{ $active_url = app()->request->baseUrl();}';
        $parse .= '$web_menu_list = [];';
        $parse .= '$web_menu_name = "";';
        $parse .= '$web_menu_mate_kw = "";';
        $parse .= '$web_menu_mate_des = "";';
        $parse .= '$web_menu_img = "";';
        $parse .= '$'.$navName.' = \app\common\model\WebMenuModel::getAllData(["status"=>1,"active_url"=>"$active_url"])->each(function($item)use(&$web_menu_name,&$web_menu_list,&$web_menu_mate_kw,&$web_menu_mate_des,&$web_menu_img){
          $linkChild = $item["linkChild"]??[];
          if($item["is_active"]){ 
            $web_menu_name = trim($item["name"]??"");
            $web_menu_mate_kw = trim($item["meta_kw"]??"");
            $web_menu_mate_des = trim($item["meta_des"]??"");
            $web_menu_img = trim($item["img"]??"");
            $web_menu_list = $linkChild;
          }
          foreach($linkChild as $child){
              if($child["is_active"]){ 
                $web_menu_name = trim($child["name"]??"");
                $web_menu_mate_kw = trim($child["meta_kw"]??"");
                $web_menu_mate_des = trim($child["meta_des"]??"");
                $web_menu_img = trim($item["img"]??"");
              }
          }
          
        });';
        $parse .= ' ?>';
//        $parse .= '{volist name="' . $name . '"  id="'.$id.'" }';
//        $parse .= $content;
//        $parse .= '{/volist}';
        return $parse;
    }





    /**
     * 标签
     * 这是一个非闭合标签的简单演示
     */
    public function tagMultiFun($tag, $content)
    {

        $funName = empty($tag['funName']) ? '__LIST__' : $tag['funName']; //变量名称
        $name = empty($tag['name']) ? '__LIST__' : $tag['name']; //变量名称
        //查询数据
        $id = empty($tag['id']) ? 'vo' : $tag['id']; // name是必填项，这里不做判断了
        $parse = '<?php ';
        $parse .= '$' . $name . ' =' . $funName . '();';
        $parse .= ' ?>';
        $parse .= '{volist name="' . $name . '" id="' . $id . '"}';
        $parse .= $content;
        $parse .= '{/volist}';
        return $parse;
    }
    /**
     * 标签
     * 这是一个非闭合标签的简单演示
     */
    public function tagMulti($tag, $content)
    {
        $limit = empty($tag['limit']) ? null : $tag['limit'];
        $type = empty($tag['type']) ? 0 : $tag['type']; // 这个type目的是为了区分类型，一般来源是数据库
        $table = empty($tag['table']) ? '' : $tag['table']; //数据库名
        $viewModel = empty($tag['viewModel']) ? "False" : $tag['viewModel']; //数据库名
        $where = empty($tag['where']) ? '' : $tag['where'];  //查询条件
        $order = empty($tag['order']) ? '' : $tag['order'];  //查询条件
        if (empty($viewModel) && empty($table)) {
            throw new \Exception('请填写表名');
        }
//        dump($viewModel);exit;
        $name = empty($tag['name']) ? '__LIST__' : $tag['name']; //变量名称
        //查询数据
        $id = empty($tag['id']) ? 'vo' : $tag['id']; // name是必填项，这里不做判断了
        $parse = '<?php ';
        $parse .= 'if ( !empty( "' . $viewModel . '"!="False" ) ) {';
        $parse .= ' $' . $name . ' = ' . $viewModel . '::getPageData('.$where.'); ';
        $parse .= ' } else { ';
        $parse .= '$' . $name . ' = \think\facade\Db::table("' . $table . '")->where(' . $where . ')->whereNull("delete_time")->order("' . $order . '")->paginate(' . $limit . ');';
        $parse .= '}';
        $parse .= ' ?>';
        $parse .= '{volist name="' . $name . '" id="' . $id . '"}';
        $parse .= $content;
        $parse .= '{/volist}';
        return $parse;
    }

    /**
     * 标签
     * 这是一个非闭合标签的简单演示
     */
    public function tagMultiList($tag, $content)
    {
        $varPage = empty($tag['varPage']) ? "page" : $tag['varPage'];
        $limit = empty($tag['limit']) ? '15' : $tag['limit'];
        $type = empty($tag['type']) ? 0 : $tag['type']; // 这个type目的是为了区分类型，一般来源是数据库
        $table = empty($tag['table']) ? '' : $tag['table']; //数据库名
        $where = empty($tag['where']) ? '' : $tag['where'];  //查询条件
        $order = empty($tag['order']) ? '' : $tag['order'];  //查询条件
        $viewModel = empty($tag['viewModel']) ? "False" : $tag['viewModel']; //数据库名
        if (empty($viewModel) && empty($table)) {
            throw new \Exception('请填写表名');
        }


        $name = empty($tag['name']) ? '__LIST__' : $tag['name']; //变量名称
        //查询数据
        $id = empty($tag['id']) ? 'vo' : $tag['id']; // name是必填项，这里不做判断了
        $parse = '<?php ';
        $parse .= 'if ( !empty( "' . $viewModel . '"!="False" ) ) {';
        $parse .= ' $' . $name . ' = ' . $viewModel . '::getPageData('.$where.'); ';
        $parse .= ' } else { ';
        $parse .= '$' . $name . ' = \think\facade\Db::table("' . $table . '")->where(' . $where . ')->whereNull("delete_time")->order("' . $order . '")->paginate(["list_rows"=>' . $limit . ',"var_page"=>"' . $varPage . '"]);';
        $parse .= '}';
        $parse .= ' ?>';

        return $parse;
    }


    /**
     * 详情展示
     * */
    public function tagMultiDetail($tag, $content)
    {
        //        dump($tag['name']);exit;
        $table = empty($tag['table']) ? '' : $tag['table']; //数据库名
        $varName = empty($tag['varName']) ? '' : $tag['varName']; // name是必填项，这里不做判断了

        $where = empty($tag['where']) ? '' : $tag['where'];  //查询条件
        $order = empty($tag['order']) ? '' : $tag['order'];  //查询条件
        $viewModel = empty($tag['viewModel']) ? "False" : $tag['viewModel']; //数据库名

        if (empty($viewModel) && empty($table)) {
            throw new \Exception('请填写表名');
        }
        if (empty($varName)) {
            throw new \Exception('请填写变量名');
        }

        $parse = '<?php ';
        $parse .= 'if ( !empty( "' . $viewModel . '"!="False" ) ) {';
        $parse .= ' $' . $varName . ' = null; ';
        $parse .=  $viewModel . '::getPageData('.$where.')->each(function($item)use('.' &$' . $varName . '){ '.' $' . $varName . '=$item; }); ';
        $parse .= ' } else { ';
        $parse .= '$' . $varName . ' = \think\facade\Db::table("' . $table . '")->where(' . $where . ')->order("' . $order . '")->find();';
        $parse .= '}';
        $parse .= ' ?>';
        return $parse;
    }
}
