<?php
namespace app\common\model;



class OrderBackMoneyLogsModel extends BaseModel
{

    protected $table='o_back_money_logs';

    protected $json = ['third_result'];

    public static $fields_third_state = [
        ['name'=>'异常'],
        ['name'=>'处理中'],
        ['name'=>'已退款'],
    ];

    public static function record($order_id,$pay_way,$money,array $input_data = [])
    {
        $remark = trim($input_data['remark']??'');
        $third_code = $input_data['third_code']??0;
        $third_state = $input_data['third_state']??0;
        $third_message = $input_data['third_message']??'';
        $third_result = $input_data['third_result']??[];

        $model = new self();
        $model->setAttrs([
            'oid'=>$order_id,
            'pay_way'=>$pay_way,
            'remark'=>$remark,
            'money'=>$money,
            'third_code'=>$third_code,
            'third_state'=>$third_state,
            'third_message'=>$third_message,
            'third_result'=>$third_result,
        ]);
        $model->save();

    }

    public static function getPageData(array $input_data=[])
    {
        $limit = $input_data['limit']??null;
        $where = [];
        if(isset($input_data['order_id'])){
            $where[] = ['oid','=',$input_data['order_id']];
        }

        return self::where($where)->order('id desc')->paginate($limit);

    }



    public function apiNormalInfo()
    {
        return [
            'id'=>$this['id'],
            'pay_way'=>$this['pay_way'],
            'pay_way_name'=>OrderModel::getPropInfo('fields_pay_way',$this['pay_way'],'name'),
            'third_state_name'=>self::getPropInfo('fields_third_state',$this['third_state'],'name'),
        ];
    }

}