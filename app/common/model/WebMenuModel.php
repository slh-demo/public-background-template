<?php
namespace app\common\model;

use think\model\concern\SoftDelete;

class WebMenuModel extends BaseModel
{
    use SoftDelete;

    protected $table='web_menu';

    const VIEW_ROOT_DIR = "";
    const VIEW_ROOT = "app/view/index".self::VIEW_ROOT_DIR;

    private static $switch_child_route = [ ];


//    public function getTagsAttr($value)
//    {
////        $data = [];
////        $values = empty($value) ? [] : explode(",",$value) ;
////        foreach ($values as $item){
////            if(empty($item)) continue;
////            $data[] = intval($item);
////        }
//        return empty($value)?'': substr($value,1,-1);
//    }

    public static function getMenu()
    {
        $where = [];
        $where[] = ['status','=',1];
        $list = [];
        self::where($where)->order('sort asc')->select()->each(function($item)use(&$list){
            array_push($list,[
                'id'=>$item['id'],
                'img'=>$item['img'],
                'name'=>$item['name'],
                'icon'=>$item['icon'],
                'url'=>$item['url'],
            ]);
        });
        return $list;
    }


    public static function handleSaveData(array $input_data = [])
    {
        if(empty($input_data['name'])) throw new \Exception('请输入名字');
//        if(empty($input_data['url'])) throw new \Exception('请输入跳转路径');
        $input_data['node'] = empty($input_data['node']) || !is_array($input_data['node'])?'':implode(',',$input_data['node']);
//        $input_data['tags'] = !empty($input_data['tags']) && is_array($input_data['tags']) ? ','.implode(',',$input_data['tags']).',' : null;
//        $input_data['tags'] = !empty($input_data['tags']) ? ','.$input_data['tags'].',' : null;

        (new self())->actionAdd($input_data);
    }

    /**
     * 页面数据
     * @param array $input_data 封装数据
     * @throws
     * @return \think\Collection
     * */
    public static function getAllData(array $input_data = [])
    {
        $active_url = trim($input_data['active_url']??'');
        $where = [];
        $where[] =['pid','=',0];

        $where_link_fun = function($query)use($input_data){
            if(isset($input_data['status'])){
                $query->with(['linkChild'=>function($query)use($input_data){
                    $query->where(['status'=>$input_data['status']]);
                }])->where(['status'=>$input_data['status']]);
            }else{
                $query->with(['linkChild']);
            }
        };

        if(isset($input_data['status'])){
            $where[] = ['status','=',$input_data['status']];
        }

        $model = self::where($where)->with(['linkChild'=>$where_link_fun])->order('sort asc');

        return $model->select()->each(function($item)use($active_url){
            $is_active = false;
            $item['is_active'] = $item['url']==$active_url;
            foreach ($item['linkChild'] as $vo){
                $is_active = $vo['url']==$active_url;
                $vo['is_active'] = $is_active;
                if($is_active){
                    $item['is_active'] = $is_active;
                }

                foreach ($vo['linkChild'] as $child) {
                    $is_active = $child['url']==$active_url;
                    $vo['is_active'] = $is_active;
                    $child['is_active'] = $is_active;
                    if($is_active){
                        $item['is_active'] = $is_active;
                    }
                }

            }

        });
    }
    /**
     * 页面数据
     * @param array $input_data 封装数据
     * @throws
     * @return array
     * */
    public static function getTopData(array $input_data = [])
    {
        $where = [];
        $where[] =['pid','=',0];

        $where_link_fun = function($query)use($input_data){
            if(isset($input_data['status'])){
                $query->where(['status'=>$input_data['status']]);
            }
        };

        if(isset($input_data['status'])){
            $where[] = ['status','=',$input_data['status']];
        }

        $list = [];
        self::where($where)->with(['linkChild'=>$where_link_fun])->order('sort asc')->select()->each(function($item)use(&$list){
            $info = ['id'=>$item['id'],'pid'=>$item['pid'],'group_cate_id' => $item['group_cate_id'],'name'=>$item['name'],'list'=>[]];
            $linkChild = $item->getRelation('linkChild');
            foreach ($linkChild as $child){
                $info['list'][] = [
                    'id' => $child['id'],
                    'pid'=> $child['pid'],
                    'group_cate_id' => $child['group_cate_id'],
                    'name' => $child['name'],
                ];
            }
            array_push($list,$info);
        });

        return $list;
    }

    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(), [
            'desc'=> (string)$this['desc'],
            'link_node'=> (string)$this['link_node'],
            'is_top'=> (string)$this['is_top'],
            'cover_img'=> (string)$this['cover_img'],
            'm_cover_img'=> (string)$this['m_cover_img'],
            'icon'=> (string)$this['icon'],
            'intro'=> (string)$this['intro'],
            'content'=> (string)$this['content'],
            'meta_kw'=> (string)$this['meta_kw'],
            'meta_des'=> (string)$this['meta_des'],
            'sort'=> (int)$this['sort'],
            'tags' => $this['tags'],

            'node'=>empty($this['node'])?[]:explode(',',$this['node']),
            'status' => $this['status'],
            'status_bool'=> $this['status']==1,
            'update_time'=> $this['update_time'],
        ]);
    }

    public function apiNormalInfo()
    {

        return [
            'id' => $this['id'],
            'type' => $this['type'],
            'pid' => $this['pid'],
            'name' => (string)$this['name'],
            'url' => (string)$this['url'],
            'img' => (string)$this['img'],
            'route' => (string)$this['route'],
        ];
    }


    public function linkChild()
    {
        return $this->hasMany(self::class,'pid')->order('sort asc');
    }
}