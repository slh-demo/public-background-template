<?php
namespace app\common\model;
use think\model\concern\SoftDelete;
use think\Paginator;

class ProductModel extends BaseModel
{
    use SoftDelete;
    protected $table = 'product';

    protected $json = ['attr','info','content'];


    public function getGroupCateIdAttr($value,$data)
    {
        $cid = $data['cid']??'';
        $ct_id = $data['ct_id']??'';
        $str = empty($cid)?'':$cid;
        if(!empty($ct_id)){
            $str .= ','.$ct_id;
        }
        return $str;
    }

    public function getContentAttr($value)
    {

        return self::getContent($value);
    }

    public static function getContent($value)
    {
        if(!is_array($value)){
            $value = json_decode($value,true);
        }

        if(empty($value)){
            return [];
        }

        if(app()->http->getName()!='admin' && !empty($value)){
            $data = [];
            foreach ($value as $item){
                $name = $item['name']??'';
                $content = explode("\n",$item['content']??'');
                $data[] = [
                    'name' =>$name,
                    'content' =>$content,
                ];
            }
            return $data;

        }
        return $value;
    }

    public static function copy(array $input_data = [])
    {
        $id = $input_data['id']??0;
        if(empty($id))  throw new \Exception('参数异常:id');
        $model = self::find($id);
        if(empty($model))  throw new \Exception('产品不存在或已被删除,请刷新页面重新尝试');
        $data = $model->getData();
        unset($data['id'],$data['create_time'],$data['update_time'],$data['delete_time']);
        $new_model = new self();
        $new_model->setAttrs($data);
        $new_model->save();
    }


    /**
     * 获取列表
     * @param array  $input_data
     * @throws
     * @return Paginator
     * */
    public static function getPageData(array $input_data = [])
    {
        $limit = $input_data['limit']??null;
        $where=[];
        if(app()->http->getName()!='admin'){
            $where[] = ['status','=',1];
        }
        $model =self::where($where)->order('sort asc');
        return $model->paginate($limit);
    }


    public static function handleSaveData(array $input_data = [])
    {
        $group_cid=empty($input_data['group_cate_id'])?[]:explode(',',$input_data['group_cate_id']);
        if(empty($group_cid)) throw new \Exception('请选择分类');
        if(empty($input_data['name'])) throw new \Exception('请输入产品名');
        if(empty($input_data['img'])) throw new \Exception('请上传图片');
        $input_data['cid'] = $group_cid[0]??0;
        $input_data['ct_id'] = $group_cid[1]??0;
        $input_data['content'] = empty($input_data['content'])?null:$input_data['content'];
        $input_data['attr'] = empty($input_data['attr'])?null:$input_data['attr'];
        $input_data['info'] = empty($input_data['info'])?null:$input_data['info'];
        (new self())->actionAdd($input_data);
    }



    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[

            'status'=>$this['status'],
            'sort'=>$this['sort'],
            'status_name'=>self::getPropInfo('fields_status',$this['status'],'name'),
            'update_time'=>$this['update_time'],
            //分类
            'group_cate_id' => (string)$this['group_cate_id'],
            'editor'=>$this['editor'],
            'editor2'=>$this['editor2'],

            'meta_key'=>(string)$this['meta_key'],
            'meta_desc'=>(string)$this['meta_desc'],

        ]);
    }

    public function apiNormalInfo()
    {
        return [
            'id'=>(string)$this['id'],
            'name'=>$this['name'],
            'img'=>$this['img'],
            'content'=>$this['content'],
            'info'=>$this['info'],
            'attr'=>$this['attr'],
            'url'=>$this['url'],
        ];
    }


    public function linkChild()
    {
        return $this->hasMany(self::class,'pid')->order('sort asc');
    }
}