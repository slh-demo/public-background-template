<?php
namespace app\common\model;

use app\common\service\LangMap;
use think\facade\Db;
use think\model\concern\SoftDelete;

class UserDataSignModel extends BaseModel
{
    use SoftDelete;

    protected $table='users_date_sign';

    //签到积分
    public static function getIntegralArr()
    {
        $sign_integral = SysSettingModel::getContent('integral','sign_comma');
        return $sign_integral;
    }

    /**
     * 用户签到
     * @param UserModel $user_model 用户对象
     * @throws
     * @return
     * */
    public static function sign(UserModel $user_model,array $input_data = [])
    {
        $user_id = $user_model['id'];

        $_lang = $input_data['_lang']??'';

        $change_integral = $user_model['integral']; //变动前积分

        $yesterday = date('Y-m-d',strtotime('-1 day'));
        $today = date('Y-m-d');
        $model = self::where([
            ['uid','=',$user_model->id],
        ])->order('id desc')->find();
        if(!empty($model) && $model['date']==$today){
            throw new \Exception("今日已签到");
        }

        //获取可签到的列表数据
        //签到积分
        $integral_arr = self::getIntegralArr();
        //查看今天周几
        $week = date('w',time());
        $week_index = $week > 0 ? $week-1 : 6;
        $sign_integral = $integral_arr[$week_index]??0;

        try{
            Db::startTrans();
            $last_user_integral = $user_model['integral']+$sign_integral;
            //增加用户积分
            $row_num = UserModel::where(['id'=>$user_id,'integral'=>$user_model['integral']])->update([
                'integral'=> Db::raw('integral+'.$sign_integral),
                'history_integral' => Db::raw('history_integral+'.$sign_integral),
            ]);
            if(empty($row_num))  throw new \Exception("操作过于频繁,请稍后尝试");

            //增加签到
            $lx_times = empty($model->lx_times)?1:($model->date!=$yesterday?1:$model->lx_times+1);

            $current_month = (int)date('Ym');
            $new_model = new self();
            $new_model->setAttrs([
                'uid'=>$user_id,
                'date'=>$today,
                'month'=>$current_month,
                'times'=>empty($model->times)?1:$model->times+1,//总签到次数
                'lx_times'=>$lx_times,//总签到次数
                'integral'=>$sign_integral,//总签到次数
            ]);

            $new_model->save();
            Db::commit();
        }catch (\Exception $e){
            Db::rollback();
            throw new \Exception($e->getMessage());
        }
        //积分日志
        if($sign_integral>0) {
            UserLogsModel::recordData(1, $user_id, $sign_integral, "签到成功奖励积分", [
                'q_money'=>$change_integral,
                'h_money'=>($change_integral+$sign_integral),
                'm_type'=>1,
                'id' => $new_model['id'],
                'sign_integral'=>$sign_integral,
            ]);
        }

        return [$new_model->lx_times,$sign_integral,$today,$last_user_integral];
    }





    /**
     * 每日签到数据
     * @param array $input_data 请求内容
     * @throws
     * @return \think\Paginator
     * */
    public static function dayPageData(array $input_data=[])
    {
        $limit = $input_data['limit']??null;
        $start_date = $input_data['start_date']??'';
        $start_time = empty($start_date)?'':$start_date;
        $end_date = $input_data['end_date']??'';
        $end_time = empty($end_date)?'':$end_date;
        $keyword = trim($input_data['keyword']??'');
        $where = [];

        if($start_time && $end_time){
            $where[] = ['date','between',[$start_time,$end_time]];
        }elseif($start_time){
            $where[] = ['date','>',$start_time];
        }elseif($end_time){
            $where[] = ['date','<=',$end_time];
        }


        $fields = '*,sum(integral) as sum_integral';
        return self::where($where)->select($fields)->groupBy(['date'])->orderByDesc('date')->paginate($limit);
    }
}