<?php
namespace app\common\model;


class SmsModel extends BaseModel
{
    //数据库表名
    protected $table = 'sms';

    public static $fields_type = [
        0=>['name'=>'账户注册','exp_time'=>600,'content'=>'验证码:{verify},您正在使用注册功能,请勿泄漏给他人','checkSendMethod'=>'checkPhoneNotExist'],
        1=>['name'=>'忘记密码','exp_time'=>600,'content'=>'验证码:{verify},您正在使用注册功能,请勿泄漏给他人','checkSendMethod'=>'checkPhoneNotExist'],
        2=>['name'=>'手机号登录','exp_time'=>600,'content'=>'验证码:{verify},您正在使用注册功能,请勿泄漏给他人','checkSendMethod'=>'checkPhoneNotExist'],
        3=>['name'=>'更换手机号','exp_time'=>600,'content'=>'验证码:{verify},您正在使用注册功能,请勿泄漏给他人','checkSendMethod'=>'checkPhoneExist'],
        4=>['name'=>'授权绑定','exp_time'=>600,'content'=>'验证码:{verify},您正在使用注册功能,请勿泄漏给他人','checkSendMethod'=>'checkPhoneNotExist'],
        5=>['name'=>'测试','exp_time'=>600,'content'=>'验证码:{verify},您正在使用注册功能,请勿泄漏给他人'],
        6=>['name'=>'快捷登录','exp_time'=>600,'content'=>'验证码:{verify},您正在使用注册功能,请勿泄漏给他人'],
        7=>['name'=>'技师注册','exp_time'=>600,'content'=>'验证码:{verify},您正在使用注册功能,请勿泄漏给他人','checkSendMethod'=>'checkMasterPhoneExist'],
        8=>['name'=>'技师忘记密码','exp_time'=>600,'content'=>'验证码:{verify},您正在使用注册功能,请勿泄漏给他人','checkSendMethod'=>'checkMasterPhoneNotExist'],
        9=>['name'=>'更换手机号','exp_time'=>600,'content'=>'验证码:{verify},您正在使用注册功能,请勿泄漏给他人','checkSendMethod'=>'checkMasterPhoneExist'],
        10=>['name'=>'技师修改密码','exp_time'=>600,'content'=>'验证码:{verify},您正在使用注册功能,请勿泄漏给他人','checkSendMethod'=>'checkMasterPhoneNotExist'],

    ];

    /**
     * 发送验证码
     * @param integer $type 短信类型
     * @param string $phone 手机号码
     * @throws
     * */
    public static function send($type,$phone)
    {
        $type_info = self::getPropInfo('fields_type',$type);
        //验证类型
        if(!array_key_exists($type,self::getPropInfo('fields_type'))) throw new \Exception('发送类型异常');
        //验证手机号码
        if(!valid_phone($phone)) throw new \Exception('请输入正确的手机号');
        //验证短信发送
        if(!empty($type_info['checkSendMethod'])){
            $method = $type_info['checkSendMethod'];
            self::$method($phone);
        }

        //发送内容
        list($content,$param) = self::handleContent(self::getPropInfo('fields_type',$type));
        //发送短信
//        $send_info = \app\common\service\AliSms::sendAction($param['verify'],self::$fields_type[$type]['name'],$phone);
        $send_info = \app\common\service\AliSms::send($param['verify'],$phone);
//        $send_info = \app\common\service\HWSms::send($param['verify'],$phone);
        //保存数据库
        (new self)->save([
            'type'      => $type,
            'phone'     => $phone,
            'content'   => $content,
            'verify'    => isset($param['verify'])?$param['verify']:'',
            'info'      => $send_info
        ]);
    }

    /**
     * 处理数据
     * @param array $data
     * @return array
     * */
    public static function handleContent(array $data=[])
    {
        $param = [];
        $content = isset($data['content'])?$data['content']:'';
        $replace_data = [
            'exp_time' => intval((isset($data['exp_time'])?$data['exp_time']:0)/60), //直接转为分钟
            'verify'   => (string)mt_rand(100000,999999),
        ];
        preg_match_all('/\{[^\}]+\}/',$content,$matches);
        $match_data = isset($matches[0]) ? $matches[0] : [];

        if(count($match_data)){
            foreach ($match_data as $vo){
                $field = substr($vo,1,-1);
                $param[$field] = isset($replace_data[$field])?$replace_data[$field]:'';
            }
            $content = str_replace($match_data, $param,$content);
        }
        return [$content,$param];
    }


    /**
     * 验证验证码
     * @param int $type 验证码类型
     * @param string $phone 手机号码
     * @param string $verify 验证码
     * @throws
     * @return bool
     * */
    public static function validVerify($type,$phone,$verify)
    {
        if($verify=='1234'){
            return true;
        }
        //验证类型
        if(!array_key_exists($type,self::getPropInfo('fields_type'))) throw new \Exception('发送类型异常');
        //验证手机号码
        if(!valid_phone($phone)) throw new \Exception('请输入正确的手机号');
        if(empty($verify)) throw new \Exception('请输入验证码');
        $model = self::where(['type'=>$type,'phone'=>$phone])->order('id desc')->find();
        if(empty($model)) throw new \Exception('请先获取验证码');
        if(!empty($model['use_time'])) throw new \Exception('验证码已被使用,请重新获取');
        if(trim($model['verify']) != trim($verify)) throw new \Exception('验证码错误:');
        //保存验证码
        $model->use_time = date('Y-m-d H:i:s');
        $model->save();
        return true;
    }

    //验证是否注册
    public static function checkPhoneExist($phone)
    {
        if(UserModel::where(['phone'=>$phone])->find()){
            throw new \Exception('手机号已注册');
        }
    }
    //验证是否注册
    public static function checkPhoneNotExist($phone)
    {
        if(!UserModel::where(['phone'=>$phone])->find()){
            throw new \Exception('手机号未注册');
        }
    }
    //验证是否注册
    public static function checkMasterPhoneExist($phone)
    {
        if(MasterModel::where(['phone'=>$phone])->find()){
            throw new \Exception('手机号已注册');
        }
    }
    //验证是否注册
    public static function checkMasterPhoneNotExist($phone)
    {
        if(!MasterModel::where(['phone'=>$phone])->find()){
            throw new \Exception('手机号未注册');
        }
    }
}