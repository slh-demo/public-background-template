<?php
namespace app\common\model;


class UserCartCustomModel extends BaseModel
{
    protected $table = 'users_cart_custom';

    protected $json = ['list','logo_list'];


    public static function syncUserData($user_id,$session_id)
    {
        if(empty($user_id) || empty($session_id)){
            return ;
        }
        $session_model = self::where(['session_id'=>$session_id])->find();
        if(!empty($session_model)){
            self::where(['uid'=>$user_id])->delete();
            $session_model->setAttrs([
                'uid'=>$user_id,
                'session_id'=>null,
            ]);
            $session_model->save();
        }

    }

    //添加购物车
    public static function cartAdd(UserModel $user_model=null,array $input_data = [])
    {
        $session_id = $input_data['session_id']??'';
        $goods_id = $input_data['goods_id']??'';
        $logo = $input_data['logo']??'';
        $buy_num = intval($input_data['buy_num']??'');
        $remark = $input_data['remark']??'';
        $list = empty($input_data['list']) || !is_array($input_data['list'])? [] :$input_data['list'];
        $logo_list = empty($input_data['logo_list']) || !is_array($input_data['logo_list'])? [] :$input_data['logo_list'];

        if(empty($goods_id)) throw new \Exception(lang('err_data_param',['param'=>':goods_id']));
//        if(empty($logo)) throw new \Exception(lang('err_cart_custom_logo_empty'));
        if(empty($buy_num)) throw new \Exception(lang('err_cart_custom_buy_num_empty'));
        if(empty($list)) throw new \Exception(lang('err_data_param',['param'=>':list']));
        if(empty($logo_list)) throw new \Exception(lang('err_cart_custom_logo_empty'));

        $where = [];
        if ( empty($user_model) ){
            $where['session_id'] = $session_id;
        }else{
            $where['uid'] = $user_model['id'];
        }

        $model = self::where($where)->findOrEmpty();

        $model->setAttr('logo', $logo);
        $model->setAttr('logo_list', json_encode($logo_list,JSON_UNESCAPED_UNICODE));
        $model->setAttr('num', $buy_num);
        $model->setAttr('gid', $goods_id);
        $model->setAttr('remark', $remark);
        $model->setAttr('list', json_encode($list,JSON_UNESCAPED_UNICODE));
        $model->setAttrs($where);
        $model->save();

    }

    //删除
    public static function cartDel(UserModel $user_model=null,array $input_data = [])
    {
        $session_id = $input_data['session_id']??'';
        $id = empty($input_data['id']) ? [] : ( !is_array($input_data['id']) ? [$input_data['id']] : $input_data['id'] );
        if(empty($id)) throw new \Exception('参数异常:id');
        $where = [];
        if(!empty($user_model)){
            $where[] = ['uid','=',$user_model['id']];
        }elseif(!empty($session_id)){
            $where[] = ['session_id','=',$session_id];
        }else{
            return;
        }
        self::where($where)->whereIn('id',$id)->select()->each(function($item){
            $item->delete();
        });

    }
    //修改信息
    public static function selfModInfo(UserModel $user_model=null,array $input_data = [])
    {
        $session_id = $input_data['session_id']??'';
        $mode = $input_data['mode']??'cart';
        $where = [];
        if(!empty($user_model)){
            $where[] = ['uid','=',$user_model['id']];
        }elseif(!empty($session_id)){
            $where[] = ['session_id','=',$session_id];
        }else{
            return;
        }
        if($mode=='cart'){
            $id = $input_data['id']??0;
            if(empty($id)) throw new \Exception(lang('err_data_param',['id'=>'id']));
            $where[] = ['id',is_array($id)?'in':'=',$id];
        }
//        dump($where);exit;
        $update_data =[];
        if(isset($input_data['is_checked'])){
            $update_data['is_checked'] = $input_data['is_checked']==1?1:0;
        }
        if(isset($input_data['num'])){
            $num = intval($input_data['num']);
            $update_data['num'] = $num <=0 ? 1 : $num;
        }

        count($update_data)>0 && self::where($where)->update($update_data);
    }


    //心愿单选中/取消
    public static function cartCheck($uid,$cid=null)
    {
        $where[] = ['uid','=',$uid];
//        self::where($where)->update(['is_checked'=>0]);
        if($cid){            
            $where[] = ['id','in',$cid];

//            if(!self::where($where)->count()) throw new \Exception('异常操作');
//            $bol = self::where($where)->update(['is_checked'=>1]);
        }
        self::where($where)->update(['is_checked'=>\think\facade\Db::raw('if(is_checked>0,0,1)')]);

        return true;
    }


    //清理购物车
    public static function clearAll(UserModel $user_model,array $input_data = [])
    {
        $where = [];
        $where[] = ['uid','=',$user_model['id']];
        $mch_id = $input_data['mch_id']??0; //按商户删除
        $cart_id = $input_data['cid']??0; //购物车id
        $is_force = $input_data['is_force']??0; //强制删除所有
        if(!empty($mch_id)){
            $where[] = ['mch_id','=',$mch_id];
        }elseif (!empty($cart_id)){
            $where[] = ['id','=',$cart_id];
        } elseif (!empty($is_force)){

        }

        return self::where($where)->delete();
    }




    /**
     * 获取所有数据
     * @param $input_data array
     * @throws
     * @return array
     * */
    public static function getAllData(array $input_data = [])
    {
        $session_id = $input_data['session_id']??0;
        $user_id = $input_data['uid']??0;
        if(empty($user_id) && empty($session_id)){
            return [];
        }
//        $user_model = UserModel::find($user_id);
//        if(empty($user_model)){
//            return [];
//        }
        //数据池
        $input_data['show_all'] = 1; //全部查询
        //获取购物车
        $where =[];
        if(!empty($user_id)){
            $where[] = ['uid','=',$user_id];
        }else{
            $where[] = ['session_id','=',$session_id];
        }

        $cart_goods_ids = [];
        $cart_list = self::where($where)->select()->each(function($item)use(&$pool_list,&$cart_goods_ids){
            !in_array($item['gid'],$cart_goods_ids) && $cart_goods_ids[] = $item['gid'];
        });

        if(empty($cart_goods_ids)){
            return [];
        }
        //查询产品信息
//        GoodsModel::$user_model = $user_model;
//        GoodsSkuPriceModel::$user_model = $user_model;
        $goods_pool = [];
        GoodsModel::getPageData(['ignore_type'=>1,'limit'=>10000,'id'=>$cart_goods_ids])->each(function($item)use(&$goods_pool){
            $goods_pool[$item['id']] = $item;
        });

        //创建集合
        $collection = [];
        $cart_list->each(function($item,$index)use($goods_pool,&$collection){
            $goods_info = $goods_pool[$item['gid']]??null;
            if(!empty($goods_info)){
                $goods_info = clone $goods_info;


                $sku_id = $item['sku_id']??0;
                $g_key = $goods_info['id'].'_'.$sku_id;
                $goods_info->sku_id = $sku_id;
                $goods_info['buy_num'] = $item['num']??1;
                $goods_info['is_checked'] = $item['is_checked']??1;
                $goods_info['cart_id'] = $item['id']??0;
                $goods_info['cart_logo'] = $item['logo']??'';
                $goods_info['cart_logo_list'] = $item['logo_list']??[];
                $goods_info['cart_remark'] = $item['remark']??'';
                $goods_info['cart_list'] = $item['list']??[];
                $collection[$g_key] = clone $goods_info;
            }
        });

        if(!isset($input_data['handle'])){ //判断是否转义
            return $collection;
        }else{

            $goods_list = [];
            foreach ($collection as $key=>$goods){
                $goods_list[] = $goods->apiNormalInfo();
            }

            return $goods_list;
        }

    }





    //购物信息
    public function apiNormalInfo()
    {
        $linkGoods = $this->getRelation('linkGoods');
        $sku_info = empty($linkGoods)?null:$linkGoods->getRelation('linkSkuPriceOne');
//        dump($this->toArray());exit;
        return [
            'id' => (string)$this->getAttr('id'),
            'goods_id' => (string)$this->getAttr('gid'),
            'mch_id' => (string)$this->getAttr('mch_id'),
            'num' => (Int)$this->getAttr('num'),
            'cover_img' => (string)$linkGoods['cover_img'],
            'goods_name' => (string)$linkGoods['name'],
            'sold_price' => (string)$linkGoods['sold_price'],
            'og_price' => (string)$linkGoods['og_price'],
            'sku_id' => (int)$sku_info['id'],
            'sku_group_name' => (string)$sku_info['name'],
            'is_checked' => (int)$this->getAttr('is_checked'),
        ];
    }


    public function linkGoods()
    {

        return  $this->belongsTo(GoodsModel::class,'gid');
    }

    public function linkMch()
    {
        return  $this->belongsTo(UserModel::class,'mch_id');
    }


    public function linkGoodsAttrPrice()
    {
        return  $this->belongsTo(GoodsSkuPriceModel::class,'sku_id');
    }

}