<?php
namespace app\common\model;

use think\model\concern\SoftDelete;

class WgtModel extends BaseModel
{
    use SoftDelete;
    protected $table='wgt';
    public static $fields_type = [
        ['name'=>'用户端'],
        ['name'=>'技师端'],
    ];
    public static $fields_platform=[
        ['name'=>'全平台'],
        ['name'=>'android'],
        ['name'=>'ios'],
    ];

    public static function handleSaveData()
    {
        $model = new self();
        $version = input('version');
        $type = input('type',0,'intval');
        $platform = input('platform',0,'intval');
        $path = input('path');
        $content = input('content');

        if(empty($version)) throw new \Exception('请输入版本号');
        if(empty($path)) throw new \Exception('请上传wgt包');

        //查找最后一条记录
        $last_model = self::where(['type'=>$type,'platform'=>$platform])->order('id desc')->find();
        if(!empty($last_model) && $last_model['version']>=$version){
            throw new \Exception('最新添加的版本不得低于历史版本');
        }
        $model->setAttrs([
            'version' => $version,
            'platform' => $platform,
            'type' => $type,
            'path' => $path,
            'content' => $content,
        ]);
        $model->save();


    }

    /**
     * 页面数据
     * @param int $type 分类
     * @param array $input_data 封装数据
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data = [])
    {
        $limit = $input_data['limit']??null;
        $keyword = trim($input_data['keyword']??'');
        $where = [];

        if(isset($input_data['id'])){
            $where[] = ['id','=',$input_data['id']];
        }

        !empty($keyword) && $where[] = ['title','like','%'.$keyword.'%'];



        $order = 'id desc';

        return self::where($where)->order($order)->paginate($limit);
    }

    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[

            'status'=>(string)$this['status'],
            'status_name'=>self::getPropInfo('fields_status',$this['status'],'name'),
        ]);
    }

    public function apiNormalInfo()
    {
        return [
            'id'=>$this['id'],
            'type'=>$this['type'],
            'type_name'=>self::getPropInfo('fields_type',$this['type'],'name'),
            'platform'=>$this['platform'],
            'platform_name'=>self::getPropInfo('fields_platform',$this['platform'],'name'),
            'content' =>(string)$this['content'],
            'version' =>(string)$this['version'],
            'path' =>(string)$this['path'],
            'create_time' =>(string)$this['create_time'],
        ];
    }

}