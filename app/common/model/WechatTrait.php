<?php
namespace app\common\model;
use app\common\service\WxPublic;
use think\model\concern\SoftDelete;
use think\Paginator;

trait WechatTrait
{
    //给用户发送消息
    public function sendUserMsg($og_id,$openid,array $msg_data = [])
    {
        $msg_type = $msg_data['type']??'';
        if($msg_type=='image'){
            $content="image";
        }else{
            $content = $msg_data['content'];
        }
        try{
            $send_result_info = WxPublic::sendMsg($openid,$content,$msg_type);
        }catch (\Exception $e){
            $send_result_info = $e->getMessage();
        }
        $this->setAttrs([
            'send_msg_info'=>$send_result_info,
            'send_msg_time'=>date("Y-m-d H:i:s"),
        ]);
        $this->save();
    }
}