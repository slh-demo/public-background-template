<?php

namespace app\common\model;

use app\common\service\LangMap;
use think\model\concern\SoftDelete;

class GoodsCateModel extends BaseModel
{
    use SoftDelete;
    protected $table = 'goods_cate';


    public function getTagsAttr($value)
    {
//        $data = [];
//        $values = empty($value) ? [] : explode(",",$value) ;
//        foreach ($values as $item){
//            if(empty($item)) continue;
//            $data[] = intval($item);
//        }
        return empty($value)?'': mb_substr($value,1,-1);
    }

    //获取商品分类作为网站的菜单
    public static function getMenu()
    {
        $cate = self::where(['status' => 1, 'pid' => 0])->select();
        $data = [];
        foreach ($cate as $v) {
            $data[] = [
                'name' => $v['name'],
                'url' => 'shopping/products?cid=' . $v['id'],
            ];
        }
        return $data;
    }

    /**
     * 获取数据
     * @param array $input_data 请求内容
     * @throws
     * @return \think\Collection
     * */
    public static function getAllCate(array $input_data = [])
    {
        $is_cate = $input_data['is_cate'] ?? '';

        $keyword = trim($input_data['keyword'] ?? '');
        $whereOr = $where = [];

        $where[] = ['pid', '=', 0];
        !empty($keyword) && $where[] = ['name', 'like', '%' . $keyword . '%'];

        if (isset($input_data['id'])) {
            $where[] = ['id', '=', $input_data['id']];
        }

        if ($is_cate == 'category') {
            $whereOr[] = ['url', '=', ''];
            $whereOr[] = ['url', '=', null];
        }


        if (isset($input_data['status'])) {
            $where[] = ['status', '=', $input_data['status']];
        }

        return self::with(['linkChild' => function ($query) use ($input_data) {
            $where = [];
            if (isset($input_data['status'])) {
                $where[] = ['status', '=', $input_data['status']];
            }
            $query->where($where);
        }])->where($where)->where(function ($query) use ($whereOr) {
            $query->whereOr($whereOr);
        })->order('sort asc')->select();
    }


    public static function shareSelectList($lang)
    {
        $list = [];
        self::where(['pid' => 0, 'status' => 1, 'share_state' => 1])->order('sort asc')->select()->each(function ($item, $index) use (&$list, $lang) {
            array_push($list, $item->apiLangNormalInfo($lang));
        });
        return $list;
    }

    public static function getSelectList()
    {
        $list = [];
        self::with(['linkChild' => function ($query) {
            $query->where(['status' => 1]);
        }])->where(['pid' => 0, 'status' => 1])->order('sort asc')->select()->each(function ($item, $index) use (&$list) {
            $info = $item->apiNormalInfo();
            $info['child_list'] = [];
            $linkChild = $item->getRelation('linkChild');
            $child_list = [];
            if (!empty($linkChild)) {
                foreach ($linkChild as $vo) {
                    array_push($child_list, $vo->apiNormalInfo());
                }
            }
            $info['child_list'] = $child_list;
            array_push($list, $info);
        });
        return $list;
    }


    public static function handleSaveData(array $input_data = [])
    {
        $id = $input_data['id'] ?? 0;
        if (empty($input_data['name'])) throw new \Exception('请输入名字');

//        $input_data['tags'] = !empty($input_data['tags']) && is_array($input_data['tags']) ? ','.implode(',',$input_data['tags']).',' : null;
        $input_data['tags'] = !empty($input_data['tags']) ? ','.$input_data['tags'].',' : null;

        (new self())->actionAdd($input_data);
    }



    public function apiFullInfo()
    {

        return array_merge($this->apiNormalInfo(), [
            'pid' => $this->getAttr('pid'),
            'sort' => $this->getAttr('sort'),
            'update_time' => $this['update_time'],

            'tags' => $this['tags'],
            'status' => (string)$this['status'],
            'status_name' => self::getPropInfo('fields_status', $this['status'], 'name'),
        ]);
    }

    public function apiNormalInfo()
    {
        return [
            'id' => $this->getAttr('id'),
            'name' => $this->getAttr('name'),
            'name_ban' => $this->getAttr('name_ban'),
            'img' => (string)$this->getAttr('img'),
            'icon' => (string)$this->getAttr('icon'),
            'url' => (string)$this->getAttr('url'),
            'ad_url' => (string)$this->getAttr('ad_url'),

        ];
    }


    //导航分类
    public static function getNav()
    {
        return self::where(['status' => 1, 'pid' => 0])->order('sort asc')->select();
    }


    public function linkChild()
    {
        return $this->hasMany(self::class, 'pid')->order('sort asc');
    }
}
