<?php
namespace app\common\model;

use app\common\service\LangMap;
use app\common\service\TimeActivity;
use think\model\concern\SoftDelete;
use think\Paginator;

class UserCollGoodsModel extends BaseModel
{
    protected $table='users_coll_goods';

    public static function getCount($user_id = 0)
    {
        if(empty($user_id)){
            return 0;
        }
        return self::withJoin(['linkGoods'],'left')->where(['user_coll_goods_model.uid'=>$user_id,'status'=>1])->whereNotNull('coll_time')->count();
    }

    public static function del(UserModel $user_model, array $input_data = [])
    {
        $id = $input_data['id']??'';
        if(empty($id)) throw new \Exception(lang('err_data_param:',['id'=>'id']));
        self::where(['id'=>$id,'uid'=>$user_model['id']])->update(['coll_time'=>null]);
    }

    public static function coll(UserModel $user_model, array $input_data = [])
    {
        $is_coll = !isset($input_data['is_coll']) || $input_data['is_coll']==1?1:0;
        $goods_info = !is_array($input_data['goods_info']) ? [] : $input_data['goods_info'];

        foreach ($goods_info as $vo){
            $goods_id = $vo['goods_id']??0;
            if(empty($goods_id)) throw new \Exception(lang('err_product_not_empty'));

            $model = self::where(['uid'=>$user_model['id'],'gid'=>$vo['goods_id']])->findOrEmpty();
            $model->setAttr('uid',$user_model['id']);
            $model->setAttr('gid',$goods_id);
            if($is_coll){
                $model->setAttr('coll_time',date('Y-m-d H:i:s'));
            }else{
                $model->setAttr('coll_time',null);
            }
            $model->save();
        }
        return $is_coll;

    }


    public static function getPageData(array $input_data)
    {
        $where = [];
        $user_id = $input_data['uid']??0;
        if(empty($user_id)){
            return Paginator::make(null,1,1,0);
        }
        $where[] =['linkGoods2.status','=',1];
        $where[] =['uid','=',$user_id];
        //获取收藏的所有信息
        //查询数量
        $coll_count = self::whereNotNull('coll_time')->group('gid')->column('count(*)','gid');
        return self::withJoin(['linkGoods2'],'left')->with(['linkGoods'=>function($query){
            $query->with(['linkKill','linkSkuPrice','linkSkuPriceOne']);
        }])->where($where)->whereNotNull('coll_time')->order('id desc')->paginate()->each(function($info)use($coll_count){
            $info->setAttr('coll_count',$coll_count[$info['gid']]??0);

            $item = $info->getRelation('linkGoods');

            $info->setRelation('linkGoods',$item);

        });
    }

    public function apiNormalInfo()
    {
        $linkGoods = $this->getRelation('linkGoods');

        return array_merge(empty($linkGoods)?[]:$linkGoods->apiNormalInfo(),[
            'coll_id' =>$this['id'],
        ]);
    }





    public function linkGoods2()
    {
        return $this->belongsTo(GoodsModel::class,'gid');
    }
    public function linkGoods()
    {
        return $this->belongsTo(GoodsModel::class,'gid');
    }
    public function linkCollNumber()
    {
        return $this->hasMany(UserCollGoodsModel::class,'gid','gid')->whereNotNull('coll_time');
    }
}