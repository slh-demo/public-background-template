<?php
namespace app\common\model;
use think\model\concern\SoftDelete;

class EditorModel extends BaseModel
{
    use SoftDelete;
    protected $table = 'editor';


    /**
     * 获取列表
     * @param array  $input_data
     * @throws
     * @return array
     * */
    public static function getPageData(array $input_data = [])
    {
        $limit = $input_data['limit']??null;
        $type = empty($input_data['type'])?0:$input_data['type'];
        $where=[];
        $where[] =['type','=',(int)$type];
        if(app()->http->getName()!='admin'){
            $where[] = ['status','=',1];
        }
        $model =self::where($where)->order('sort asc');
        return $model->paginate($limit);
    }


    public static function handleSaveData(array $input_data = [])
    {
        if(empty($input_data['name'])) throw new \Exception('请输入名字');
        if(empty($input_data['img'])) throw new \Exception('请上传图片');
        (new self())->actionAdd($input_data);
    }



    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[

            'status'=>$this['status'],
            'sort'=>$this['sort'],
            'status_name'=>self::getPropInfo('fields_status',$this['status'],'name'),
            'update_time'=>$this['update_time'],
            'content'=>$this['content'],

        ]);
    }

    public function apiNormalInfo()
    {
        return [
            'id'=>(string)$this['id'],
            'pid'=>(string)(empty($this['pid'])?'':$this['pid']),
            'name'=>$this['name'],
            'sub_name'=>$this['sub_name'],
            'child_name'=>$this['child_name'],
            'img'=>$this['img'],
            'img2'=>$this['img2'],
            'url'=>$this['url'],
        ];
    }


    public function linkChild()
    {
        return $this->hasMany(self::class,'pid')->order('sort asc');
    }
}