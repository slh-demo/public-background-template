<?php
namespace app\common\model;

use think\model\concern\SoftDelete;

class PlatformLocationWordModel extends BaseModel
{
    use SoftDelete;
    protected $table='platform_location_word';



    public static function getNormalList()
    {
        $list = [];
        self::where(['status'=>1])->order('sort','asc')->order('sort asc')->select()->each(function($item)use(&$list){
            $key = empty($item['word'])?'#':$item['word'];
            if(!isset($list[$key])){
                $list[$key] = [
                    'letter'=>$key,
                    'data'=>[],
                ];
            }
            $list[$key]['data'][] = [
                'name'=>$item['name'],
                'mobile'=>'',
                'keyword'=>$item['name'].''.$key.'',
            ];
        });
        ksort($list);
        return array_values($list);
    }

    public static function handleSaveData(array $input_data = [])
    {
        if(empty($input_data['name'])) throw new \Exception('请输入名字');
        if(empty($input_data['word'])) throw new \Exception('请输入名字首字母');
        (new self())->actionAdd($input_data);
    }


    /**
     * 页面数据
     * @param array $input_data 图片类型
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data = [])
    {
        $limit = $input_data['limit']??null;
        $where=[];
        if(app()->http->getName()!='admin'){
            $where[] = ['status','=',1];
        }
        $keyword = empty($input_data['keyword'])?'':trim($input_data['keyword']);
        !empty($keyword) && $where[] = ['name|word','like','%'.$keyword.'%'];
        $model =self::where($where)->order('sort asc');
        return $model->paginate($limit);
    }

    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[
            'sort' => $this['sort'],
            'status' => $this['status'],
            'status_bool' => $this['status']==1,
            'update_time' => $this['update_time'],
        ]);
    }

    public function apiNormalInfo()
    {
        return [
            'id' => $this['id'],
            'name' => $this['name'],
            'word' => $this['word'],
        ];
    }
}