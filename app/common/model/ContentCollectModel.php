<?php
namespace app\common\model;
use think\model\concern\SoftDelete;
use think\Paginator;

class ContentCollectModel extends BaseModel
{
    use SoftDelete;
    protected $table = 'content_collect';

    public static $fields_type = [
        ['name'=>'项目需求资料'],
        ['name'=>'邮箱手机'],
    ];

    public static function contactSubmit(array $input_data = [])
    {
        if(empty($input_data['name'])) throw new \Exception("请输入您的名称");
        if(empty($input_data['company'])) throw new \Exception("请输入公司名");
        if(empty($input_data['tel'])) throw new \Exception("请输入您的电话");
        if(empty($input_data['content'])) throw new \Exception("请输入您的需求");
        (new self())->actionAdd($input_data);
    }

    public static function email(array $input_data = [])
    {
        if(empty($input_data['email'])) throw new \Exception("请输入您的邮箱");
        if(!valid_email($input_data['email'])) throw new \Exception("请输入正确的邮箱");
        (new self())->actionAdd($input_data);
    }

    /**
     * 获取列表
     * @param array  $input_data
     * @throws
     * @return Paginator
     * */
    public static function getPageData(array $input_data = [])
    {
        $limit = $input_data['limit']??null;
        $type = empty($input_data['type'])?0:$input_data['type'];
        $where=[];
        $where[] =['type','=',(int)$type];
        if(app()->http->getName()!='admin'){
            $where[] = ['status','=',1];
        }

        $keyword = trim($input_data['keyword']??'');
        !empty($keyword) && $where[] = ['email','like','%'.$keyword.'%'];

        return self::where($where)->order('id desc')->paginate($limit);
    }


    public static function handleSaveData(array $input_data = [])
    {
        if(empty($input_data['time'])) throw new \Exception('请输入时间');
        if(empty($input_data['img'])) throw new \Exception('请上传图片');
        (new self())->actionAdd($input_data);
    }



    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[

            'status'=>$this['status'],
            'sort'=>$this['sort'],
            'status_name'=>self::getPropInfo('fields_status',$this['status'],'name'),
            'update_time'=>$this['update_time'],
            'create_time'=>$this['create_time'],

        ]);
    }

    public function apiNormalInfo()
    {
        return [
            'id'=>(string)$this['id'],
            'time'=>$this['time'],
            'img'=>$this['img'],
            'email'=>$this['email'],
            'name'=>$this['name'],
            'company'=>$this['company'],
            'tel'=>$this['tel'],
            'content'=>$this['content'],
        ];
    }


    public function linkChild()
    {
        return $this->hasMany(self::class,'pid')->order('sort asc');
    }
}