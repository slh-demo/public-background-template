<?php
namespace app\common\model;

use think\model\concern\SoftDelete;

class WebFileHistoryModel extends BaseModel
{
    use SoftDelete;

    protected $table='web_file_history';

    public static function record($path,$content)
    {
        return self::insert([
            'path'=>$path,
            'content'=>$content,
        ]);
    }
}