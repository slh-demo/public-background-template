<?php
namespace app\common\model;



use app\common\service\Location;
use think\Model;
use think\model\concern\SoftDelete;

class UserAddrModel extends BaseModel
{
    use SoftDelete;

    protected $table = 'users_addr';
    protected $_lng_lat="";

    public static $fields_sex = [
        ['name'=>''],
        ['name'=>'先生'],
        ['name'=>'女士'],
    ];

    protected function getHidePhoneAttr($value,$data)
    {
        return empty($data['phone'])?'':substr_replace($data['phone'],'****',3,4);
    }




    public static function onAfterWrite(Model $model)
    {
        $data = $model->getData();
        if(!empty($data['is_default']) && !empty($data['uid'])){
            //其它的默认地址为0
            self::update(['is_default'=>'0'],[['uid','=',$data['uid']],['id','<>',$data['id']]]);
        }
    }

    /**
     * 获取数据
     * @param array $input_data 请求内容
     * @param int|null $limit 页面条数
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data=[])
    {
        $checkMode = $input_data['checkMode']??null;
        $limit = $input_data['limit']??null;

        if($checkMode=='search'){
            $limit = 1000;
        }

        $where = [];
        if(isset($input_data['id'])){
            $where[] = ['id','=',$input_data['id']];
        }
        isset($input_data['uid']) && $where[] =['uid','=',$input_data['uid']];
        return self::where($where)->order('is_default desc, create_time desc')->paginate($limit);
    }

    public static function handleSaveData(array $input_data = [])
    {
        if(empty($input_data["phone"]))  throw new \Exception(lang('err_location_info_param',['param'=>'phone']));
        if(isset($input_data['email'])){
            if(empty($input_data['email'])) throw new \Exception(lang('input_email'));
            if(!valid_email($input_data['email'])) throw new \Exception(lang('err_input_email'));
        }
        if(empty($input_data["country"]))  throw new \Exception(lang('err_location_info_param',['param'=>'country']));
        if(empty($input_data["first_name"]))  throw new \Exception(lang('err_location_info_param',['param'=>'first_name']));
        if(empty($input_data["last_name"]))  throw new \Exception(lang('err_location_info_param',['param'=>'last_name']));
//        if(empty($input_data["street"]))  throw new \Exception(lang('err_location_info_param',['param'=>'street']));
//        if(empty($input_data["state"]))  throw new \Exception(lang('err_location_info_param',['param'=>'state']));
//        if(empty($input_data["city"]))  throw new \Exception(lang('err_location_info_param',['param'=>'city']));
//        if(empty($input_data["postcode"]))  throw new \Exception(lang('err_location_info_param',['param'=>'postcode']));
        (new self())->actionAdd($input_data);
    }
    /**
     * 删除地址
     * @param $id int 地址id
     * @param $user_model UserModel|null 用户模型
     * @throws
     * */
    public static function del($id,UserModel $user_model=null)
    {
        $where['id'] =$id;
        !empty($user_model) && $where['uid'] = $user_model['id'];
        $model = new self();
        $model->actionDel($where);
    }

    /**
     * 设置默认地址
     * @param $id int 地址id
     * @param $user_model User|null 用户模型
     * @throws
     * */
    public static function setDef($id,UserModel $user_model=null)
    {
        !empty($user_model) && $where['uid'] = $user_model['id'];
        $model = self::find($id);
        if(empty($model)) throw new \Exception('操作异常');
        $model->is_default = 1;
        $model->save();

    }

    /**
     * 获取收货地址
     * @param $uid int 用户idid
     * @param $id int 地址id
     **/
    public static function getAddr($uid,$id=null)
    {
        $where['uid'] = $uid;
        if($id){
            $where['id'] = $id;
        }else{
            $where['is_default'] = 1;
        }
        return self::where($where)->find();
    }


    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[

        ]);
    }

    public function apiNormalInfo()
    {
        $country = $this->getAttr('country');
        return [
            'id' => $this->getAttr('id'),
            'first_name' => (string)$this->getAttr('first_name'),
            'last_name' => (string)$this->getAttr('last_name'),
            'name' => (string)$this->getAttr('name'),
            'phone' => (string)$this->getAttr('phone'),
            'email' => (string)$this->getAttr('email'),
            'location_name' => (string)$this->getAttr('location_name'),
            'country' => (string)$country,
            'country_name' => isset(Location::DEF_DATA[$country]) ? Location::DEF_DATA[$country]['name'] : '',
            'state' => (string)$this->getAttr('state'),
            'city' => (string)$this->getAttr('city'),
            'street' => (string)$this->getAttr('street'),
            'suite' => (string)$this->getAttr('suite'),
            'postcode' => (string)$this->getAttr('postcode'),
            'is_default' => $this->getAttr('is_default'),
        ];
    }
}