<?php
namespace app\common\model;

use think\model\concern\SoftDelete;

class CouponCateModel extends BaseModel
{
    use SoftDelete;
    protected $table='coupon_cate';

    /**
     * 获取数据
     * @param array $input_data 请求内容
     * @throws
     * @return \think\Collection
     * */
    public static function getAllCate(array $input_data=[])
    {


        $keyword = trim($input_data['keyword']??'');
        $where = [];
        $where[] = ['pid','=',0];
        !empty($keyword) && $where[] = ['name','like','%'.$keyword.'%'];


        if(isset($input_data['status'])){
            $where[] = ['status','=',$input_data['status']];
        }


        return self::with(['linkChild'])->where($where)->order('sort asc')->select();

    }

    public static function getOneSelectList()
    {
        $list = [];
        self::where(['pid'=>0,'status'=>1])->order('sort asc')->select()->each(function($item,$index)use(&$list){
            array_push($list,$item->apiNormalInfo());
        });
        return $list;
    }
    public static function getOneLangSelectList($lang)
    {
        $list = [];
        self::with(['linkChild'=>function($query){
            $query->where(['status'=>1]);
        }])->where(['pid'=>0,'status'=>1])->order('sort asc')->select()->each(function($item,$index)use(&$list,$lang){
            array_push($list,$item->apiLangNormalInfo($lang));
        });
        return $list;
    }

    public static function getSelectList()
    {
        $list = [];
        self::with(['linkChild'=>function($query){
            $query->where(['status'=>1]);
        }])->where(['pid'=>0,'status'=>1])->order('sort asc')->select()->each(function($item,$index)use(&$list){
            $info = $item->apiNormalInfo();
            $info['child_list'] = [];
            $linkChild = $item->getRelation('linkChild');
            $child_list = [];
            if(!empty($linkChild)){
                foreach ($linkChild as $vo){
                    array_push($child_list,$vo->apiNormalInfo());
                }
            }
            $info['child_list'] = $child_list;
            array_push($list,$info);
        });
        return $list;
    }

    public static function getSelectLangList($lang)
    {
        $list = [];
        self::with(['linkChild'=>function($query){
            $query->where(['status'=>1]);
        }])->where(['pid'=>0,'status'=>1])->order('sort asc')->select()->each(function($item,$index)use(&$list,$lang){
            $info = $item->apiLangNormalInfo($lang);
            $info['child_list'] = [];
            $linkChild = $item->getRelation('linkChild');
            $child_list = [];
            if(!empty($linkChild)){
                foreach ($linkChild as $vo){
                    array_push($child_list,$vo->apiLangNormalInfo($lang));
                }
            }
            $info['child_list'] = $child_list;
            array_push($list,$info);
        });
        return $list;
    }

    public static function generateModel(array $input_data = [])
    {
        $id = $input_data['id']??0;
        if(empty($input_data['name'])) throw new \Exception('请输入名字');
        (new self())->actionAdd($input_data);
    }



    public function apiFullInfo()
    {

        return array_merge($this->apiNormalInfo(),[
            'pid'=>$this->getAttr('pid'),
            'sort'=>$this->getAttr('sort'),
            'update_time' => $this['update_time'],

            'status'=>(string)$this['status'],
            'status_name'=>self::getPropInfo('fields_status',$this['status'],'name'),
        ]);
    }

    public function apiNormalInfo()
    {
        return [
            'id'=>$this->getAttr('id'),
            'name'=>$this->getAttr('name'),
            'name_ban'=>$this->getAttr('name_ban'),
            'img'=>(string)$this->getAttr('img'),
            'icon'=>(string)$this->getAttr('icon'),

        ];
    }
    public function apiLangNormalInfo($lang)
    {
        $name = $this->getAttr('name');
        if($lang=='ban'){
            $name = $this->getAttr('name_ban');
        }
        return [
            'id'=>$this->getAttr('id'),
            'name'=> $name,
            'img'=>(string)$this->getAttr('img'),
            'icon'=>(string)$this->getAttr('icon'),

        ];
    }

    //导航分类
    public static function getNav()
    {
        return self::where(['status'=>1,'pid'=>0])->order('sort asc')->select();
    }


    public function linkChild()
    {
        return $this->hasMany(self::class,'pid')->order('sort asc');
    }
}