<?php
namespace app\common\model;

use app\common\service\LangMap;
use think\model\concern\SoftDelete;

class GoodsSkuPriceModel extends BaseModel
{
    protected $table='goods_sku_price';
    public $kill_price = 0;
    /**
     * @var UserModel||null
     * */
    public static $user_model=null;
    //商品实际销售价格
    public function getSoldPriceAttr($value,$data)
    {
        $price = $data['price'];
        if(!empty($data['kill_price'])){
            return empty($data['kill_price'])?'0.00':$data['kill_price'];
        } else {
            return $price;
        }

    }

    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[
            'gid' => $this['gid'],
            'status_bool'=>$this['status']==1,
        ]);
    }

    public function apiNormalInfo()
    {
        $data = [
            'id' => $this['id'],
            'temp_id' => $this['temp_id'],
            'temp_pid' => $this['temp_pid'],
            'name' => (string)$this['name'],
            'img' => (string)$this['img'],
            'og_price' => $this['og_price'],
            'price' => $this['price'],
            'weight' => $this['weight'],
            'sold_price' => (string)$this['sold_price'],
            'stock' => $this['stock'],
        ];

        if(!empty($this['kill_price'])){
            $data['kill_price'] = $this['kill_price'];
        }
        if(!empty($this['kill_stock'])){
            $data['kill_stock'] = $this['kill_stock'];
        }
        return $data;
    }

}