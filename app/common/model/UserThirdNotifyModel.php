<?php
namespace app\common\model;


use app\common\service\HWSms;

class UserThirdNotifyModel extends BaseModel
{
    //数据库表名
    protected $table = 'users_third_notify';

    public static $fields_type = [
        ['name'=>'短信'],
        ['name'=>'小程序'],
    ];

    public static $fields_m_type = [
        ['name'=>'预约'],
    ];

    public static function sendNotify(BaseModel $model,$type=0)
    {

        if(!method_exists($model,'sendServiceNotify')){
            throw new \Exception("请检查函数信息");
        }
        list( $m_type, $cond_id, $phone, $data) = $model->sendServiceNotify($type);
        //发送短信
        try{
            $content = HWSms::sendNotify($phone,$data);
        }catch (\Exception $e){
            $content  = $e->getMessage();
        }
        $model = new self();
        $model->setAttrs([
            'type' => $type,
            'm_type' => $m_type,
            'cond_id' => $cond_id,
            'phone' => $phone,
            'params' => json_encode($data),
            'content' => $content,
        ]);
        $model->save();
        dump($m_type,$cond_id,$phone,$data,$content,$model);exit;


    }
}