<?php

namespace app\common\model;


class OrderServiceTimeModel extends BaseModel
{
    use TraitPay;
    protected $table='o_service_time';

    //订单有效时间--有效时间为2小时  单位秒
    const ORDER_EXP_TIME = 7200;

    protected $json = ['pay_info'];

    public static $fields_status = [
        ['name'=>'待开启'],
        ['name'=>'使用中'],
        ['name'=>'已完成'],
    ];

    //订单剩余支付时间
    public function getMinuteIntroAttr()
    {
        $is_add = $this['is_add'];
        if($is_add){
            return "时长：{$this['minute']}分钟";
        }else{
            return "加钟：{$this['minute']}分钟";
        }
    }
    //订单剩余支付时间
    public function getReduceSecond()
    {
        if($this['status']!=1){ //服务进行中
            return 0;
        }

        $start_datetime = $this->getData('start_time');
        $start_time = time();strtotime($start_datetime);
        $end_datetime = $this->getData('end_time');
        $end_time = strtotime($end_datetime);

        $reduce_second = $end_time - $start_time;
        return $reduce_second<=0 ? 0 : $reduce_second;
    }

    public static function verifySerState(MasterModel $model_master)
    {
        $where = [];
        $where[] = ['mid','=',$model_master['id']];
        $where[] = ['is_pay','=',1];
        $where[] = ['status','=',1];
        $where[] = ['start_time','<=',date('Y-m-d H:i:s')];
        $where[] = ['end_time','>=',date('Y-m-d H:i:s')];
        return OrderServiceTimeModel::where($where)->find();
    }

    public static function serComplete(OrderModel $model_order)
    {
        $end_date = date('Y-m-d H:i:s');
        MasterModel::where(['id'=>$model_order['mid']])->update([
            'servicing_end'=>$end_date,
        ]);

        self::where(['oid'=>$model_order['id']])->update([
            'status'=>2,
            'complete_time'=>$end_date,
        ]);
    }


    public static function addService(BaseModel $model)
    {
        $current_datetime =date('Y-m-d H:i:s');
        if($model instanceof OrderModel){

            $self_model = new self();
            $self_model->setAttrs([
                'no' => $model['no'],
                'uid' => $model['uid'],
                'mid' => $model['mid'],
                'oid' => $model['id'],
                'minute' => $model['ser_minute'],
                'end_time' => $model['ser_minute'],
                'pay_money' => $model['goods_money'],
                'is_pay' => 1,
                'pay_time' => $current_datetime,
            ]);
        }else{
            $self_model = $model;
        }
        $minute = empty($self_model['minute']) ? 0 : $self_model['minute'] ;
        $end_time = date('Y-m-d H:i:s',strtotime("+ $minute minute",strtotime($current_datetime)));
        $self_model->setAttr('start_time',$current_datetime); //已付款
        $self_model->setAttr('end_time',$end_time); //已付款
        $self_model->setAttr('status',1); //已付款
        $self_model->save();
        return $current_datetime;
    }

    public static function confirm(UserModel $user_model,array $input_data =[])
    {
        $current_datetime =date('Y-m-d H:i:s');

        $order_id = intval($input_data['order_id']??0);
        $order_goods_id = intval($input_data['order_goods_id']??0);
//        $minute = intval($input_data['minute']??0);
        $remark = trim($input_data['remark']??'');


        if(empty($order_id)) throw new \Exception("参数异常:order_id");
        if(empty($order_goods_id)) throw new \Exception("参数异常:order_goods_id");
//        if(empty($minute)) throw new \Exception("参数异常:minute");
//        if($minute<30) throw new \Exception("服务时间不得低于30分钟");
        $model_order = OrderModel::find($order_id);
        if(empty($model_order)) throw new \Exception("订单信息不存在");
        list($status_name,$handle_action) = $model_order->getStatusName();
        if(!in_array(OrderModel::ORDER_HANDLE_ADD_TIME,$handle_action)) throw new \Exception("订单未处于可续钟状态");
        $model_order_goods = OrderGoodsModel::find($order_goods_id);
        if(empty($model_order)) throw new \Exception("订单信息不存在");

        //每15分钟价格
        $price_minute = $model_order_goods['price_minute'];
        $price_num = 1;//ceil($minute/15); //多少个15分钟


        $model = new self();
        $model->setAttrs([
            'no' => 'S'.$model_order['no'],
            'uid' => $user_model['id'],
            'mid' => $model_order['mid'],
            'oid' => $model_order['id'],
            'minute' => $model_order['ser_minute'],
            'price_minute' => $price_minute,
            'pay_money' => $price_num * $price_minute,
            'remark' => $remark,
        ]);
        $model->save();
        return $model;
    }


    //付款方式
    public function _sure_pay($pay_way)
    {
        $this->setAttr('status',1); //已付款
        $this->setAttr('is_pay',1);
        $this->setAttr('pay_time',date('Y-m-d H:i:s'));

        self::addService($this);
    }


    public function apiNormalInfo()
    {
        $linkUser = $this->getRelation('linkUser');
        $linkMaster = $this->getRelation('linkMaster');

        return [
            'id'=> $this['id'],
            'start_time'=> $this['start_time'],
            'minute'=> $this['minute'],
            'end_time'=> $this['end_time'],
            'money'=> $this['money'],
            'status'=> $this['status'],

            'complete_time'=> $this['complete_time'],
            'create_time'=> $this['create_time'],

            'reduce_second' =>$this->getReduceSecond(), //剩余服务时长
            'minute_intro' => $this['minute_intro'],

            'user_id' => (int)$linkUser['id'],
            'user_name' => (string)$linkUser['name'],
            'user_avatar' => (string)$linkUser['avatar'],
            'user_phone' => (string)$linkUser['phone'],


            //商家信息
            'master_id' => $linkMaster['id'],
            'master_name' => (string)$linkMaster['name'],
            'master_avatar' => (string)$linkMaster['avatar'],
        ];
    }

    public function linkOrder()
    {
        return $this->belongsTo(OrderModel::class, 'oid');
    }

    public function linkUser()
    {
        return $this->belongsTo(UserModel::class,'uid');
    }


    public function linkMaster()
    {
        return $this->belongsTo(MasterModel::class,'mid');
    }


}