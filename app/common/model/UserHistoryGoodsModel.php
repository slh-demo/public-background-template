<?php
namespace app\common\model;

use app\common\service\LangMap;
use app\common\service\TimeActivity;
use think\model\concern\SoftDelete;
use think\Paginator;

class UserHistoryGoodsModel extends BaseModel
{
    protected $table='users_history_goods';


    public static function syncUserData($user_id,$session_id)
    {
        if(empty($user_id) || empty($session_id)){
            return ;
        }

        self::where(['session_id'=>$session_id])->select()->each(function($item)use($user_id){
            $where =[
                'uid'=>$user_id,
                'gid'=>$item['gid'],
            ];
            $model = self::where($where)->find();
            if(empty($model)){
                $model = new self();
                $model->setAttrs($where);
                $model->setAttr('show_time',date('Y-m-d H:i:s'));
                $model->save();
            }
        });
        self::where(['session_id'=>$session_id])->delete();
    }

    public static function getCount($user_id = 0)
    {
        if(empty($user_id)){
            return 0;
        }
        return self::withJoin(['linkGoods'],'left')->where(['user_coll_goods_model.uid'=>$user_id,'linkGoods.status'=>1])->whereNotNull('show_time')->count();
    }

    public static function del(UserModel $user_model, array $input_data = [])
    {
        $id = $input_data['id']??'';
        if(empty($id)) throw new \Exception(lang('err_data_param:',['id'=>'id']));
        self::where(['id'=>$id,'uid'=>$user_model['id']])->update(['show_time'=>null]);
    }

    public static function record(UserModel $user_model=null, array $input_data = [])
    {
        $session_id = $input_data['session_id']??'';
        $goods_id = $input_data['goods_id']??0;

        if(empty($goods_id)){
            return;
        }
        $where = [];
        $where['gid'] = $goods_id;

        if(!empty($user_model)){
            $where['uid']=$user_model['id'];
        }else{
            $where['session_id'] = $session_id;
        }
        $model = self::where($where)->findOrEmpty();
        $model->setAttrs($where);
        $model->setAttr('show_time',date('Y-m-d H:i:s'));
        $model->save();

    }


    public static function getPageData(array $input_data)
    {
        $where = [];
        $user_id = $input_data['uid']??0;
        $session_id = $input_data['session_id']??'';
        $limit = $input_data['limit']??null;
        if(!empty($user_id)){
            $where[] = ['uid','=',$user_id];
        }else{
            $where[] = ['session_id','=',$session_id];
        }
        $where[] =['linkGoods2.status','=',1];
        $where[] =['linkGoods2.delete_time','=',null];
        //获取收藏的所有信息
        //查询数量
        $list = [];
        $info = self::withJoin(['linkGoods2'],'left')->with(['linkGoods'=>function($query){
            $query->with(['linkKill','linkSkuPrice','linkSkuPriceOne']);
        }])->where($where)->whereNotNull('show_time')->order('show_time desc')->paginate($limit)->each(function($item)use(&$list){

            array_push($list, $item->getRelation('linkGoods'));

        });
        $paginate = Paginator::make($list,$info->listRows(),$info->currentPage(),$info->total());
        return $paginate;
    }

    public function apiNormalInfo()
    {
        $linkGoods = $this->getRelation('linkGoods');

        return array_merge(empty($linkGoods)?[]:$linkGoods->apiNormalInfo(),[
            'history_id' =>$this['id'],
        ]);
    }





    public function linkGoods2()
    {
        return $this->belongsTo(GoodsModel::class,'gid');
    }
    public function linkGoods()
    {
        return $this->belongsTo(GoodsModel::class,'gid');
    }
    public function linkCollNumber()
    {
        return $this->hasMany(UserCollGoodsModel::class,'gid','gid')->whereNotNull('show_time');
    }
}