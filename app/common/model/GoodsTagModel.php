<?php

namespace app\common\model;


use think\facade\Db;
use think\model\concern\SoftDelete;

class GoodsTagModel extends BaseModel
{
    use SoftDelete;

    protected $table = 'goods_tag';


    public static function getSelectList(array $input_data = [])
    {
        $list = [];
        $where = [];
        if (isset($input_data['status'])) {
            $where[] = ['status', '=', $input_data['status']];
        }

        self::where($where)->order('sort', 'asc')->select()->each(function ($item) use (&$list) {
            array_push($list, $item->apiNormalInfo());
        });
        return $list;
    }



    public static function handleSaveData(array $input_data = [])
    {
        if (empty($input_data['name'])) throw new \Exception('请输入名字');

        //直接保存信息
        $model = (new self())->actionAdd($input_data);
    }



    /**
     * 获取数据
     * @param array $input_data 请求内容
     * @param int|null $limit 页面条数
     * @throws
     * @return \think\Paginator
     * */
    public static function getPageData(array $input_data = [])
    {
        $limit = $input_data['limit'] ?? null;

        $where = [];
        if (isset($input_data['status'])) {
            $where[] = ['status', '=', $input_data['status']];
        }
        if (isset($input_data['id'])) {
            $where[] = ['id', '=', $input_data['id']];
        }

        !empty($input_data['keyword']) && $where[] = ['name', 'like', "%" . $input_data['keyword'] . "%"];

        return self::where($where)->order('sort', 'asc')->paginate($limit);
    }



    public function apiNormalInfo()
    {
        $linkTempPrice = $this->getRelation('linkTempPrice');
        $sku_price = [];
        if (!empty($linkTempPrice)) {
            foreach ($linkTempPrice as $vo) {
                $sku_price[] = $vo->apiFullInfo();
            }
        }
        return [
            'id' => $this['id'],
            'name' => (string)$this['name'],
        ];
    }
}
