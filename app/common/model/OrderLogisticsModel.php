<?php
namespace app\common\model;

use app\common\service\ExpressBird;
use app\common\service\Logistics;
use think\model\concern\SoftDelete;

class OrderLogisticsModel extends BaseModel
{
    use SoftDelete;

    protected $json = ['info','flow','express_info'];

    protected $table='o_logistics';

    public static $fields_status = [
        ['name'=>'无轨迹'],
        ['name'=>'已揽收'],
        ['name'=>'在途中'],
        ['name'=>'签收'],
        ['name'=>'问题件'],
    ];

    public static function mbCallback(array $input_data=[])
    {
        $platformOrderId = $input_data['platformOrderId']??'';
        if(empty($platformOrderId)){
            return;
        }

        $order_model = OrderModel::where(['no'=>$platformOrderId])->find();
        if(empty($order_model)){
            return;
        }
        //添加物流
        $model = new self();
        $model->setAttrs([
            'oid'=>$order_model['id'],
            'code'=>$input_data['logisticsCode']??'',
            'name'=>$input_data['logisticsName']??'',
            'no'=>$input_data['trackNumber']??'',
            'url'=>$input_data['trackUrl']??'',
        ]);
        $model->save();
        //发货完成
        $order_model->setAttrs([
            'send_time' => date('Y-m-d H:i:s'),
            'is_send' => 1,
            'is_receive' => 0,
        ]);
        $order_model->save();
        //发送订单物流变化通知
        $order_model->sendEmail('logistic');

    }


    /**
     * 发货2
     * @param $input_data array 请求数据
     * @throws
     * @return OrderModel
     * */
    public static function sendOrder(array $input_data = [])
    {
        $id = $input_data['oid']??0;
        $logistics_no = $input_data['logistics_no']??''; //物流编号
        $name = $input_data['name']??''; //物流名称
        $no = $input_data['no']??'';  //物流单号
        $more_no = trim($input_data['more_no']??'');
        $money = $input_data['money']??0;//运费
        $remark = trim($input_data['remark']??'');//备注

        if(empty($no))   throw new \Exception('请输入物流单号');
        if(empty($name) || empty($logistics_no))   throw new \Exception('请选择快递公司');

        if(empty($id))  throw new \Exception('订单数据异常');
        //查询订单信息
        $model = OrderModel::find($id);
        if(empty($model))  throw new \Exception('操作对象异常');


        \think\facade\Db::startTrans();
        try{
            //修改发货状态
            //物流
            $model_logistics = OrderLogisticsModel::where(['oid'=>$id])->findOrEmpty();
            $model_logistics->setAttrs([
                'oid' =>$id,
                'name' =>$name,
                'logistics_no' =>$logistics_no,
                'no' =>$no,
                'check_time' =>null,
                'flow' =>null,
                'err_flow' =>null,
                'money' =>$money,
                'remark' =>$remark,
            ]);
            $model_logistics->save();
            if(empty($model['is_send'])){
                //发货完成
                $model->setAttrs([
                    'send_end_time' => date('Y-m-d H:i:s'),
                    'is_send' => 1,
                    'is_receive' => 0,
                ]);
                $model->save();

            }
            \think\facade\Db::commit();
        }catch (\Exception $e){
            \think\facade\Db::rollback();
            throw new \Exception('订单操作异常:'.$e->getMessage());
        }
        //交易通知
        $model->trigger('sendNoticeSend');

        return OrderModel::find($model['id']);
    }


    public function apiNormalInfo()
    {
        $no = $this->getAttr('no');
        //查询快递
//        $check_time = $this->getAttr('check_time');

        $flow = $this->getAttr('flow');

//        if ( time() - $check_time > 15*60 ) { //查询物流
//            $this->setAttr('check_time',time());
//            try{
//                $flow = Logistics::check100($this['logistics_no'],$this['no']);
//                if(!empty($flow)){
//                    $this->setAttr('flow',$flow);
//                    $this->save();
//                }
//            }catch (\Exception $e){
//                $this->setAttrs([
//                    'err_flow'=>$e->getMessage(),
//                ]);
//                $this->save();
//            }
//
//        }
        return [
            'id' => $this['id'],
            'oid' => $this['oid'],
            'code' => (string)$this['code'],
            'no' => (string)$no,
            'name' => (string)$this['name'],
            'url' => (string)$this['url'],
            'logistics_no' => (string)$this['logistics_no'],
            'flow' => empty($flow)?[]:$flow,
        ];
    }



    public function linkOrder()
    {
        return $this->belongsTo(OrderModel::class, 'oid');
    }
}
