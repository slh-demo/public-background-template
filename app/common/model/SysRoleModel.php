<?php
namespace app\common\model;
use think\model\concern\SoftDelete;

class SysRoleModel extends BaseModel
{
    use SoftDelete;
    protected $table = 'sys_role';

    public static function getSelectList(array $where=['status'=>1],$api_field = 'apiNormalInfo')
    {
        $list = [];
        self::with(['linkChild'=>function($query)use($where){
            $query->where($where);
        }])->where(['pid'=>0])->where($where)->order('sort asc')->select()->each(function($item,$index)use(&$list,$api_field){
            $info = $item->$api_field();
            $info['child_list'] = [];
            $linkChild = $item->getRelation('linkChild');
            $child_list = [];
            if(!empty($linkChild)){
                foreach ($linkChild as $vo){
                    array_push($child_list,$vo->$api_field());
                }
            }
            $info['child_list'] = $child_list;
            array_push($list,$info);
        });
        return $list;
    }


    /**
     * 获取列表
     * @param array  $input_data
     * @throws
     * @return array
     * */
    public static function getPageData(array $input_data = [])
    {
        return self::getSelectList([],'apiFullInfo');
    }


    public static function handleSaveData(array $input_data = [])
    {
        if(empty($input_data['name'])) throw new \Exception('请输入角色名字');
        $input_data['node'] = empty($input_data['node']) || !is_array($input_data['node'])?'':implode(',',$input_data['node']);
        (new self())->actionAdd($input_data);
    }



    public function apiFullInfo()
    {
        return array_merge($this->apiNormalInfo(),[

            'status'=>$this['status'],
            'sort'=>$this['sort'],
            'node'=>empty($this['node'])?[]:explode(',',$this['node']),
            'update_time'=>$this['update_time'],
        ]);
    }

    public function apiNormalInfo()
    {
        return [
            'id'=>(string)$this['id'],
            'pid'=>(string)(empty($this['pid'])?'':$this['pid']),
            'name'=>$this['name'],
        ];
    }


    public function linkChild()
    {
        return $this->hasMany(self::class,'pid')->order('sort asc');
    }
}