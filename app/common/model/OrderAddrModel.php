<?php
namespace app\common\model;



use think\Model;

class OrderAddrModel extends BaseModel
{

    protected $table='o_addr';
    protected $_lng_lat="";

    public function getGroupNameAttr()
    {
        return $this['last_name'].' '. $this['first_name'];
    }

    public function getAddrFullAttr()
    {
        return $this['city'].' '. $this['state'].' '. $this['country_name'].' '. $this['postcode'].' '. $this['street'].' '. $this['suite'];
    }

    public function apiNormalInfo()
    {
        return [
            'id' => $this->getAttr('id'),
            'group_name' => $this->getAttr('group_name'),
            'name' => $this->getAttr('name'),
            'phone' => (string)$this->getAttr('phone'),
            'location_name' => $this->getAttr('location_name'),
            'sex_name' => UserAddrModel::getPropInfo('fields_sex',$this['sex'],'name'),
            'addr' => $this->getAttr('addr'),
            'lng' => $this->getAttr('lng'),
            'lat' => $this->getAttr('lat'),
            'addr_extra' => $this->getAttr('addr_extra'),
            'addr_full' => $this->getAttr('addr_full'),
        ];
    }




    protected function getHidePhoneAttr($value,$data)
    {
        return empty($data['phone'])?'':substr_replace($data['phone'],'****',3,4);
    }



}