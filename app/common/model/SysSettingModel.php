<?php
namespace app\common\model;
use think\model\concern\SoftDelete;

class SysSettingModel extends BaseModel
{
    const CACHE_PREFIX = 'cache_setting_sql';
    //数据库表名
    protected $table = 'sys_setting';

    protected $pk = 'type';

    public static $protocol_info = [
        'userProtocol' => ['name' => '用户协议'],
        'tipProtocol' => ['name' => '隐私协议'],

    ];

    //获取内容字段
    public static function getContent($type, $filed = null, $trance = true)
    {
        $cache_name = $type;
        if (!is_null($filed)) {
            $cache_name = $type . '_' . $filed;
        }
        $content = cache($cache_name);
        if (empty($content)) {
            $content = self::where('type', $cache_name)->value('content');
            //保存内容
            self::_cacheData($type, $cache_name, $content);
        }
        $content = self::_filterData($cache_name, $content, 'get', $trance);
        return $content;
    }

    //获取内容字段
    public static function setContent($type, $content)
    {
        if (is_array($content)) {
            if (is_numeric(key($content))) {
                $content = self::_filterData($type, $content, 'insert');
                self::_writeData($type, $content);
            } else {
                foreach ($content as $key => $vo) {
                    $pk_key = $type . '_' . $key;
                    $content = self::_filterData($pk_key, $vo, 'insert');

                    self::_writeData($pk_key, $content);
                }
            }
        } else {
            $content = self::_filterData($type, $content, 'insert');
            self::_writeData($type, $content);
        }
        self::_cacheclear($type);
    }

    //写入数据
    private static function _writeData($type, $content)
    {
        $model = self::where(['type' => $type])->find();
        if (empty($model)) {
            $model = new self;
        }
        $model->data([
            'type' => $type,
            'content' => $content,
            'update_time' => time(),
        ], true);
        $model->save();
        //清空缓存
        \think\facade\Cache::delete($type);;
    }

    private static function _filterData($name, $value, $opt_type = 'get', $trance = false)
    {
        $arr = explode('_', $name);
        $type = end($arr);
        if ($opt_type == 'get') {
            if ($trance && $type == 'array') {
                $value = empty($value) ? [] : explode(',', $value);
            } elseif ($trance && $type == 'json') {
                $value = empty($value) ? [] : json_decode($value, true);
            } elseif ($trance && $type == 'comma') { //逗号
                $value = empty($value) ? "" : $value;
                $value = empty($value) ? [] : explode(',', $value);
            } elseif ($trance && $type == 'enter') {//回车
                $value = empty($value) ? [] : explode("\n", $value);
            } elseif ($type == 'int') {//
                $value = (int)$value;
            } elseif ($trance && $type == 'linkSubShape') {//
                $value = empty($value) ? [] : explode("\n", $value);
                $info_arr = [];
                $key = 0;
                foreach ($value as $vo) {
                    if ($vo == '----') {
                        $key++;
                        continue;
                    } else {
                        $arr = explode('#', $vo);
                        $info_arr[$key][] = [
                            'name' => $arr[0] ?? '',
                            'href' => $arr[1] ?? '',
                        ];
                    }

                }
                $value = $info_arr;
            } elseif ($trance && $type == 'shape') {//
                $item_key0 = $arr[count($arr)-3]??'';
                $item_key1 = $arr[count($arr)-2]??'';
                $value = empty($value) ? [] : explode("\n", $value);
                $info_arr = [];
                $key = 0;
                foreach ($value as $vo) {
                    if(empty($vo)) continue;
                    $arr = explode('#', $vo);
                    $info_arr[] = [
                        $item_key0 => $arr[0] ?? '',
                        $item_key1 => $arr[1] ?? '',
                    ];

                }
                $value = $info_arr;
            }
        } else {
            if ($type == 'int') {
                $value = intval($value);
            } elseif ($type == 'float') {
                $value = floatval($value);
            } elseif ($type == 'array') {
                $value = empty($value) ? [] : $value;
                $value = implode(',', $value);
            } elseif ($type == 'json') {
                $value = empty($value) ? [] : $value;
                $value = json_encode($value);
            }
        }
        return $value;
    }


//    //购买卡信息
//    public static function getBuyCardInfo()
//    {
//        $card_list = self::getContent('power','card_info_enter');
//        $data = [];
//        foreach ($card_list as $vo){
//            $info = explode('|',$vo);
//            $data[] = [
//                'num'=>$info[0]??'',
//                'award_num'=>$info[1]??'',
//                'integral'=>$info[2]??'',
//                'money'=>$info[3]??'',
//                'intro'=>$info[4]??'',
//            ];
//        }
//        return $data;
//    }

    public static function getVipPrice($first=false)
    {
        $list = [];
        $vip_list = self::getContent('money','recharge_vip_enter');
        foreach ($vip_list as $item){
            $arr = explode("#",$item);
            if ( count($arr)<3 ) break;
            $list[] = [
                'name' =>$arr[0],
                'money' =>$arr[1],
                'day' =>$arr[2],
            ];
        }
        if($first){
            return $list[0]??[];
        }else{
            return $list;
        }
    }

    //打赏项
    public static function getOrderAward($first=false)
    {
        $list = [];
        $vip_list = self::getContent('money','order_award_enter');
        foreach ($vip_list as $item){
            $arr = explode("#",$item);
            if ( count($arr)<2 ) break;
            $list[] = [
                'name' =>$arr[0],
                'money' =>$arr[1],
            ];
        }
        if($first){
            return $list[0]??[];
        }else{
            return $list;
        }
    }

    //获取应用审核状态
    public static function getIntegralExchangeIntro()
    {
        $num = self::getContent('integral','exchange_num_int');
        $intro = '兑换通道已关闭';
        if($num){
            $intro = sprintf('兑换比例为%d:1',$num);
        }
        return [$num, $intro];
    }

    public static function goWayInfo($travel=null)
    {
        $data = [];

        $data[]=[
            'name' => '出租/滴滴',
            'value' => 'taxi',
            'money' => sprintf('%.02f',self::getContent('money','car_money')),
            'distance' => self::getContent('money','car_distance_int'),
            'money_step' => sprintf('%.02f',self::getContent('money','car_money_step')),
        ];

        if(!is_null($travel)){
            foreach ($data as $vo){
                if($vo['value']==$travel){
                    return $vo;
                }
            }
            return $data[0];
        }


        return $data;
    }


    //获取应用审核状态
    public static function getAppAuthState()
    {
        return (int)self::getContent('normal', 'auth_state');
    }


    //记录缓存
    private static function _cacheData($tag,$name,$content)
    {
        \think\facade\Cache::tag($tag)->set($name,$content,3600);
    }

    //删除缓存
    private static function _cacheclear($tag)
    {
        \think\facade\Cache::tag($tag)->clear();
    }

    
}