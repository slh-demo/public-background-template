<?php
namespace app\common\service;

use GuzzleHttp\Exception\RequestException;

class SocketReq{


    public static function request($api,array $body=[]){
        $url = config('socket.host').$api;

        try{
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $url, [
                'json' => $body,
                'verify'=>false,
                'headers' => [
                    'Accept' => 'application/json',
                ]
            ]);
            $result = $response->getBody()->getContents();
        }catch (RequestException $e){
            $result = $e->getResponse()->getBody()->getContents();
        }

        $result = empty($result)?[]:json_decode($result,true);

        return $result;

    }
}