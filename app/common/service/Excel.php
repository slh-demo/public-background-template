<?php
namespace app\common\service;

use think\Exception;

class Excel{
    public static function readLine($path)
    {
        $abs_path = root_path().$path;
        if(empty($path) || !file_exists($abs_path)){
            throw new Exception("文件不存在");
        }
        if(stripos($abs_path,'.csv')!==false){
            $objPHPExcel = \PHPExcel_IOFactory::createReader('CSV')->load($abs_path);
        }else{
            $objPHPExcel = \PHPExcel_IOFactory::load($abs_path);
        }
        return $objPHPExcel->getSheet(0)->toArray();
    }
}