<?php
namespace app\common\service;

use GTClient;
use GTNotification;
use GTPushMessage;
use GTPushRequest;

//defined(CURLOPT_CONNECTTIMEOUT_MS) && define ('CURLOPT_CONNECTTIMEOUT_MS', 156);
class Push
{

    const DOMAIN_URL = "https://restapi.getui.com";

    /**
     * @var GTClient
     * */
    private static $_igt;
    private static function _igtObj($push_type):MyGeTui
    {
        if(empty(self::$_igt)){
            $config = Config::push($push_type);
            self::$_igt = new MyGeTui(self::DOMAIN_URL, $config['app_key'], $config['app_id'], $config['master_secret'],$config['app_secret'] );
        }
        return self::$_igt;
    }

    public static function sendMsg($push_type,$client_id,$params,&$reqId){
        $reqId = create_guid();
        $title = $params['title'];
        $body = $params['body'];
        //创建API，APPID等配置参考 环境要求 进行获取
        $api = self::_igtObj($push_type);


//        dump($api->getAuthToken());exit;
        //设置推送参数
        $push = new GTPushRequest();
        $push->setRequestId($reqId);
        $message = new GTPushMessage();
        $notify = new GTNotification();
        $notify->setTitle($title);
        $notify->setBody($body);
        //点击通知后续动作，目前支持以下后续动作:
        //1、intent：打开应用内特定页面url：打开网页地址。2、payload：自定义消息内容启动应用。3、payload_custom：自定义消息内容不启动应用。4、startapp：打开应用首页。5、none：纯通知，无后续动作
        $notify->setClickType("none");
        $message->setNotification($notify);
        $push->setPushMessage($message);
//        dump($client_id);exit;
        $push->setCid($client_id);
        //处理返回结果
        $result = $api->pushApi()->pushToSingleByCid($push);
        return $result;
    }

}


class MyGeTui extends GTClient{
    public $appSecret = "";
    public function __construct($domainUrl,  $appkey, $appid, $masterSecret,$appSecret='', $ssl = NULL)
    {
        $this->appSecret = $appSecret;
        parent::__construct($domainUrl,  $appkey,$appid, $masterSecret, $ssl);
    }
}