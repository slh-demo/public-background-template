<?php
namespace app\common\service;
use app\common\model\SysSettingModel;

class Config
{

    const WX_TOKEN = "tcm";

    public static function payWay($key=null,$field=null,$config_field=null)
    {
        $data = [
            'wechat_mini' => ['name'=>'小程序支付','conf_key'=>'wx_min'],
            'wechat_app' => ['name'=>'app微信支付','conf_key'=>'wx_open'],
            'wechat_web' => ['name'=>'微信h5支付','conf_key'=>'wx_web'],

            'alipay_app' => ['name'=>'app支付宝支付','conf_key'=>null],
            'alipay_web' => ['name'=>'支付宝h5支付','conf_key'=>null],
        ];
        if($field==='config' && !is_null($key)){
            $data = self::_getInfo($data,$key);
            $arr = explode('_',$key);
            if(count($arr)==2){
                $method = $arr[0];
                return self::$method($data['conf_key'],$config_field);
            }else{
                return $data;
            }
        }else{
            return self::_getInfo($data,$key,$field);
        }
    }


    //获取微信信息
    public static function wechat($key=null,$field=null)
    {
        $data = [
            //微信商户
            'wx_mch'=>[
                'key_v3'=>'',
                'key'=>'',
                'mch_id'=>'',
                'serial_no'=>'',
                'amount_currency'=>'CNY',

            ],
            //微信开放平台-网页
            'wx_web'=>[
                'app_id'=>'',
                'app_secret'=>'',
                'encoding_AESKey'=>'',
            ],
            //微信开放平台-小程序wx823d0cc692abf129-6c48df07dd5e98855e4c89a843f4187f
            'wx_min'=>[
                'app_id'=>'',
                'app_secret'=>'',
            ],
            //微信开放平台-开放平台
            'wx_open'=>[
                'app_id'=>'',
                'app_secret'=>'',
            ],
            //微信开放平台-开放平台
            'logistics'=>[
                'key'=>'',
                'customer'=>'',
                'secret'=>'',
                'userid'=>'',
                'if'=>'',
            ],
            //微信开放平台-开放平台
            'hwsms'=>[
                'app_key'=>'',
                'app_secret'=>'',
                'signature'=>'',
                //验证码签名通道号
                'sender_channel_verify'=>'',
                'template_id_verify'=>'',
                //通知签名通道号
                'sender_channel_notify'=>'',
                'template_id_notify'=>'',
            ],
        ];

        //技师端
        if(app()->http->getName()=='master'){
            $data['wx_open']  = [
                'app_id'=>'',
                'app_secret'=>'',
            ];
        }

        return self::_getInfo($data,$key,$field);
    }
    //获取微信信息
    public static function alipay($key=null,$field=null)
    {
        $data = [
            'geteway'=>'https://openapi.alipay.com/gateway.do',
            'app_id'=>'',
            'rsa'=>'',
            'pk'   =>'',
            'yy_pk'=>'',
        ];
        return self::_getInfo($data,$key);
    }

    public static function pay_asiabill($key=null,$field=null)
    {
        $data = [
            'url'=>SysSettingModel::getContent('asiabill','url'),
            'debug'=>SysSettingModel::getContent('asiabill','mode')=='test',
            'mer_no'=>SysSettingModel::getContent('asiabill','mer_no'),
            'gateway_no'=>SysSettingModel::getContent('asiabill','gateway_no'),
            'sign_key'=>SysSettingModel::getContent('asiabill','sign_key'),
        ];
        return self::_getInfo($data,$key);
    }

    public static function pay_paypal($key=null,$field=null)
    {
        $data = [
            'url'=>'url',
            'debug'=>SysSettingModel::getContent('paypal','mode')=='test',
            'account'=>SysSettingModel::getContent('paypal','account'),
            'client_id'=>SysSettingModel::getContent('paypal','client_id'),
            'secret'=>SysSettingModel::getContent('paypal','secret'),
        ];
        return self::_getInfo($data,$key);
    }

    public static function maBangErp($key=null,$field=null)
    {
        $data = [
            'shop_name'=>SysSettingModel::getContent('mabang','shop_name'),
            'mch_no'=>SysSettingModel::getContent('mabang','mch_no'),
            'key'=>SysSettingModel::getContent('mabang','key'),
            'secret'=>SysSettingModel::getContent('mabang','secret'),
        ];
        return self::_getInfo($data,$key);
    }



    private static function _getInfo(array $data,$key=null,$field=null)
    {
        if(is_null($key)){
            return $data;
        }elseif(!isset($data[$key])){
            return false;
        }else{
            $info = $data[$key];
            if(is_null($field)){
                return $info;
            }else{
                return $info[$field];
            }
        }
    }

}