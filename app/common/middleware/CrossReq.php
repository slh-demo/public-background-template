<?php
namespace app\common\middleware;

use think\Response;

class CrossReq
{
    /**
     * @param $request \think\Request
     * @param $next \Closure
     * @return \Closure|Response
     * */
    public function handle($request, \Closure $next)
    {

        header("Access-Control-Allow-Origin:*");
        header("Access-Control-Allow-Headers:uniapp-platform,user-token,*");

        return $next($request);
    }
}