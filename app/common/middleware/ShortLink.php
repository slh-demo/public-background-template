<?php
namespace app\common\middleware;

use app\common\model\PlatformShortLinkModel;
use think\facade\Db;
use think\Response;

class ShortLink
{
    /**
     * @param $request \think\Request
     * @param $next \Closure
     * @return \Closure|Response
     * */
    public function handle($request, \Closure $next)
    {
        $url = $request->url(true);
        $where = [];
        $where[] = ['url','=',$url];
        $where[] = ['status','=',1];
        $model = PlatformShortLinkModel::where($where)->find();
        if(!empty($model)){
            return redirect($model['return_url']);
        }

        return $next($request);
    }
}