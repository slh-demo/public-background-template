<?php
namespace app\admin\controller;


use app\common\model\OrderModel;
use app\common\service\MaBangErp;

class Order extends Common
{

    //确认订单
    public function sure()
    {

        $id = $this->request->param('id');
        try {
            $model = OrderModel::sureOrder($this->user_model, $id);
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        list($status_name, $handle_action) = $model->getStatusName();
        return $this->_resData(1, '操作成功', [
            'status' => (int)$model['status'],
            'is_sure' => $model['is_sure'],
            'sure_time' => $model['sure_time'],
            'status_name' => $status_name,
            'handle_action' => $handle_action,
        ]);
    }


    //发货
    public function sendOrder()
    {
        $input_data = input();
        try {
            $model = \app\common\model\OrderLogisticsModel::sendOrder($input_data);
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        list($status_name, $handle_action) = $model->getStatusName();
        return $this->_resData(1, '操作成功', [
            'status' => (int)$model['status'],
            'is_send' => $model['is_send'],
            'send_time' => $model['send_time'],
            'status_name' => $status_name,
            'handle_action' => $handle_action,
        ]);
    }
    //发货
    public function recovery()
    {
        $input_data = input();
        try {
            $model = OrderModel::recovery($input_data);
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1, '操作成功', [

        ]);
    }


    //调整订单地址
    public function modAddr()
    {
        $php_input = input();
        try {
            \app\common\model\OrderModel::modAddr($php_input);
            return $this->_resData(1, '操作成功');
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
    }


    //确认出发
    public function send()
    {

        $id = $this->request->param('id');
        try {
            $model = OrderModel::sendOrder($this->user_model, $id);
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        list($status_name, $handle_action) = $model->getStatusName();
        return $this->_resData(1, '操作成功', [
            'status' => (int)$model['status'],
            'is_send' => $model['is_send'],
            'send_time' => $model['send_time'],
            'status_name' => $status_name,
            'handle_action' => $handle_action,
        ]);
    }

    //已到达
    public function receive()
    {

        $id = $this->request->param('id');
        try {
            $model = OrderModel::receiveOrder($this->user_model, $id);
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        list($status_name, $handle_action) = $model->getStatusName();
        return $this->_resData(1, '操作成功', [
            'status' => (int)$model['status'],
            'is_receive' => $model['is_receive'],
            'rec_time' => $model['rec_time'],
            'status_name' => $status_name,
            'handle_action' => $handle_action,
        ]);
    }

    //开始服务
    public function service()
    {

        $id = $this->request->param('id');
        try {
            $model = OrderModel::serviceOrder($this->user_model, $id);
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        list($status_name, $handle_action) = $model->getStatusName();
        return $this->_resData(1, '操作成功', [
            'status' => (int)$model['status'],
            'is_service' => $model['is_service'],
            'service_time' => $model['service_time'],
            'status_name' => $status_name,
            'handle_action' => $handle_action,
        ]);
    }

    //服务完成
    public function complete()
    {

        $id = $this->request->param('id');
        try {
            $model = OrderModel::completeOrder($this->user_model, $id);
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        list($status_name, $handle_action) = $model->getStatusName();
        return $this->_resData(1, '操作成功', [
            'status' => (int)$model['status'],
            'is_complete' => $model['is_complete'],
            'complete_time' => $model['complete_time'],
            'status_name' => $status_name,
            'handle_action' => $handle_action,
        ]);
    }

    //取消订单
    public function cancel()
    {

        $id = $this->request->param('id');
        try {
            $model = OrderModel::cancel($this->user_model, $id);
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        list($status_name, $handle_action) = $model->getStatusName();
        return $this->_resData(1, '操作成功', [
            'status' => (int)$model['status'],
            'status_name' => $status_name,
            'handle_action' => $handle_action,
        ]);
    }

    //删除订单
    public function actionDel()
    {

        $id = $this->request->param('id');
        try {
            OrderModel::del($this->user_model, $id);
            return $this->_resData(1, '操作成功');
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }

    }

    //确定支付
    public function surePay()
    {
        $id = $this->request->param('id');
        try {
            $model = OrderModel::surePay($this->user_model, $id);
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        list($status_name, $handle_action) = $model->getStatusName();
        return $this->_resData(1, '操作成功', [
            'status' => (int)$model['status'],
            'status_name' => $status_name,
            'handle_action' => $handle_action,
        ]);
    }

    public function saleAfterAuth()
    {
        try {
            $model = OrderModel::saleAfterAuth(input());
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        list($status_name, $handle_action) = $model->getStatusName();
        return $this->_resData(1, '操作成功', [
            'status' => (int)$model['status'],
            'status_name' => $status_name,
            'handle_action' => $handle_action,
        ]);
    }

    //退款
    public function backMoney()
    {
        try {
            $model = OrderModel::handleBackMoney(input());
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1, '操作成功', [
        ]);
    }

    public function modifyCustomerRemark()
    {
        try {
            $model = OrderModel::modifyCustomerRemark(input());
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1, '操作成功', [
        ]);
    }


    public function syncMbOrder()
    {
        try {
            $order_id = input('id');
            if(empty($order_id)) throw new \Exception('参数异常:id');
            $order_model = OrderModel::find($order_id);
            if(empty($order_model)) throw new \Exception('订单不存在或已被删除');
            $model = MaBangErp::createOrder($order_model);
            if(empty($model['mb_state'])){
                throw new \Exception("同步马帮信息异常:".$model['mb_result']);
            }
            list($status_name, $handle_action) = $model->getStatusName();
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }

        return $this->_resData(1, '操作成功', [
            'status' => (int)$order_model['status'],
            'status_name' => $status_name,
            'handle_action' => $handle_action,
        ]);
    }


}