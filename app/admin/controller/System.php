<?php
namespace app\admin\controller;

use app\common\model\ImageModel;
use app\common\model\SysManagerModel;
use app\common\model\SysRoleModel;
use app\common\model\SysSettingModel;
use app\common\model\WgtModel;

class System extends Common
{

    //获取列表
    public function sysRoleModel_lists()
    {
        $list = SysRoleModel::getPageData();
        return $this->_resData(1,'获取成功',['list'=>$list]);
    }

    //系统设置
    public function setting()
    {
        $type = input('type','','trim');
        $table_type = input('table_type','','trim');
        $content = input('content');

        $data = [];
        if(!empty($content)){

            $content = empty($content)?[]:$content;
            foreach ($content as $key=>$vo){
                $data[$key] = SysSettingModel::getContent($type, $key,false);
            }
        }else{
            $data['content'] = SysSettingModel::getContent($type, null,false);
        }

        return $this->_resData(1,'操作成功',$data);
    }

    //系统设置
    public function settingSave()
    {
        $input_data = input();
        $type = $input_data['type']??'';
        $table_type = $input_data['table_type']??'';
        $table_content = $input_data['table_content']??'';
        $content = $input_data['content']??'';
        unset($input_data['type']);
        unset($input_data['table_type']);
        try{
            if(!empty($table_type)){
                SysSettingModel::setContent($table_type, $table_content);
            }
            if(!empty($type)){
                SysSettingModel::setContent($type, $content);
            }
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }



}