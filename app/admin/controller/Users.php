<?php

namespace app\admin\controller;


use app\common\model\UserModel;
use think\facade\Db;

class Users extends Common
{

    public function auth_list()
    {
        $input_data = input();
        $keyword = trim($input_data['keyword'] ?? '');
        $where = [];
        if (!empty($keyword)) {
            if (is_numeric($keyword)) {
                $where[] = ['phone', 'like', '%' . $keyword . '%'];
            } else {
                $where[] = ['name', 'like', '%' . $keyword . '%'];
            }
        }
        $where[] = ['is_auth', '=', 0];
        $order = 'id desc';
        $info = \app\common\model\UserModel::where($where)->order($order)->paginate($input_data['limit'] ?? null);
        return $this->_resData(1, '获取成功', $info->toArray());
    }
}
