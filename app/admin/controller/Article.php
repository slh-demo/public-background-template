<?php

namespace app\admin\controller;



use app\common\model\ArticleCateModel;

class Article extends Common
{
    //获取列表
    public function articleCateModel_lists()
    {
        $list = ArticleCateModel::getSelectList(input());
        return $this->_resData(1,'获取成功',['list'=>$list]);
    }

}