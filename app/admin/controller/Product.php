<?php

namespace app\admin\controller;



use app\common\model\ProductCateModel;
use app\common\model\ProductModel;

class Product extends Common
{
    //获取列表
    public function productCateModel_lists()
    {
        $list = ProductCateModel::getPageData(input());
        return $this->_resData(1,'获取成功',['list'=>$list]);
    }

    public function productModel_copy()
    {
        try {
            $input_data = input();
            ProductModel::copy($input_data);
        } catch (\Exception $e) {
            return $this->_resData(0,$e->getMessage());
        }

        return $this->_resData(1,'提交成功');
    }
}