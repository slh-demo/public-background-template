<?php
namespace app\admin\controller;

use app\common\model\ImageModel;
use app\common\model\MerchantModel;
use app\common\model\SysManagerModel;
use app\common\model\SysRoleModel;
use app\common\model\SysSettingModel;
use app\common\model\UserDrawLogsModel;
use app\common\model\UserModel;
use app\common\model\WebsiteModel;
use app\common\model\WgtModel;

class Website extends Common
{

    //获取管理员列表
    public function lists()
    {
        $list = [];
        $info = WebsiteModel::getPageData(input())->each(function($item,$index)use(&$list){
            array_push($list,$item->apiFullInfo());
        });
        return $this->_resData(1,'获取成功',['list'=>$list,'total'=>$info->total(),'total_page'=>$info->lastPage()]);
    }

    //
    public function listsAdd()
    {
        $php_input = $this->request->param();
        try{
            WebsiteModel::handleSaveData($php_input);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

    //删除数据
    public function listsDel()
    {
        $id = $this->request->param('id',0,'int');
        try{
            WebsiteModel::actionDel(['id'=>$id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }


    //修改信息
    public function listsModInfo()
    {
        $php_input = input();
        try{
            WebsiteModel::modInfo($php_input);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

}