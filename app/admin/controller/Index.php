<?php

namespace app\admin\controller;

use app\common\model\ArtCateModel;
use app\common\model\ArticleCateModel;
use app\common\model\DeviceModel;
use app\common\model\DownloadCateModel;
use app\common\model\DownloadModel;
use app\common\model\GoodsAttrsModel;
use app\common\model\GoodsCateModel;
use app\common\model\GoodsSkuTempModel;
use app\common\model\GoodsTagModel;
use app\common\model\HelpCenterModel;
use app\common\model\HelpInfoModel;
use app\common\model\ImageModel;
use app\common\model\MasterModel;
use app\common\model\MchCateModel;
use app\common\model\MchMerchantModel;
use app\common\model\MerchantCateModel;
use app\common\model\MerchantModel;
use app\common\model\OrderModel;
use app\common\model\OrderStatisticalReportModel;
use app\common\model\PlatformAdModel;
use app\common\model\PlatformEmailTempModel;
use app\common\model\PlatformFreightPlanModel;
use app\common\model\PlatformLocationModel;
use app\common\model\PlatformProblemCateModel;
use app\common\model\PlatformStatisticsModel;
use app\common\model\PlatformTagsModel;
use app\common\model\ProCateModel;
use app\common\model\ProductCateModel;
use app\common\model\ProProjectModel;
use app\common\model\QaCateModel;
use app\common\model\RechargeCateModel;
use app\common\model\RechargeModel;
use app\common\model\SysManagerModel;
use app\common\model\SysRoleModel;
use app\common\model\SysSettingModel;
use app\common\model\UserModel;
use app\common\model\WebMenuModel;
use app\common\model\WebsiteModel;
use app\common\model\WxMenuModel;
use app\common\service\Location;
use app\common\service\Logistics;
use app\common\service\TimeActivity;
use app\common\service\WxPublic;

class Index extends Common
{

    protected $ignore_action = ['index', 'login', 'verify'];

    const LEFT_MENU = [];

    //获取管理员授权栏目
    public function authMenu()
    {
        if ($this->user_model['is_special'] == 1) {
            $data = self::LEFT_MENU;
        } else {
            $role_id = empty($this->user_model['rt_id']) ? $this->user_model['rid'] : $this->user_model['rt_id'];
            $role_nodes = \app\common\model\SysRoleModel::where(['id' => $role_id])->value('node');
            $auth_nodes = empty($role_nodes) ? [] : explode(",", $role_nodes);
            $data = [];
            foreach (self::LEFT_MENU as $item) {
                if (!in_array($item['auth_rules'], $auth_nodes)) {
                    continue;
                }
                $info = $item;
                if (!empty($info['childs'])) {
                    $info['childs'] = [];
                    foreach ($item['childs'] as $childs) {
                        if (!in_array($childs['auth_rules'], $auth_nodes)) {
                            continue;
                        }
                        $info['childs'][] = $childs;
                    }
                }

                $data[] = $info;
            }
        }
        return $this->_resData(1, '获取成功', $data);
    }


    public function index()
    {
        return view('index', []);
    }


    public function defInfo()
    {
        $data = [];
        $type = input('type', '', 'trim');

        if ($type == 'article_cate') {
            $data['cate_list'] = ArticleCateModel::getSelectList(['status' => 1]);
        } elseif ($type == 'goods_cate') { //管理员
            $data['cate_list'] = GoodsCateModel::getSelectList();
            $data['sku_temp_list'] = GoodsSkuTempModel::getSelectList(['status' => 1]);
//            $data['tag_options'] = GoodsTagModel::getSelectList(['status' => 1]);
        }  elseif ($type == 'goods_attrs') { //管理员
            $data['tag_lists'] = GoodsAttrsModel::getSelectList(['status' => 1]);
        }elseif ($type == 'web_menu') {

            $data['select_list'] =  WebMenuModel::getTopData(['status' => 1]);
        } elseif ($type == 'role') { //角色
            $data['nav'] = self::LEFT_MENU;
            $data['role_list'] = SysRoleModel::getSelectList();
        } elseif ($type == 'image') { //图片
            $data['image_list'] = ImageModel::getPropInfo('fields_type');
        } elseif ($type == 'problem_cate') { //问题分类
            $data['select_list'] = PlatformProblemCateModel::getSelectList();
        } elseif ($type == 'manager') { //管理员
            $data['role_list'] = SysRoleModel::getSelectList();
        } elseif ($type == 'show_total_data') {
            list($where,$whereOr) = OrderModel::getStateWhere('sure_pay');
            //累计成交订单数
            $data['order_num'] = OrderModel::where($where)->where(function($query)use($whereOr){
                $query->whereOr($whereOr);
            })->count();
            list($where,$whereOr) = OrderModel::getStateWhere('send');
            //累计成交订单数
            $data['order_send_num'] = OrderModel::where($where)->where(function($query)use($whereOr){
                $query->whereOr($whereOr);
            })->count();
            //累计成交订单数
            list($where,$whereOr) = OrderModel::getStateWhere('wait_pay');
            $data['order_wait_pay_num'] = OrderModel::where($where)->where(function($query)use($whereOr){
                $query->whereOr($whereOr);
            })->count();
            //累计成交总金额
            list($where,$whereOr) = OrderModel::getStateWhere('sure_pay');
            $data['order_money'] = OrderModel::where($where)->where(function($query)use($whereOr){
                $query->whereOr($whereOr);
            })->sum('pay_money');
            //用户总数
            $data['user_num'] = UserModel::count();

        }


        return $this->_resData(1, '获取成功', $data);
    }


    /**
     */
    public function login()
    {
        //用户登录
        try {
            $model = SysManagerModel::login(input());
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1, '登录成功', $this->loginInfo($model));
    }


    public function verify()
    {
        return \think\captcha\facade\Captcha::create();
    }

    //获取统计信息
    public function echarts()
    {
        $input_data = input();
        list($x_axis_data1, $series_data1) = OrderModel::echarts($input_data);

        list($x_axis_data2, $series_data2) = UserModel::echarts($input_data);
        list($x_axis_data3, $series_data3) = PlatformStatisticsModel::echarts(array_merge($input_data,['type'=>'ip']));

        $fnc = function($name,$data){
            return [
                "name"=> $name,
                "type"=> 'line',
                "symbolSize"=> 12,

                "data"=> $data
            ];
        };

        return $this->_resData(1,'获取成功',[
            'x_axis_data'=>$x_axis_data1,
            'series_data'=>[$fnc('成交单',$series_data1),$fnc('注册量',$series_data2),$fnc('访问量',$series_data3)],
        ]);
    }


}
