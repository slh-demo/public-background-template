<?php
namespace app\admin\controller;

use app\common\model\CommentModel;
use app\common\model\CouponModel;
use app\common\model\CouponUserModel;
use app\common\model\UserModel;

class Comment extends Common
{


    public function create()
    {
        $input_data  = input();
        $user_id = input('uid');
        $input_data['ignore_order'] = 1;
        try{
//            if(empty($user_id)) throw new \Exception('请输入评论用户id');
            $user_model = UserModel::find($user_id);
//            if(empty($user_model)) throw new \Exception('请输入评论用户信息不存在');
            CommentModel::comment($user_model, $input_data);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,'操作成功');
    }

}