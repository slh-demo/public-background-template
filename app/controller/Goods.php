<?php
namespace app\controller;


use app\common\model\CommentModel;
use app\common\model\CouponModel;
use app\common\model\CouponUserModel;
use app\common\model\GoodsModel;
use app\common\model\SysSettingModel;
use app\common\model\UserCartCustomModel;
use app\common\model\UserCartModel;
use app\common\model\UserCollGoodsModel;
use app\common\service\LangMap;

class Goods extends Common
{
//    protected $open_action_validate = true;
//    protected $ignore_action=['lists','colllist','cartlists'];

    public function lists()
    {
        $input_data = input();
        $list = [];
        $model = GoodsModel::getPageData($input_data)->each(function($item)use(&$list){
            $info = $item->apiNormalInfo();
            $info['is_coll'] = 0;
            array_push($list,$info);
        });
        if(!empty($this->user_id) && !empty($list)){
            $goods_ids = array_column($list,'id');
            $coll_goods_ids = UserCollGoodsModel::where([
                ['uid','=',$this->user_id],
                ['gid','in',$goods_ids],
            ])->whereNotNull('coll_time')->column('gid');
            if(!empty($coll_goods_ids)){
                foreach ($list as &$item){
                    $item['is_coll'] = in_array($item['id'], $coll_goods_ids)?1:0;
                }
            }
        }


        return $this->_resData(1, lang('tip_get_success'), [
            'list'=>$list,
            'total'=>$model->total(),
            'total_page'=>$model->lastPage(),
        ]);
    }


    public function coll()
    {
        try {
            $is_coll = UserCollGoodsModel::coll($this->user_model, input());
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1, lang($is_coll ? 'tip_coll_suc' : 'tip_coll_cancel'), [
            'is_coll' => $is_coll,
        ]);
    }


    //产品收藏
    public function collList()
    {
        $input_data = input();
        $input_data['uid'] = $this->user_id;
        $lists = [];
        $info = UserCollGoodsModel::getPageData($input_data)->each(function ($item, $index) use (&$lists) {
            array_push($lists, $item->apiNormalInfo());
        });

        return $this->_resData(1, lang('tip_get_success'), [
            'list' => $lists,
            'total' => $info->total(),
            'total_page' => $info->lastPage(),
        ]);
    }

    public function collDel()
    {
        try {
             UserCollGoodsModel::del($this->user_model, input());
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1, lang('tip_del_success'));
    }


    //添加购物车
    public function cartAdd()
    {
        $input_data = input();
        $input_data['uid'] = $this->user_id;
        $input_data['handle'] = 1;
        $input_data['session_id'] = $this->session_id;
        try {
            UserCartModel::cartAdd($this->user_model, $input_data);

        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1,  lang('tip_cart_success'), [
            'num' => UserCartModel::getNum($this->user_id,$this->session_id),
        ]);
    }

    public function cartLists()
    {
        $input_data = input();
        $input_data['uid'] = $this->user_id;
        $input_data['session_id'] = $this->session_id;
        $input_data['handle'] = 1;

        $channel = $input_data['channel']??'';
        if($channel=='cart_custom'){
            $goods_list = UserCartCustomModel::getAllData($input_data);
        }else{
            $goods_list = UserCartModel::getAllData($input_data);
        }

        $coupon_list = [];

        CouponModel::getPageData(['use_range'=>'1','status'=>1])->each(function($item)use(&$coupon_list){
            array_push($coupon_list,$item->apiNormalInfo());
        });

        $coupon = [];
        if(!empty($input_data['coupon_id'])){
            CouponModel::getPageData(['id'=>$input_data['coupon_id'],'status'=>1])->each(function($item)use(&$coupon){
                $coupon = $item->apiNormalInfo();
            });
        }

        //获取赠品信息
        $gift_money = SysSettingModel::getContent('free','gift_money');

        return $this->_resData(1, lang('tip_get_success'), [
            'gift_money'=>empty($gift_money)?0:round($gift_money,2),
            'coupon_list'=>$coupon_list,
            'goods_list'=>$goods_list,
            'coupon'=> (object)$coupon,
        ]);
    }

    public function cartDel()
    {
        try {
            $input_data = input();
            $input_data['uid'] = $this->user_id;
            $input_data['session_id'] = $this->session_id;
            UserCartModel::cartDel($this->user_model, $input_data);

        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1, lang('tip_opt_success'), [
            'num' => UserCartModel::getNum($this->user_id,$this->session_id),
        ]);
    }

    public function cartModInfo()
    {
        $input_data = input();
        $input_data['uid'] = $this->user_id;
        $input_data['session_id'] = $this->session_id;
        try {
            UserCartModel::selfModInfo($this->user_model, $input_data);
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1, lang('tip_opt_success'), [
        ]);
    }

    //购买定制产品
    public function addCustom()
    {
        $input_data = input();
        $input_data['session_id'] = $this->session_id;
        try {
            UserCartCustomModel::cartAdd($this->user_model, $input_data);
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1, lang('tip_opt_success'), [
        ]);
    }
}