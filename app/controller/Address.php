<?php
namespace app\controller;


use app\common\model\CommentModel;
use app\common\model\CommentPraiseModel;
use app\common\model\UserAddrModel;

class Address extends Common
{
    protected $open_action_validate = true;

    protected $ignore_action=['lists','userinfo'];

    //用户地址
    public function lists()
    {
        $input_data = input();
        $input_data['uid'] = $this->user_id;
        $list = [];
        $info = UserAddrModel::getPageData($input_data)->each(function($item) use (&$list){
            array_push($list,$item->apiNormalInfo());
        });
        return $this->_resData(1,lang('tip_get_success'),[
            'list'=>$list,
            'total'=>$info->total(),
            'total_page'=>$info->lastPage(),
        ]);
    }

    public function info()
    {
        $id =input('id',0,'intval');
        $input_data['uid'] = $this->user_id;
        $input_data['id'] = $id;

        $detail = [];
        UserAddrModel::getPageData($input_data)->each(function($item)use(&$detail){
            $detail = $item->apiFullInfo();
        });

        return $this->_resData(1,lang('tip_get_success'),[
            'detail' => $detail,
        ]);
    }


    //保存数据
    public function add()
    {

        $php_input = $this->request->param();
        try{
            $php_input['uid'] = $this->user_id;
            UserAddrModel::handleSaveData($php_input);
        }catch (\Exception $e){
            return json(['code'=>0,'msg'=>$e->getMessage()]);
        }
        return json(['code'=>1,'msg'=>lang('tip_opt_success')]);
    }

    //删除数据
    public function del()
    {
        $id = input('id',0,'intval');
        try{
            UserAddrModel::actionDel(['id'=>$id,'uid'=>$this->user_id]);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,lang('tip_opt_success'));
    }

    public function modInfo()
    {
        $id = input('id',0,'intval');
        try {
            $update = [];
            $update['is_default'] = input('is_default',0,'intval');
            UserAddrModel::modInfo($update,['id'=>$id,'uid'=>$this->user_id]);
        } catch (\Exception $e) {
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,lang('tip_opt_success'));
    }
}