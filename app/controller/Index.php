<?php
namespace app\controller;



use app\common\model\CommentModel;
use app\common\model\ContentCollectModel;
use app\common\model\GoodsModel;
use app\common\model\PlatformAdModel;
use app\common\model\PlatformStatisticsModel;
use app\common\model\SysSettingModel;
use app\common\model\UserCartCustomModel;
use app\common\model\UserCartModel;
use app\common\model\UserCollGoodsModel;
use app\common\model\UserHistoryGoodsModel;
use app\common\model\UserModel;
use think\facade\Db;
use think\facade\Session;

class Index extends Common
{
//    public function index()
//    {
////        dump(Lang::defaultLangSet(),lang('lang_var'),$this->app->config);exit;
//        return view('index');
//    }

    public function initData()
    {
        return $this->_resData(1,lang('tip_get_success'),[
            'cart_num'=>UserCartModel::getNum($this->user_id,$this->session_id),
        ]);
    }

    public static function syncUserData($user_id,$session_id)
    {
        if(empty($user_id) || empty($session_id)){
            return;
        }

        //查询购物车
        UserCartModel::syncUserData($user_id,$session_id);
        //产品浏览记录
        UserHistoryGoodsModel::syncUserData($user_id,$session_id);
        //定制产品
        UserCartCustomModel::syncUserData($user_id,$session_id);
    }

    public function login()
    {
        $input_data = input();
        try {
            $model = UserModel::handleLogin($input_data);
            $this->createSession($model['id']);
            self::syncUserData($model['id'],$this->session_id);
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1, lang('tip_login_success'));
    }

    //注册用户
    public function register()
    {

        $input_data = input();
        try {
            $model = UserModel::register($input_data);
            session('user_info',[
                'user_id'=>$model['id'],
            ]);
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1, lang('tip_register_success'));


    }


    public function logout()
    {
//        session('user_info',null);
        Session::destroy();
        return $this->_resData(1,lang('tip_logout_success'));
    }

    public function contactSubmit()
    {
        try {
            $input_data = input();
            ContentCollectModel::contactSubmit($input_data);
        } catch (\Exception $e) {
            return $this->_resData(0,$e->getMessage());
        }

        return $this->_resData(1,lang('tip_submit_email'));
    }

    public function subscribeEmail()
    {
        try {
            $input_data = input();
            $input_data['type'] = 1;
             ContentCollectModel::email($input_data);
        } catch (\Exception $e) {
            return $this->_resData(0,$e->getMessage());
        }

        return $this->_resData(1,lang('tip_submit_success'));
    }

    public function forgetEmail()
    {
        try {
            $input_data = input();
            UserModel::forgetEmail($input_data);
        } catch (\Exception $e) {
            return $this->_resData(0,$e->getMessage());
        }

        return $this->_resData(1,lang('tip_email_success'));
    }

    public function forgetPwd()
    {

        return view('/forgotPwd');
    }
    public function forgetPwdAction()
    {

        try {
            $input_data = input();
            UserModel::forgetEmailModPwd($input_data);
        } catch (\Exception $e) {
            return $this->_resData(0,$e->getMessage());
        }

        return $this->_resData(1,lang('tip_opt_success'));
    }


    public function products()
    {
        $goodsInfo = null ;
        $id = input('id','','intval');
        $input_data['id'] = $id;
        $input_data['status'] = 1;
        $input_data['ignore_type'] = 1;

        GoodsModel::getPageData($input_data)->each(function($item)use(&$goodsInfo){
            $goodsInfo = $item;
        });
        if(empty($goodsInfo) || $goodsInfo['status']!=1){
            return view('/error/miss',[]);
        }

//        dump($goodsInfo['sku_attr']);exit;
        $comment_info = CommentModel::getCommentInfo(['check_cond_id'=>$id]);
        //添加
        UserHistoryGoodsModel::record($this->user_model,['session_id'=>$this->session_id,'goods_id'=>$id]);

        try{
            GoodsModel::where(['id'=>$id])->update(['views'=>Db::raw('views +1')]);
        }catch (\Exception $e){}

        $ad_list =[];
        PlatformAdModel::getPageData(['status'=>1,'type'=>1,'activeState'=>'goods_ad','goods_id'=>$id])->each(function($item)use(&$ad_list){
            $info = $item->apiNormalInfo();
            $info['list'] = [];
            if(!empty($item['goods_tags'])){
                GoodsModel::getPageData(['status'=>1,'keyword_tags'=>$item['goods_tags']])->each(function ($goods)use(&$info){
                    array_push($info['list'], $goods->apiNormalInfo());
                });
            }
            array_push($ad_list, $info);
        });

        $viewPage='/products';
        if($goodsInfo['type']==1){
            $viewPage = "/productscustom";
        }

        $custom_logo_num_intro = empty($goodsInfo['custom_logo_num_intro'])?[]:$goodsInfo['custom_logo_num_intro'];
        //获取赠品信息
        $gift_money = SysSettingModel::getContent('free','gift_money');
        $is_coll = 0;
        if(!empty($this->user_id) ){
            $coll_goods_ids = UserCollGoodsModel::where([
                ['uid','=',$this->user_id],
                ['gid','=',$id],
            ])->whereNotNull('coll_time')->column('gid');
            if(!empty($coll_goods_ids)){
                $is_coll = 1;
            }
        }
        return view($viewPage,[
            'is_coll'=>$is_coll,
            'gift_money'=>empty($gift_money)?0:round($gift_money,2),
            'custom_logo_num_intro'=>$custom_logo_num_intro,
            'goodsInfo'=>$goodsInfo,
            'sku_price'=>empty($goodsInfo['sku_price']) ? [] : $goodsInfo['sku_price'],
            'sku'=>empty($goodsInfo['sku_attr']) ? [] : $goodsInfo['sku_attr'],
            'commentInfo'=>$comment_info,
            'ad_list'=>$ad_list,
        ]);
    }

    public function productscustom()
    {
        return $this->products();
    }

    public function statistics()
    {
        $input_data = input();
        $type = input('type');
        PlatformStatisticsModel::record($type,$input_data);
        return $this->_resData(1);
    }

}
