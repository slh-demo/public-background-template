<?php
namespace app\controller;


use app\common\model\CommentModel;
use app\common\model\GoodsModel;
use app\common\model\UserHistoryGoodsModel;
use think\facade\Db;

class Shopping extends Common
{
    public function productsdetail()
    {
        $goodsInfo = null ;
        $id = input('id','','intval');
        $input_data['id'] = $id;
        $input_data['status'] = 1;

        GoodsModel::getPageData($input_data)->each(function($item)use(&$goodsInfo){
            $goodsInfo = $item;
        });
//        dump($goodsInfo['sku_attr']);exit;
        $comment_info = CommentModel::getCommentInfo(['check_cond_id'=>$id]);
        //添加
        UserHistoryGoodsModel::record($this->user_model,['session_id'=>$this->session_id,'goods_id'=>$id]);

        try{
            GoodsModel::where(['id'=>$id])->update(['views'=>Db::raw('views +1')]);
        }catch (\Exception $e){}
//        dump($comment_info);exit;
        return view('productsdetail',[
            'goodsInfo'=>$goodsInfo,
            'sku_price'=>empty($goodsInfo['sku_price']) ? [] : $goodsInfo['sku_price'],
            'sku'=>empty($goodsInfo['sku_attr']) ? [] : $goodsInfo['sku_attr'],
            'commentInfo'=>$comment_info,
        ]);
    }
}