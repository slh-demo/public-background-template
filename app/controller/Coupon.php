<?php
namespace app\controller;

use app\common\model\CouponCateModel;
use app\common\model\CouponModel;
use app\common\model\CouponUserModel;
use app\common\model\GoodsModel;

class Coupon  extends Common
{
    public function cate()
    {
        $list = CouponCateModel::getOneLangSelectList([]);
        return $this->_resData(1,lang('tip_get_success'),['list'=>$list]);
    }
    public function search()
    {
        $goods_ids = input('goods_ids');
        $code = input('code');
        if(empty($code)){
            return $this->_resData(0,lang('err_data'));
        }
        $goods_ids = empty($goods_ids) || !is_array($goods_ids) ? [] : $goods_ids;
        $input_data['code'] = $code;
        $input_data['status'] = 1;
        $coupon = [];
        $coupon_model =null;
        CouponModel::getPageData($input_data)->each(function ($item)use(&$coupon,&$coupon_model){
            $coupon_model = $item;
            $coupon = $item->apiNormalInfo();
        });
        if(empty($coupon)){
            return $this->_resData(0,lang('err_coupon_no_exist',['code'=>$code]));
        }


        $coupon_dis_goods_tags = empty($coupon_model['dis_goods_tags']) ? [] : array_values(array_filter(explode(',',$coupon_model['dis_goods_tags'])));
        if(!empty($goods_ids) && !empty($coupon_dis_goods_tags)){
            $tags_arr = GoodsModel::whereIn('id', $goods_ids)->column('tags');
            $all_goods_tags =[];
            foreach ($tags_arr as $tag){
                $goods_tags = empty($tag)?[]:array_values(array_filter(explode(',', $tag)));
                $all_goods_tags = array_merge($all_goods_tags, $goods_tags);
            }
            if(array_intersect($all_goods_tags,$coupon_dis_goods_tags)){
                throw new \Exception(lang('err_coupon_dis_goods_tags_exist'));
            }
        }

        return $this->_resData(1,lang('tip_get_success'),['coupon'=>$coupon]);
    }

    public function lists()
    {
        $input_data = input();
        $input_data['status'] = 1;
        $input_data['uid'] = $this->user_id;
        $input_data['reg_send'] = 0;
        $list =[];
        $model = CouponModel::getPageData($input_data);
        foreach ($model as $item){
            array_push($list,$item->apiNormalInfo());
        }

        return $this->_resData(1,lang('tip_get_success'),['list'=>$list,'total'=>$model->total(),'total_page'=>$model->lastPage()]);
    }

    public function openLists()
    {
        $input_data = input();
        $input_data['status'] = 1;
        $input_data['use_range'] = 1;


        $list =[];
        $model = CouponModel::getPageData($input_data);
        foreach ($model as $item){
            array_push($list,$item->apiNormalInfo());
        }

        return $this->_resData(1,lang('tip_get_success'),['list'=>$list,'total'=>$model->total(),'total_page'=>$model->lastPage()]);
    }



    public function receive()
    {
        $input_data = input();
        try {
            $user_model = $this->user_model;
            CouponModel::getCoupon($user_model,$input_data);
        } catch (\Exception $e) {
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1, lang('tip_get_success') );
    }



    public function myCoupon()
    {
        $input_data = input();
        $input_data['uid'] = $this->user_id;
        $list = [];
        $info = CouponUserModel::getPageData($input_data)->each(function($item)use(&$list){
            array_push($list,$item->apiNormalInfo());
        });
        return $this->_resData(1,lang('tip_get_success'),['list'=>$list,'total'=>$info->total(),'total_page'=>$info->lastPage()]);
    }



}