<?php
namespace app\controller;


use app\common\model\OrderModel;
use app\common\model\PlatformNotifyModel;

class Pay extends Common
{
    public function notify()
    {
        cache('Pay-notify',input());
        dump(input());
        echo 'success';exit;
    }
    public function notify_paypal()
    {
        $input_data = input();
        cache('notify_paypal',input());
        try{
            if(!empty($input_data)){
                $cond_id = $input_data['order_id']??'';
                $cond_no = $input_data['orderNo']??'';
                PlatformNotifyModel::record('paypal',OrderModel::class,$cond_id,$cond_no,$input_data);
            }
        }catch (\Exception $e){
            cache('notify_paypal_err',$e->getMessage());
            \think\facade\Log::write(sprintf("notify_asiabill-err:%s;;data:%s",$e->getMessage() ,json_encode($input_data)) ,'-------input-----');
        }
        echo 'success';exit;
    }
    public function notify_asiabill()
    {
        $input_data = input();
        cache('notify_asiabill',$input_data);
        try{
            if(!empty($input_data)){
                $cond_id = $input_data['order_id']??'';
                $cond_no = $input_data['orderNo']??'';
                PlatformNotifyModel::record('asiabill',OrderModel::class,$cond_id,$cond_no,$input_data);
            }
        }catch (\Exception $e){
            \think\facade\Log::write(sprintf("notify_asiabill-err:%s;;data:%s",$e->getMessage() ,json_encode($input_data)) ,'-------input-----');
        }
        echo 'success';exit;
    }

    public function notifyInfo()
    {
        dump(cache('notify_paypal'));
        dump(cache('notify_paypal_err'));
        dump(cache('notify_asiabill'));
//        dump(cache('Pay-notify'));
    }

    //微信支付回调
    public function wechatNotify()
    {
        cache('wechatNotify',input());
        $data = \app\common\service\WechatV3Pay::notify(input());
        return json_encode($data);
    }

    //支付宝支付回调
    public function alipayNotify()
    {
        \think\facade\Log::write("alipayNotify-notify:" .json_encode(input()),'-------input-----');
        \think\facade\Log::write("alipayNotify-notify:" .file_get_contents("php://input"),'-----file_get_contents-----');
        return \app\common\service\Alipay::notify();
    }
}