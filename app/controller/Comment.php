<?php
namespace app\controller;


use app\common\model\CommentModel;
use app\common\model\CommentPraiseModel;
use app\common\model\OrderGoodsModel;

class Comment extends Common
{
    protected $open_action_validate = true;

    protected $ignore_action=['lists','info'];

    //订单评论信息
    public function lists()
    {
        $input_data = input();
        $input_data['status'] = 1;
        $input_data['check_type'] = 0;
        $checkMode = $input_data['checkMode']??'';

        if(!empty($this->user_id)){ //点赞用户id
            $input_data['check_praise_uid'] = $this->user_id;
        }

        if($checkMode=='self'){
            $input_data['check_user_id'] = $this->user_id;
        }

        $list = [];
        $info = CommentModel::getPageData($input_data)->each(function($item) use (&$list){
            array_push($list,$item->apiNormalInfo());
        });
        return $this->_resData(1,'获取成功',[
            'list'=>$list,
            'total'=>$info->total(),
            'total_page'=>$info->lastPage(),
        ]);
    }

    public function goodsLists()
    {

    }


    public function info()
    {
        $input_data = input();
        $data = CommentModel::getCommentInfo($input_data);
        return $this->_resData(1,'获取成功',$data);
    }

    public function praise()
    {
        $input_data = input();
        try{
            $state = CommentPraiseModel::praise($this->user_model, $input_data);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,$state==1 ? "点赞成功":'取消点赞',[
            'state'=>$state
        ]);
    }

    //评论
    public function comment()
    {
        $input_data = input();
        try{
            CommentModel::comment($this->user_model, $input_data);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,"感谢您的评价");
    }
    //订单评论信息
    public function orderGoodsDetail()
    {
        list($order,$goods_list) = CommentModel::orderGoodsDetail($this->user_model,input());
        return $this->_resData(1,lang('tip_get_success'),[
            'order'=>$order,
            'goods_list'=>$goods_list,
        ]);
    }


    //评论
    public function orderGoodsComment()
    {
        $input_data = input();
        try{
            CommentModel::OrderGoodsComment($this->user_model, $input_data);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,lang('tip_get_success'));
    }

    public function goodsCommentInfo()
    {
        $input_data = input();
        $input_data['goods_id'] = (int)input('goods_id');
        $navPage = input('navPage');
        if($navPage=='selfComment'){
            $input_data['uid'] = $this->user_id;
        }
        $info = CommentModel::getCommentInfo($input_data);


        return $this->_resData(1,lang('tip_get_success'),[
            'info'=>$info,
        ]);
    }

    public function goodsCommentLists()
    {
        $input_data = input();
        $input_data['uid'] = $this->user_id;

        $list =[];

        $model = OrderGoodsModel::commentLists($input_data)->each(function($item)use(&$list){
            array_push($list,$item->apiNormalInfo());
        });


        return $this->_resData(1,lang('tip_get_success'),['list'=>$list,'total'=>$model->total(),'total_page'=>$model->lastPage()]);
    }


    //我的评论
    public function selfLists()
    {
        $input_data = input();
        $input_data['status'] = 1;
        $input_data['uid'] = $this->user_id;
        $input_data['status'] = 1;

        $list =[];

        $model = CommentModel::getPageData($input_data)->each(function($item,$index)use (&$list){
            array_push($list,$item->apiNormalInfo());
        });


        return $this->_resData(1,lang('tip_get_success'),['list'=>$list,'total'=>$model->total(),'total_page'=>$model->lastPage()]);
    }

    //评论删除
    public function selfDel()
    {
        try{
            CommentModel::selfDel($this->user_model,input());
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage());
        }
        return $this->_resData(1,"操作成功");
    }
}