<?php

namespace app\controller;

use app\common\model\ContentCollectModel;
use app\common\model\UserLogsModel;
use app\common\model\UserModel;
use think\facade\Session;

class User extends Common
{
//    protected $open_action_validate = true;
//    protected $ignore_action = [];

    //获取用户信息
    public function get_user_info()
    {
        try {
            $info = UserModel::getUserInfo();
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1, lang('tip_get_success'), $info);
    }

    public function modPwd()
    {
        try {
            $this->user_model->modPwd(input());
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1, lang('tip_opt_success'));
    }

    //修改信息
    public function modify()
    {
        try {
            $this->user_model->modifyInfo(input());
        } catch (\Exception $e) {
            return $this->_resData(0, $e->getMessage());
        }
        return $this->_resData(1, lang('tip_opt_success'));
    }

    public function moneyLogs()
    {
        $input_data = input();
        $list = [];
        //日志记录
        $input_data['uid'] = $this->user_id;

        $info = UserLogsModel::getPageData($input_data);
        foreach ($info as $item){
            array_push($list,$item->apiNormalInfo());
        }
        return $this->_resData(1, lang('tip_get_success'),[
            'list'=>$list,
            'total'=>$info->total(),
            'total_page'=>$info->lastPage()
        ]);
    }


    public function order()
    {
//        dump(input());exit;
        return view('order');
    }

}
