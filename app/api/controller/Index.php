<?php

namespace app\api\controller;

use app\common\model\GoodsCateModel;
use app\common\model\ImageModel;
use app\common\model\OrderModel;
use app\common\model\SysSettingModel;
use app\common\model\WebMenuModel;
use app\common\model\WgtModel;
use app\common\service\MaBangErp;
use think\facade\View;

class Index extends Common
{
    public function index()
    {
//        sleep(3);
        return $this->_resData(0,'入口异常');
    }

    public function test()
    {

    }



    //协议入口  //api/index/webProtocol?type=tipProtocol
    public function webProtocol()
    {
        $type = input('type','','trim');
        $content = SysSettingModel::getContent($type);
        return view('/protocol',[
            'title'=> isset(SysSettingModel::$protocol_info[$type])?SysSettingModel::$protocol_info[$type]['name']:config('app.app_name'),
            'content'=>$content,
        ]);
    }


    //检测版本更新
    public function checkUpdate()
    {
        $mode = input('mode');
        $platform = input('platform');
        $name = input('name');

        $type = $mode == 'master' ? 1 : 0 ;
        //查找最后一条记录
        $last_model = WgtModel::where(['status' => 1, 'type' => $type, 'platform' => 0])->order('id desc')->find();
        if ($platform == 'ios') {
            $ios_model = WgtModel::where(['status' => 1, 'type' => $type, 'platform' => 2])->order('id desc')->find();
            if (!empty($ios_model) && $ios_model['id'] >= $last_model['id']) {
                $last_model = $ios_model;
            }
        } elseif ($platform == 'android') {
            $android_model = WgtModel::where(['status' => 1, 'type' => $type, 'platform' => 1])->order('id desc')->find();
            if (!empty($android_model) && $android_model['id'] >= $last_model['id']) {
                $last_model = $android_model;
            }
        }
//        dump($last_model->toArray());exit;
        $version = input('version');
        $wgtUrl = '';//更新组件
        $pkgUrl = '';//更新包
        $update = false;
        $msg = '';
        if ($last_model['version'] > $version) {
            $msg = "发现新版本";
            $update = true;
            $wgtUrl = $last_model['path'];
        }

        $data = [
            'msg' => $msg,
            'update' => $update,
            'platform' => $platform,
            'content' => empty($last_model['content']) ? '' : $last_model['content'],
            'wgtUrl' => $wgtUrl,
            'pkgUrl' => $pkgUrl,//更新包
        ];
        return $this->_resData(1, 'success', $data);
    }

    public function data()
    {
        $data = [
            'app_auth_state' => SysSettingModel::getAppAuthState(), //应用审核状态
            'open_app_state'=> 0,    //app开发状态
        ];

        //轮播图
        $data['follow_image'] = ImageModel::getNormalList(0);
        //icon
        $data['nav'] = WebMenuModel::getMenu();

        return $this->_resData(1, "获取成功", $data);
    }


    //发送短信
    public function sendSms()
    {

        $type = $this->request->param('type','');
        $phone = $this->request->param('phone','','trim');
        if(app()->http->getName()=='api' && $type==6 && !isset($_SERVER['HTTP_USER_TOKEN'])){
            return $this->_resData(1,'发送成功.',[]);
        }

        try{
            \app\common\model\SmsModel::send($type,$phone);
        }catch (\Exception $e){
            return $this->_resData(0,$e->getMessage(),[]);
        }
//        dump(123);exit;
        return $this->_resData(1,'发送成功',[]);
    }

}
