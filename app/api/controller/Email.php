<?php

namespace app\api\controller;

use app\common\model\GoodsCateModel;
use app\common\model\OrderModel;
use app\common\model\PlatformEmailTempModel;
use app\common\service\MaBangErp;
use think\facade\View;

class Email extends Common
{
    public function index()
    {
//        sleep(3);
        return $this->_resData(0,'入口异常');
    }

    public function preview()
    {
        $id = input('id');
        $order_id = input('order_id');

        $order_model = OrderModel::find($order_id);
        $item = PlatformEmailTempModel::find($id);
        //发送邮件
        list($status_name,$handle_action) =empty($order_model) ? null : $order_model->getStatusName();
        $logisticInfo = empty($order_model) ? null : $order_model->linkLogistics;
        $addrInfo = empty($order_model) ? null : $order_model->linkAddr;
        //今天成交了多少单
        $check_where = [];
        $check_where[] =['is_pay','=',0];
        $check_where[] =['create_time','>',strtotime(date('Y-m-d'))];
        $today_order_count = OrderModel::where($check_where)->count();
        $back_money = 0;
        $body = View::display($item['content'],[
            'orderModel'=>$order_model,
            'addrInfo'=>$addrInfo,
            'logisticInfo'=>$logisticInfo,
            'status_name'=>$status_name,
            'handle_action' => $handle_action,
            'today_order_count' => $today_order_count,
            'back_money' => $back_money,
        ]);
        return $body;

    }


}
