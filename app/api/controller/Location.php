<?php

namespace app\api\controller;


use app\common\model\PlatformLocationWordModel;
use app\common\model\SysSettingModel;

class Location extends Common
{
    //系统设置的地址信息
    public function sysLists()
    {
        $list = PlatformLocationWordModel::getNormalList();
        return $this->_resData(1,'获取成功',[
            'list'=>$list,
            'hot_list'=>SysSettingModel::getContent('normal','hot_location_comma'),
        ]);
    }


}
