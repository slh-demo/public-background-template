<?php

namespace app\api\controller;

use app\common\model\GoodsCateModel;
use app\common\model\OrderLogisticsModel;
use app\common\model\OrderModel;
use app\common\model\PlatformMbNotifyModel;
use app\common\service\MaBangErp;

class Store extends Common
{

    public function callback()
    {
        $input_data = input();
        PlatformMbNotifyModel::record($input_data);
        if(isset($input_data['itemList'])){
            if(isset($input_data['logisticsCode'])){ //物流
                OrderLogisticsModel::mbCallback($input_data);
            }
        }
        cache('store_callback',input());
        return $this->_resThirdData(999,'empty');
    }

    public function showCallback()
    {
        dump(cache('store_callback'));
    }


}
