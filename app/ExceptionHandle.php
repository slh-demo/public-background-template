<?php
namespace app;

use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\Handle;
use think\exception\HttpException;
use think\exception\HttpResponseException;
use think\exception\ValidateException;
use think\Response;
use Throwable;

/**
 * 应用异常处理类
 */
class ExceptionHandle extends Handle
{
    /**
     * 不需要记录信息（日志）的异常类列表
     * @var array
     */
    protected $ignoreReport = [
        HttpException::class,
        HttpResponseException::class,
        ModelNotFoundException::class,
        DataNotFoundException::class,
        ValidateException::class,
    ];

    /**
     * 记录异常信息（包括日志或者其它方式记录）
     *
     * @access public
     * @param  Throwable $exception
     * @return void
     */
    public function report(Throwable $exception): void
    {
        // 使用内置的方式记录异常日志
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @access public
     * @param \think\Request   $request
     * @param Throwable $e
     * @return Response
     */
    public function render($request, Throwable $e): Response
    {
        // 添加自定义异常处理机制
        // 添加自定义异常处理机制
        if($request->isAjax() || app()->http->getName()=='api' || app()->http->getName()=='master'){
            return json(['code'=>$e->getCode(),'msg'=>$e->getMessage()]);
        }

        //如果用户未登录
        if($e->getCode()==-1){
            if(app()->http->getName()=='chat'){
                return redirect(url('index/login'));
            }else{
                return redirect(url('index/index'));

            }
        }

        // 其他错误交给系统处理
        if(!app()->isDebug() && in_array($e->getCode(),[400,404]) &&  (app()->http->getName()=='index' || app()->http->getName()=='m')){
            return view('/error',[
                'code'=>$e->getCode(),
                'message'=>$e->getMessage()
            ]);
        }

        // 其他错误交给系统处理
        if(!app()->isDebug() && in_array($e->getCode(),[400,404]) && app()->http->getName()=='admin'){
            return view('/error',[
                'code'=>$e->getCode(),
                'message'=>$e->getMessage()
            ]);
        }
        // 其他错误交给系统处理
        return parent::render($request, $e);
    }
}
