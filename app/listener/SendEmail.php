<?php
namespace app\listener;

use think\facade\Db;

class SendEmail
{
    public function handle()
    {
        if(!$this->_checkRunTime()){
            return ;
        }


        try{
            \app\common\model\EmailLogsModel::sendEmail();
        }catch (\Exception $e){
            \think\facade\Log::write('发送邮箱处理异常:'.$e->getMessage());
        }

    }


    //检测是否到了执行的时间
    //每分钟执行一次
    private function _checkRunTime()
    {
        $is_check = false;
        $cache_name = 'sendEmail_hehe';
        if(!cache($cache_name)){
            cache($cache_name, date('Y-m-d H:i:s'), 60);
            $is_check = true;
        }
        return $is_check;

    }
}