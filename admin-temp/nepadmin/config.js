layui.define(function (exports) {
    exports('conf', {
        //容器ID
        container: 'app',
        //容器内容ID
        containerBody: 'app-body',
        //版本号
        v: layui.cache.version,
        //记录nepadmin文件夹所在路径
        base: layui.cache.base,
        css: layui.cache.base + 'css/',
        //视图所在目录
        views: layui.cache.base + 'views/',

        //是否开启选项卡
        viewTabs: true,
        //显示页面加载条
        viewLoadBar: true,
        //公用加载的样式
        style: [
            layui.cache.base + "css/admin.css"
        ],
        //是否开启调试模式，开启的话接口异常会抛出异常 URL信息
        debug: layui.cache.debug,
        //网站名称
        name: '后台管理系统',
        //默认视图文件名
        entry: '/index',
        //视图文件后缀名
        engine: '.html',
        eventName: 'nepadmin-event',
        //本地存储表名
        tableName: 'nepadmin',
        //全局设置 headers 信息
        requestHeaders: {

        },
        //request 基础URL 如 http://192.168.23.133:9501/api/
        requestUrl: '',
        //独立页面路由，可随意添加（无需写参数）
        indPage: [
            '/user/login', //登入页
            '/user/reg', //注册页
            '/user/forget' //找回密码
        ],
        //登录页面，当未登录或登录失效时进入
        loginPage: '/user/login',
        //登录 token 名称，request 请求的时候会带上此参数到 header
        tokenName: 'user-token',
        //是否要强制检查登录状态， 使用tokenName进行登录验证，不通过的话会返回 loginPage 页面
        loginCheck: true,
        //根据服务器返回的 HTTP 状态码检查登录过期，设置为false不通过http返回码检查
        logoutHttpCode: '401',
        //全局自定义响应字段
        response: {
            //数据状态的字段名称
            statusName: 'code',
            statusCode: {
                //数据状态一切正常的状态码
                ok: 1,
                //通过接口返回的登录过期状态码
                logout: -1
            },
            msgName: 'msg', //状态信息的字段名称
            dataName: 'data', //数据详情的字段名称
            countName: 'count' //数据条数的字段名称，用于 table
        },
        //全局 table 配置
        //参数请参照 https://www.layui.com/doc/modules/table.html
        table: {
            page: true,
            size: 'lg',
            skin: 'line',
            //每页显示的条数
            limit: 20,
            //是否显示加载条
            loading: true,
            //用于对分页请求的参数：page、limit重新设定名称
            request: {
                pageName: 'page', //页码的参数名称，默认：page
                limitName: 'limit' //每页数据量的参数名，默认：limit
            },
            parseData: function (res) {
                var data = res.data || {}
                console.log('-----------', data)
                return {
                    "code": res.code,
                    "msg": res.msg,
                    "count": data.total || 0,
                    "data": data.list || [],
                };
            }
        },
        //第三方扩展
        extend: {
            //后台根据业务需求扩展的方法
            helper: 'lay/extends/helper',
            //生成二维码
            qrcode: 'lay/extends/qrcode',
            //生成 MD5 加密
            md5: 'lay/extends/md5',
            //生成图表
            echarts: 'lay/extends/echarts',
            echartsTheme: 'lay/extends/echartsTheme',
            //复制内容到剪贴板
            clipboard: 'lay/extends/clipboard',
            // select多选
            formSelects: 'lay/extends/formSelects-v4',
            //拖拽
            vuedraggable: 'lay/extends/vuedraggable',
            //文件上传
            exportExcel:'views/components/exportExcel',
            //文件上传
            fileUpload:'views/components/fileUpload',
            //excel
            //状态切换
            mySwitchStatus: 'views/components/mySwitchStatus',
            //删除按钮
            myBtnSubmit: 'views/components/myBtnSubmit',
            //删除按钮
            myBtnDel: 'views/components/myBtnDel',
            //复制按钮
            myBtnCopy: 'views/components/myBtnCopy',
            //table checkBox
            myElTableColumn: 'views/components/myElTableColumn',
            mixinsTableCheckbox: 'views/components/mixinsTableCheckbox',
        },
        //左侧导航栏
        nav: [

            {
                "auth_rules": "users",
                "auth_name": "用户管理",
                "auth_icon": "layui-icon-user",
                "childs": [
                    {
                        "auth_rules": "/user/index",
                        "auth_name": "用户列表",
                    },
                ]
            },
            {
                "auth_rules": "users",
                "auth_name": "商家管理",
                "auth_icon": "layui-icon-user",
                "childs": [
                    {
                        "auth_rules": "/merchant/index",
                        "auth_name": "商家列表",
                    },
                ]
            },
            {
                "auth_rules": "",
                "auth_name": "产品信息",
                "auth_icon": "layui-icon-filedone",
                "childs": [
                    {
                        "auth_rules": "/goods/cate",
                        "auth_name": "产品分类",
                    },
                    {
                        "auth_rules": "/goods/index",
                        "auth_name": "产品列表",
                    },

                    {
                        "auth_rules": "/platform/ad",
                        "auth_name": "广告管理",
                    },
                    {
                        "auth_rules": "/comment/index",
                        "auth_name": "评论列表",
                    },
                ]
            },

            {
                "auth_rules": "order",
                "auth_name": "订单管理",
                "auth_icon": "layui-icon-user",
                "childs": [
                    {
                        "auth_rules": "/order/index",
                        "auth_name": "订单列表",
                    },
                ]
            },
            {
                "auth_rules": "platform",
                "auth_name": "平台设置",
                "auth_icon": "layui-icon-user",
                "childs": [
                    {
                        "auth_rules": "/platform/location-word",
                        "auth_name": "地址列表",
                    },
                ]
            },
            {
                "auth_rules": "",
                "auth_name": "内容管理",
                "auth_icon": "layui-icon-filedone",
                "childs": [

                    {
                        "auth_rules": "/protocol/info/type=tipProtocol",
                        "auth_name": "隐私协议",
                    },
                    {
                        "auth_rules": "/protocol/info/type=userProtocol",
                        "auth_name": "用户协议",
                    },

                ]
            },
            {
                "auth_rules": "/web/template",
                "auth_name": "应用管理",
                "auth_icon": "layui-icon-filedone",
                "childs": [
                    {
                        "auth_rules": "/web/menu",
                        "auth_name": "首页菜单栏",
                    },
                    // {
                    //     "auth_rules": "/web/template",
                    //     "auth_name": "模板管理",
                    // },
                ]
            },
            {
                "auth_rules": "/system/setting",
                "auth_name": "系统设置",
                "auth_icon": "layui-icon-release",
                "childs": [
                    {
                        "auth_rules": "/system/setting",
                        "auth_name": "通用设置",
                    },
                    {
                        "auth_rules": "/system/image",
                        "auth_name": "轮播图管理",
                    },
                    {
                        "auth_rules": "/system/manager",
                        "auth_name": "管理员管理",
                    },
                    {
                        "auth_rules": "/system/role",
                        "auth_name": "角色管理",
                    },
                ]
            }

        ]
    })
})