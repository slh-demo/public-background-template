layui.extend({
}).define(function(exports) {
    var admin = layui.admin
    console.log(admin)
    var self = {
        data:{
            upload_info: {} ,
            uploadAccept: 'image/*' ,
            uploadLoadingInstance:undefined,
        },
        created:function(){
            this.getUploadInfo()
        },
        computed : {

        },
        methods: {
            get_upload_data:function (type){
                const upload_info_data = this.upload_info.data||{};
                return Object.assign(upload_info_data,{type:type})
            },
            //初始化图片上传
            getUploadInfo: function () {
                const that = this
                admin.uploadInfo({}, function (upload_info) {
                    that.upload_info = upload_info
                })
            },
            onBeforeUploadFile:function(){
                console.log('----------')
                this.uploadLoadingInstance = this.$loading({text:'上传中 ....'})

            },
            onProgress:function(event, file, fileList){
                var percent = event.percent.toFixed(2)
                this.uploadLoadingInstance.text="上传进度（"+percent+"%）"
            },
            handleSuccessFile:function(res, obj, type){
                console.log('22222222',res, obj, type)
                var that = this
                this.uploadLoadingInstance.text="上传完成"
                setTimeout(function(){ that.uploadLoadingInstance.close() },500)
                const data = res.data||{}
                if(res.code!==1){
                    this.$message({ message: res.msg, type: 'error' });
                }else{
                    var method_name = 'handleSuccessFile_'+type;
                    this[method_name](res)
                }
            },
        }
    }

    exports('fileUpload', self)
})
