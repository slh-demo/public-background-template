layui.define(function(exports) {
    var firstComponent = Vue.component("firstComponent",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            }
        },
        template: '<h3>{{ name }}</h3>'
    })


    exports('firstComponent', firstComponent)
})
