layui.extend({admin:'lay/modules/admin'}).define(['admin','conf'],function(exports){
    //解决 IE8 不支持console
    window.console = window.console || (function () {
        var c = {}; c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile
        = c.clear = c.exception = c.trace = c.assert = function () { };
        return c;
    })();
    layui.admin.initPage();
    exports('index',{});
});

//初始化模板变量
Vue.prototype.$tempDataInit = function(temp,obj,item){
    for (var key in temp) {
        var value_type = typeof temp[key]
        var value = item[key]===undefined ? temp[key] : item[key];

        var default_value = undefined
        if( value_type instanceof Array ) {
            default_value = []
        }else if(value_type instanceof Object){
            default_value = {}
        }else if(value_type === 'string' ) {
            default_value = ""
            value = value + ""
        }else if(value===0){
            default_value = 0
        }
        obj[key] = value||default_value

    }

}