layui.extend({
    mapGd:'views/components/mapGd',
    cityPickerChoose:'views/components/cityPickerChoose',
    fileUpload:'views/components/fileUpload'
}).define(["fileUpload","mapGd","cityPickerChoose",'vuedraggable',"myBtnSubmit"],function(exports,fileUpload,mapGd,cityPickerChoose,vuedraggable,myBtnSubmit) {
    var admin = layui.admin

    // console.log("fileUpload",layui.fileUpload)
    var vue_temp_data = {
        id:'',
        img:[],
        name:'',
        phone:'',
        contact_name:'',
        password:'',
        intro:'',
        logo:'',
        company_cert_img:'',
        status:'1',
        addr:'',
        location_addr:'',
        lng: '',
        lat: '',
    };
    exports('createMerchant', Vue.component("createMerchant",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            }
        },
        components:{
            'vuedraggable':layui.vuedraggable,
            'file-upload': layui.fileUpload,
            'map-gd' : layui.mapGd,
            'city-picker-choose' : layui.cityPickerChoose,
            'my-btn-submit': layui.myBtnSubmit,
        },
        data:function() {
            return {
                dialogVisible:false,
                title: '新增',
                pageObj:{},
                to_show_index:undefined,
                upload_info:{},
                type_list:[],
                ser_time:[],
                ser_time_items:[],
                current_ser_index:0,
                temp:Object.assign({},vue_temp_data),
                rules: {
                    name: [ { required: true, message: '请输入用户名', trigger: 'blur' }],
                    phone: [ { required: true, message: '请输入登录帐号', trigger: 'blur' }],
                },
            }
        },
        computed:{
            get_current_ser_obj :function(){
                return this.ser_time[this.current_ser_index]||{}
            },
            get_current_ser_list:function(){
                return this.get_current_ser_obj.range||[]
            },
        },
        mounted:function(){
            this.getBaseInfo()
        },
        methods: {

            getBaseInfo:function(){
                const that = this
                admin.post({
                    url:'/admin/index/defInfo',
                    data:{type:'master'},
                    success:function(res){
                        const data = res.data||{}
                        that.ser_time = data.ser_time||[]
                        that.ser_time_items = data.ser_time_items||[]
                    }
                })
            },
            showDialog:function(item,pageObj){
                this.pageObj = pageObj
                if(item && item.id){
                    this.title = '编辑'
                }else{
                    this.title = '新增'
                }

                var show_img_list = []

                this.$tempDataInit(vue_temp_data, this.temp, item)

                this.dialogVisible = true

                this.$nextTick(function(){
                    this.ser_time = item.ser_time|| this.ser_time
                    this.$refs['mapGd'].setLocation( this.temp.lng,this.temp.lat )
                    this.$refs['cityPickerChoose'].setLocation( this.temp['location_addr'] )
                    // this.$refs['fileUploadIntroImg'].setFileList( show_img_list )
                })
            },
            handleSubmit:function(){
                const that = this
                admin.post({
                    url:'/admin/users/merchantModel_add',
                    data:Object.assign(this.temp,{locationAddr:this.$refs['cityPickerChoose'].getLocation()}),
                    success:function(res){
                        const data = res.data||{}
                        that.$message({ message: res.msg, type: 'success' });
                        setTimeout(function(){
                            that.dialogVisible = false
                            that.pageObj._initData()
                        },500)
                    }
                })
            },

            onHandleSuccessFile:function(res,payload){
                console.log('onHandleSuccessFile',res,payload)
                var data = res.data||{}
                this.temp.logo = data.key||''

            },
            handleImgRemove:function(index){
                this.temp.img.splice(index,1)
                this.imgList.splice(index,1)
            },
            handlePictureCardPreview:function(index){

                this.to_show_index = index
                console.log(">>>>>放大",index,this.to_show_index)
                this.$refs.myImg.showViewer = true
                return false
            },

            onHandleImgChange:function(imgs,){
                console.log('onHandleImgChange',imgs)

                this.temp.img = imgs

            },
            onHandleSuccesscompany_cert_img:function(res,payload){
                console.log('onHandleSuccessFile',res,payload)
                var data = res.data||{}
                this.temp.company_cert_img = data.key||''

            },
            locationSearch:function(){
                var locationAddr = this.$refs['cityPickerChoose'].getLocation().replaceAll(/\//g,"");
                if(locationAddr){
                    var location =  locationAddr + " "+this.temp.addr
                    console.log(location)
                    this.$refs['mapGd'].locationSearch(location)
                }else{
                    alert('请选择地址')
                }

            },
            onMapLngLat:function(lng,lat){
                this.temp.lng = lng
                this.temp.lat = lat
            },


            closeDialog:function(e,res){
                this.dialogVisible = false;
                if(res){
                    this.pageObj._initData()
                    // layui.view.tab.refresh()
                }
            },
            //拖拽结束事件
            onDraggableEnd:function(e){
                console.log(e)
                const oldIndex = e.oldIndex
                const oldImg = this.temp.img[oldIndex]
                const newIndex = e.newIndex
                this.temp.img.splice(oldIndex,1)
                this.temp.img.splice(newIndex+1,0,oldImg)
                console.log(">>>>拖拽结束事件",oldIndex,newIndex,this.temp.img)
                // var fun=function(arr,index1,index2){
                //     arr[index1] = arr.
                //     return arr;
                // }
                // const that = this
                // var new_list = []
                // this.temp.img.map(function(item){
                //     for(var i in that.imgList){
                //
                //         if(item == that.imgList[i].key){
                //             console.log(">>>>>item",item,that.imgList[i].key)
                //             new_list.push(that.imgList[i])
                //             continue
                //         }
                //     }
                //
                // })
                // this.imgList = new_list
                // console.log(">>>>拖拽结束事件end",this.imgList)
            },
        },
        template: '<el-dialog\n' +
            '    id="components-app"\n' +
            '    :title="title"\n' +
            '    :append-to-body="true"\n' +
            '    @close="closeDialog"'+
            '    :close-on-click-modal="false"\n' +
            '    :visible.sync="dialogVisible"\n' +
            '    top="1vh"\n' +
            '    width="85%"\n' +
            '>\n' +
            '    <el-form ref="form" :rules="rules" :model="temp" label-width="180px">\n' +
            '        <el-row>\n' +
            '            <el-col :span="10">\n' +
            '                <el-form-item label="商家名" prop="name">\n' +
            '                    <el-input v-model="temp.name"></el-input>\n' +
            '                </el-form-item>\n' +
            '                <el-form-item label="联系人" prop="contact_name">\n' +
            '                    <el-input v-model="temp.contact_name"></el-input>\n' +
            '                </el-form-item>\n' +
            '                <el-form-item label="手机号" prop="phone">\n' +
            '                    <el-input type="number" v-model="temp.phone"></el-input>\n' +
            '                </el-form-item>\n' +
            '                <el-form-item label="密码" prop="password">\n' +
            '                    <el-input type="password" v-model="temp.password"></el-input>\n' +
            '                </el-form-item>\n' +

            '                <el-form-item label="状态" prop="status">\n' +
            '                    <el-radio  v-model="temp.status" label="1" >正常</el-radio>\n' +
            '                    <el-radio  v-model="temp.status" label="2" >关闭</el-radio>\n' +
            '                </el-form-item>\n' +

            '            </el-col>\n' +
            '            <el-col :span="14">\n' +
            '                <el-form-item label="logo（500*500）" prop="img">\n' +
            '                    <file-upload    :upload-success="onHandleSuccessFile">\n' +
            '                        <img v-if="temp.logo" :src="temp.logo" fit="fit" class="avatar" >\n' +
            '                        <i v-else class="el-icon-plus avatar-uploader-icon"></i>\n' +
            '                    </file-upload>\n' +
            '                </el-form-item>\n' +

            '                <el-form-item label="商品图片" prop="img">\n' +
            '                      <div class="display-flex" v-if="dialogVisible">\n' +
            '                        <ul class="el-upload-list el-upload-list--picture-card">\n' +
            '                            <draggable v-model="temp.img" chosen-class="chosen" force-fallback="true" group="people" animation="1000"  @end="onDraggableEnd">\n' +
            '                                <transition-group>\n' +
            '                                    <li tabindex="0" class="el-upload-list__item is-success" v-for="(item,index) in temp.img" :key="index"> \n' +
            '                                        <img :src="item" class="el-upload-list__item-thumbnail">\n' +
            '                                        <span class="el-upload-list__item-actions" >\n' +
            '                                            <span class="el-upload-list__item-preview"\n' +
            '                                              @click="handlePictureCardPreview(index)"\n' +
            '                                            >\n' +
            '                                              <i class="el-icon-zoom-in"></i>\n' +
            '                                            </span>\n' +
            '                                            <span class="el-upload-list__item-preview"\n' +
            '                                              @click="handleImgRemove(index)"\n' +
            '                                            >\n' +
            '                                              <i class="el-icon-delete"></i>\n' +
            '                                            </span>\n' +
            '                                        </span>\n' +
            '                                    </li>\n' +
            '                                </transition-group>\n' +
            '                            </draggable>\n' +
            '\n' +
            '                        </ul>\n' +
            '                    </div>'+
            '                    <file-upload ref="fileUploadImg" :show-file-list="false"  :upload-change="onHandleImgChange" >' +
            '                       <i class="el-icon-plus avatar-uploader-icon"></i>\n' +
            '                    </file-upload>\n' +
            '                </el-form-item>\n' +

            '                <el-form-item label="简介" prop="intro">\n' +
            '                    <el-input type="textarea" rows="5" v-model="temp.intro"></el-input>\n' +
            '                </el-form-item>\n' +
            '                <el-form-item label="城市" prop="intro">\n' +
            '                    <city-picker-choose ref="cityPickerChoose" />' +
            '                </el-form-item>\n' +
            '                <el-form-item label="详细地址" prop="addr">\n' +
            '                    <el-input   v-model="temp.addr">\n' +
            '                       <el-button size="small" type="primary"  slot="append"   @click="locationSearch">搜索</el-button>' +
            '                       </el-input>\n' +
            '                </el-form-item>\n' +
            '                <el-form-item label="定位" prop="intro">\n' +
            '                    <map-gd ref="mapGd" :showSearch="false"  :map-lng-lat="onMapLngLat" />' +
            '                </el-form-item>\n' +
            '            </el-col>\n' +
            '        </el-row>\n' +
            '\n' +
            '\n' +
            '    </el-form>\n' +
            '\n' +
            '    <span slot="footer" class="dialog-footer">\n' +
            '    <el-button @click="closeDialog">取 消</el-button>\n' +
            '    <my-btn-submit :data="temp"  :complete="closeDialog" url="/admin/index/merchantModel_add"/>\n' +
            '  </span>\n' +
            '   <el-image ref="myImg" id="show-icon" class="avatar" style="width: 0px;height: 0px;overflow: hidden;" :src="temp.img[to_show_index]" fit="fit"  :preview-src-list="temp.img"></el-image>\n' +
            '</el-dialog>'
    }))
})
