layui.extend({

}).define(["fileUpload","myBtnSubmit"],function(exports,fileUpload) {
    var admin = layui.admin

    // console.log("fileUpload",layui.fileUpload)
    var vue_temp_data = {
        id:'',
        type:0,
        name:'',
        url:'',
        img:'',
        status: '1',
        sort:100,
    };
    exports('createImage', Vue.component("createImage",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            }
        },
        components:{
            'file-upload': layui.fileUpload,
            'my-btn-submit': layui.myBtnSubmit,
        },
        data:function() {
            return {
                dialogVisible:false,
                pageObj:undefined,
                upload_info:{},
                image_list:[],
                temp:Object.assign({},vue_temp_data),
                rules: {
                    name: [ { required: true, message: '请输入图片名称', trigger: 'blur' }],
                },
            }
        },
        computed : {
            // get_upload_data:function (){
            //     const upload_info_data = this.upload_info.data||{};
            //     return Object.assign(upload_info_data,{type:'image'})
            // },
            get_image_size:function(){
                const obj = this.image_list[this.temp.type]||{}
                return '图片('+(obj.size||"750*400")+')'
            },
            get_title:function(){
                var title = "新增"
                if(this.temp.id){
                    title = '编辑'
                }
                return title
            },
        },
        created:function(){
            this.getBaseInfo()

        },
        mounted:function(){
        },
        methods: {

            getBaseInfo:function(){
                const that = this
                admin.post({
                    url:'/admin/index/defInfo',
                    data:{type:'image'},
                    success:function(res){
                        const data = res.data||{}
                        that.image_list = data.image_list||[]
                    }
                })
            },
            showDialog:function(item,pageObj){
                this.pageObj = pageObj
                this.$tempDataInit(vue_temp_data, this.temp, item)

                this.dialogVisible = true
                this.$nextTick(function(){

                })
            },

            closeDialog:function(e,res){
                this.dialogVisible = false;
                if(res){
                    this.pageObj._initData()
                    // layui.view.tab.refresh()
                }
            },
            onHandleSuccessFile:function(res,payload){
                console.log('onHandleSuccessFile',res,payload)
                var data = res.data||{}
                this.temp.img = data.key||''

            }
        },
        template: '<el-dialog\n' +
            '    id="components-app"\n' +
            '    :title="get_title"\n' +
            '    :append-to-body="true"\n' +
            '    :close-on-click-modal="false"\n' +
            '    :visible.sync="dialogVisible"\n' +
            '    width="60"\n' +
            '>\n' +
            '    <el-form ref="form" :rules="rules" :model="temp" label-width="140px">\n' +
            '        <el-form-item label="图片类型" prop="type">\n' +
            '            <el-select v-model="temp.type" placeholder="请选择图片类型">\n' +
            '                <el-option :label="item.name"  :value="item.value" v-for="(item,index) in image_list" :key="index"></el-option>\n' +
            '            </el-select>\n' +
            '        </el-form-item>\n' +
            '        <el-form-item label="图片名称" prop="name">\n' +
            '            <el-input v-model="temp.name"></el-input>\n' +
            '        </el-form-item>\n' +
            '        <el-form-item :label="get_image_size" prop="name">\n' +
            '            <file-upload   :upload-success="onHandleSuccessFile">\n' +
            '                <img v-if="temp.img" :src="temp.img" fit="fit" class="avatar" >\n' +
            '                <i v-else class="el-icon-plus avatar-uploader-icon"></i>\n' +
            '            </file-upload>\n' +
            '        </el-form-item>\n' +
            '        <el-form-item label="url" prop="url">\n' +
            '            <el-input  v-model="temp.url"></el-input>\n' +
            '        </el-form-item>\n' +
            '        <el-form-item label="排序" prop="sort">\n' +
            '            <el-input type="number" v-model="temp.sort"></el-input>\n' +
            '        </el-form-item>\n' +
            '        <el-form-item label="状态" prop="name">\n' +
            '            <el-radio  v-model="temp.status" label="1" >正常</el-radio>\n' +
            '            <el-radio  v-model="temp.status" label="2" >关闭</el-radio>\n' +
            '        </el-form-item>\n' +
            '\n' +
            '    </el-form>\n' +
            '\n' +
            '    <span slot="footer" class="dialog-footer">\n' +
            '    <el-button @click="closeDialog">取 消</el-button>\n' +
            '    <my-btn-submit :data="temp"  :complete="closeDialog" url="/admin/index/imageModel_add"/>\n' +
            '  </span>\n' +
            '</el-dialog>'
    }))
})
