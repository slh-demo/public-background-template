layui.extend({
}).define(["fileUpload","myBtnSubmit"],function(exports) {
    var admin = layui.admin

    var vue_temp_data = {
        id:'',
        pid:'',
        name:'',
        node:[],
        sort:'100',
        status: '1',
    };
    exports('createRole', Vue.component("createRole",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            }
        },
        components:{
            'file-upload': layui.fileUpload,
            'my-btn-submit': layui.myBtnSubmit,
        },
        data:function() {
            return {
                dialogVisible:false,
                pageObj:undefined,
                upload_info:{},
                role_list:[],
                temp: Object.assign({},vue_temp_data),
                node_list:[],
                rules: {
                },
            }
        },
        computed : {
            get_title:function(){
                var title = "新增"
                if(this.temp.id){
                    title = '编辑'
                }
                return title
            },
        },
        created:function(){
            this.getBaseInfo()

        },
        mounted:function(){
        },
        methods: {
            getBaseInfo:function(){
                const that = this
                admin.post({
                    url:'/admin/index/defInfo',
                    data:{type:'role'},
                    success:function(res){
                        const data = res.data||{}
                        that.node_list = data.nav||[]
                        that.role_list = data.role_list||[]
                    }
                })
            },
            showDialog:function(item,pageObj){
                this.pageObj = pageObj
                this.$tempDataInit(vue_temp_data, this.temp, item)
                this.dialogVisible = true

            },

            closeDialog:function(e,res){
                this.dialogVisible = false;
                if(res){
                    this.pageObj._initData()
                    // layui.view.tab.refresh()
                }
            },
            handleCheckAllChange:function(bool,item){
                console.log(bool,item)
                let that = this ;
                (item.childs||[]).map(function(child){
                    var rule = child.auth_rules
                    var location_index = that.temp.node.indexOf(rule)
                    console.log(rule,location_index)
                    if(bool){
                        if(location_index===-1){
                            that.temp.node.push(rule)
                        }
                    }else{
                        if(location_index>-1){
                            that.temp.node.splice(location_index,1)
                        }
                    }

                })

            },
            handleCheckChange:function(bool,child,item){
                var that = this
                var item_auth_rules_index = this.temp.node.indexOf( item.auth_rules)
                var some_state = false;
                if(bool){
                    some_state = true;
                }else{
                    var all_childs = item.childs||[]
                    for(var i=0;i<all_childs.length;i++){
                        var child_info = all_childs[i]
                        var location_index = that.temp.node.indexOf(child_info.auth_rules)
                        if(location_index>-1){
                            some_state = true
                        }
                        if(some_state){
                            break;
                        }
                    }
                }
                if(some_state){
                    if(item_auth_rules_index===-1){
                        this.temp.node.push(item.auth_rules)
                    }
                }else{
                    if(item_auth_rules_index>-1){
                        this.temp.node.splice(item_auth_rules_index,1)
                    }
                }

            },

            beforeUpload:function(res){
                console.log('---beforeUpload--',res)
            },
            handleSuccess:function(res){
                const data = res.data||{}
                if(res.code!==1){
                    this.$message({ message: res.msg, type: 'error' });
                }else{
                    this.temp.icon = data.key||''
                }
            }
        },
        template: '<el-dialog\n' +
            '        id="components-app"\n' +
            '        :title="get_title"\n' +
            '        :append-to-body="true"\n' +
            '        :close-on-click-modal="false"\n' +
            '        :visible.sync="dialogVisible"\n' +
            '        width="60%"\n' +
            '>\n' +
            '    <el-form ref="form" :rules="rules" :model="temp" label-width="100px">\n' +
            '        <!--<el-form-item label="分类" prop="rid">\n' +
            '            <el-select v-model="temp.pid" placeholder="请选择角色">\n' +
            '                <el-option :label="item.name"  :value="item.id" v-for="(item,index) in role_list" :key="index"></el-option>\n' +
            '            </el-select>\n' +
            '        </el-form-item>-->\n' +
            '        <el-form-item label="帐号" prop="name">\n' +
            '            <el-input v-model="temp.name"></el-input>\n' +
            '        </el-form-item>\n' +
            '        <el-form-item label="排序" prop="sort">\n' +
            '            <el-input v-model="temp.sort"></el-input>\n' +
            '        </el-form-item>\n' +
            '\n' +
            '        <el-form-item label="状态" prop="name">\n' +
            '            <el-radio  v-model="temp.status" label="1" >正常</el-radio>\n' +
            '            <el-radio  v-model="temp.status" label="2" >禁用</el-radio>\n' +
            '        </el-form-item>\n' +
            // '        <el-form-item label="权限配置" prop="name">\n' +
            // '            <el-checkbox-group v-model="temp.node">\n' +
            // '                <div v-if="node_list.length" v-for="(item,index) in node_list" :key="index">\n' +
            // '                    <el-checkbox :label="item.auth_rules" @change="handleCheckAllChange($event,item)" >{{item.auth_name}}</el-checkbox>\n' +
            // '                    <div style="margin: 15px 0;padding-left: 25px;display: flex" v-if="item.childs">\n' +
            // '                        <div style="width:80px;"></div>\n' +
            // '                        <div style="width:calc(100% - 80px);">\n' +
            // '                            <el-checkbox style="margin-bottom:10px;" v-for="(vo,ci) in item.childs" :key="ci" :label="vo.auth_rules"  @change="handleCheckChange($event,vo,item)"  >{{vo.auth_name}}</el-checkbox>\n' +
            // '                        </div>\n' +
            // '                    </div>\n' +
            // '\n' +
            // '                </div>\n' +
            // '            </el-checkbox-group>\n' +
            // '        </el-form-item>\n' +
            '\n' +
            '\n' +
            '    </el-form>\n' +
            '\n' +
            '    <span slot="footer" class="dialog-footer">\n' +
            '    <el-button @click="closeDialog">取 消</el-button>\n' +
            '    <my-btn-submit :data="temp"  :complete="closeDialog" url="/admin/index/sysRoleModel_add"/>\n' +
            '  </span>\n' +
            '</el-dialog>'
    }))
})
