layui.define(["fileUpload","myBtnSubmit"],function(exports) {
    var admin = layui.admin

    var vue_temp_data = {
        id: '',
        group_cate_id: '',
        name: '',
        account: '',
        password: '',
        status: '1',
    };
    exports('createManager', Vue.component("createManager",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            }
        },
        components:{
            'file-upload': layui.fileUpload,
            'my-btn-submit': layui.myBtnSubmit,
        },
        data:function() {
            return {
                dialogVisible: false,
                pageObj:undefined,
                upload_info: {},
                role_list: [],
                temp: Object.assign({},vue_temp_data),
                rules: {
                    rid: [{required: true, message: '请选择角色', trigger: 'blur'}],
                    name: [{required: true, message: '请输入管理员名', trigger: 'blur'}],
                    account: [{required: true, message: '请输入管理员帐号', trigger: 'blur'}],
                },
            }
        },
        computed:{
            get_title:function(){
                var title = "新增"
                if(this.temp.id){
                    title = '编辑'
                }
                return title
            },
        },
        mounted:function(){
            this.getBaseInfo()
        },
        methods: {
            getBaseInfo:function(){
                const that = this
                admin.post({
                    url:'/admin/index/defInfo',
                    data:{type:'manager'},
                    success:function(res){
                        const data = res.data||{}
                        that.role_list = data.role_list||[]
                    }
                })
            },

            showDialog:function(item,pageObj){
                this.pageObj = pageObj
                this.$tempDataInit(vue_temp_data, this.temp, item)
                this.temp.password = ""
                this.dialogVisible = true

            },
            closeDialog:function(e,res){
                this.dialogVisible = false;
                if(res){
                    this.pageObj._initData()
                    // layui.view.tab.refresh()
                }
            },

        },
        template: '<el-dialog\n' +
            '    id="components-app"\n' +
            '    :title="get_title"\n' +
            '    :append-to-body="true"\n' +
            '    :close-on-click-modal="false"\n' +
            '    :visible.sync="dialogVisible"\n' +
            '    width="60"\n' +
            '>\n' +
            '    <el-form ref="form" :rules="rules" :model="temp" label-width="180px">\n' +
            '        <el-form-item label="分类" prop="group_cate_id">\n' +
            '            <el-select v-model="temp.group_cate_id" placeholder="请选择管理员角色">\n' +
            '                <template  v-for="(item,index) in role_list">\n' +
            '                    <el-option :label="item.name"  :value="item.id"></el-option>\n' +
            '                    <el-option :label="child.name"  :value="item.id+\',\'+child.id" v-for="(child,child_index) in item.child_list" :key="child_index" style="margin-left: 20px"></el-option>\n' +
            '                </template>\n' +
            '            </el-select>\n' +
            '        </el-form-item>\n' +
            '        <el-form-item label="管理员名" prop="name">\n' +
            '            <el-input v-model="temp.name"></el-input>\n' +
            '        </el-form-item>\n' +
            '        <el-form-item label="管理员帐号" prop="account">\n' +
            '            <el-input v-model="temp.account"></el-input>\n' +
            '        </el-form-item>\n' +
            '        <el-form-item label="管理员密码" prop="password">\n' +
            '            <el-input type="password" v-model="temp.password"></el-input>\n' +
            '        </el-form-item>\n' +
            '\n' +
            '        <el-form-item label="状态" prop="name">\n' +
            '            <el-radio  v-model="temp.status" label="1" >正常</el-radio>\n' +
            '            <el-radio  v-model="temp.status" label="2" >禁用</el-radio>\n' +
            '        </el-form-item>\n' +
            '\n' +
            '    </el-form>\n' +
            '\n' +
            '    <span slot="footer" class="dialog-footer">\n' +
            '    <el-button @click="closeDialog">取 消</el-button>\n' +
            '    <my-btn-submit :data="temp"  :complete="closeDialog" url="/admin/index/sysManagerModel_add"/>\n' +
            '  </span>\n' +
            '</el-dialog>'
    }))
})
