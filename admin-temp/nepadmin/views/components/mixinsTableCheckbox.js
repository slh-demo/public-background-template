layui.extend({

}).define(['mySwitchStatus','myBtnDel','myBtnCopy','myElTableColumn'],function(exports) {
    var admin = layui.admin
    exports('mixinsTableCheckbox', Vue.component("mixinsTableCheckbox",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            },


        },
        data:function(){
            return {
                radio_select_mode : 'page',
                multipleSelection: [],
            }
        },

        components:{
            'my-switch-status' : layui.mySwitchStatus,
            'my-btn-del' : layui.myBtnDel,
            "my-btn-copy" : layui.myBtnCopy,
            "my-el-table-column" : layui.myElTableColumn,
        },
        mounted:function(){

        },
        computed : {
            get_multipleSelection_ids:function(){
                const ids = []
                this.multipleSelection.map(function(item){
                    ids.push(item.id)
                })
                return ids;
            }
        },
        methods: {
            changeInputEvent:function(params,e,field,url){
                console.log(e,field,url)
                var obj = {}
                obj[field] = e
                admin.post({
                    url:url
                    ,data: Object.assign(obj,params)
                    ,success: function (res) {
                        that[funName]()
                    }
                })
            },
            handleSelectionChange:function(val) {
                this.multipleSelection = val;
            },
            handleDialog:function(ref,params,url,title){
                if ( this.radio_select_mode==='page' && !this.get_multipleSelection_ids.length){
                    this.$message.error("请选中要修改的项目")
                    return
                }

                this.$refs[ref].showDialog(Object.assign({optModel:this.radio_select_mode,optQuery:this.listQuery},params),this.get_multipleSelection_ids,url,title,this)
            },
            handleTableCheckboxModifies:function(url,params,tip_content,funName){
                funName = funName||'_initData';
                const that = this;

                if( this.radio_select_mode==='page' && !this.get_multipleSelection_ids.length){
                    this.$message.error("请选中要修改的项目")
                    return
                }
                this.$confirm("提示", tip_content, { type: 'error' }).then(function(v){
                    admin.post({
                        url:url
                        ,data: Object.assign({ids:that.get_multipleSelection_ids ,optModel:that.radio_select_mode,optQuery:that.listQuery},params)
                        ,success: function (res) {
                            that[funName]()
                        }
                    })
                }).catch(function() {
                    console.log('cancel')
                })
            },
            handleTableCheckboxDelete:function(url,funName){
                funName = funName||'_initData';
                const that = this;

                if ( this.radio_select_mode==='page' && !this.get_multipleSelection_ids.length ) {
                    this.$message.error("请选中要删除的项目")
                    return
                }

                this.$confirm("提示", "是否删除选中项", { type: 'error' }).then(function(v){
                    admin.post({
                        url:url
                        ,data:{ids:that.get_multipleSelection_ids,optModel:that.radio_select_mode,optQuery:that.listQuery }
                        ,success: function (res) {
                            that[funName]()
                        }
                    })
                }).catch(function() {
                    console.log('cancel')
                })
            },

            tableListSortChange: function(data,fnc){
                console.log(data,fnc)
                const prop = data.prop
                const order = data.order
                this.listQuery.order_field =  data.prop
                this.listQuery.order_sort = data.order===null? '' :( data.order==="ascending"?'asc': 'desc')
                if(fnc instanceof Function){
                    fnc()
                }else{
                    this._initData()
                }
            },


        },
    }))
})
