layui.define(function (exports) {
    var admin = layui.admin

    exports('selectEmailTemp', Vue.component("selectEmailTemp", {
        // 在 JavaScript 中是 camelCase 的
        props: {
            name: {
                type: String,
                default: "string",
            },
            type:{
                type:String,
                default: "",
            },
        },
        data: function () {
            return {
                list:[],
                value:"",
            }
        },
        computed: {


        },
        created: function () {

        },
        mounted: function () {
            this.getBaseInfo()
        },
        methods: {
            setTags:function(data){
                this.value = data
            },
            getBaseInfo: function () {
                const that = this
                admin.post({
                    url:'/admin/index/defInfo',
                    data:{type:'email_temp_list',data:{type:this.type}},
                    success:function(res){
                        const data = res.data||{}
                        that.list = data.list||[]
                    }
                })
            },


        },
        template:  '                    <el-select v-model="value" filterable  placeholder="请选择模板">\n' +
            '                        <template  v-for="(item,index) in list" >\n' +
            '                            <el-option :label="item.name"  :value="item.id+\'\'" ></el-option>\n' +
            '                        </template>\n' +
            '                    </el-select>\n'
    }))
})
