layui.extend({

}).define(function(exports) {
    var admin = layui.admin
    exports('myBtnDel', Vue.component("myBtnDel",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            },
            fnc:{
                type: Function,
                default: function(){  return {} },
            },
            confirmTitle:{
                type: String,
                default:"提示",
            },
            confirmContext:{
                type: String,
                default:"确定删除选中的数据",
            },
            id:{
                type: [String,Number],
                default: 0,
            },
            url:{
                type: String,
                default:"",
            },

        },
        data:function(){
            return {
            }
        },
        mounted:function(){

        },
        computed : {

        },
        methods: {
            handleDelete:function(){
                const that = this
                this.$confirm(this.confirmContext, this.confirmTitle, { type: 'error' }).then(function(v){
                    admin.post({
                        url:that.url
                        ,data:{id:that.id }
                        ,success: function (res) {
                            that.fnc()
                        }
                    })
                }).catch(function() {
                    console.log('cancel')
                })
            },

        },
        template:  ' <el-button size="mini" type="danger" @click="handleDelete">删除</el-button>'
    }))
})
