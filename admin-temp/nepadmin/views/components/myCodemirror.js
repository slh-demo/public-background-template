layui.extend({
    codemirror: 'lay/extends/codemirror',
}).define(['codemirror'],function(exports) {
    var admin = layui.admin
    var codemirror = layui.codemirror
    exports('myCodemirror', Vue.component("myCodemirror",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            },
            content:{
                type: String,
                default:"",
            },

        },
        data:function(){
            return {
                editorId:'codemirror-'+(new Date()).getTime()+'-'+Math.ceil(Math.random()*10),
                codemirror:{},
            }
        },
        mounted:function(){
            this.initCodemirror()
        },
        computed : {

        },
        methods: {


            initCodemirror:function(){
                console.log('--initEditor--',this.editorId)
                var myTextarea = document.getElementById(this.editorId);
                var codemirror = CodeMirror.fromTextArea(myTextarea, {
                    lineNumbers: true,
                    matchBrackets : true,
                    styleActiveLine : true,
                });
                codemirror.setSize('auto','500px');
                // console.log('++++++++',codemirror,this.codemirror)
                this.codemirror = codemirror
            },
            setContent:function(content){
                // console.log('++++++++',content)
                this.codemirror.setValue(content)
            },
            getContent:function(){
                return this.codemirror.getValue()
            }
        },
        template:  '<input :id="editorId" />\n'
    }))
})
