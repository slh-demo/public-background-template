layui.extend({
    //城市
    citypicker: 'lay/extends/city-picker/city-picker',
}).define(['citypicker'],function(exports) {
    var admin = layui.admin
    var cityPicker = layui.citypicker
    exports('cityPickerChoose', Vue.component("cityPickerChoose",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            },

        },
        data:function(){
            return {
                cityPickerId: 'city-picker-'+(new Date()).getTime(),
                currentPicker: undefined,
            }
        },
        mounted:function(){
            this.initCityPicker()
        },
        computed : {

        },
        methods: {
            setLocation:function(location_addr){
                this.currentPicker.setValue(location_addr||'')
            },
            getLocation:function(){
                return this.currentPicker.getVal()
            },
            initCityPicker:function(){
                var that = this ;
                this.currentPicker = new cityPicker("#"+this.cityPickerId,{
                    provincename:"provinceId",
                    cityname:"cityId",
                    districtname: "districtId",
                    level: 'districtId',// 级别
                })


            },

        },
        template:  '<el-input  :id="cityPickerId"></el-input>'
    }))
})
