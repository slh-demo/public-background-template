layui.extend({
    excel: 'lay/extends/excel',
}).define(['excel'],function(exports) {
    var admin = layui.admin
    var excel = layui.excel

    exports('exportExcel', Vue.component("exportExcel",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            },
            fileName:{
                type: String,
                default:"excel",
            },
            url:{
                type: String,
                default:"string",
            },
            listQuery:{
                type: Object,
                default:function(){return {} },
            },
            generateExcelData:{
                type: Function,
                default:function(res){},
            },
        },
        data:function(){
            return {

            }
        },
        mounted:function(){

        },
        computed : {

        },
        methods: {
            handleExportExcel:function(){
                const loadingInstance = this.$loading({text:'获取下载资源...'})
                var that = this
                admin.post({
                    url:this.url,
                    data: Object.assign(this.listQuery,{limit:50000}),
                    success: function (res) {
                        loadingInstance.text="组织导出信息...."
                        var new_data = that.generateExcelData(res)
                       console.log('++++++++++++',new_data)

                        // 重点！！！如果后端给的数据顺序和映射关系不对，请执行梳理函数后导出
                        var excel_obj = new_data.shift()
                        new_data = excel.filterExportData(new_data, Object.keys(excel_obj));
                        // 重点2！！！一般都需要加一个表头，表头的键名顺序需要与最终导出的数据一致
                        new_data.unshift(excel_obj);
                        excel.exportExcel(new_data, that.fileName+'.xlsx', 'xlsx');
                        loadingInstance.text="导出完成..."
                        setTimeout(function(){ loadingInstance.close() },500)
                    },
                    error:function(res){
                        loadingInstance.text="资源获取异常.."
                        setTimeout(function(){ loadingInstance.close() },500)
                    }
                })
            },
        },
        template:  '<button  class="layui-btn icon-btn nepadmin-mar-l5" @click="handleExportExcel"><slot>导出</slot></button>'
    }))
})
