layui.define(function(exports) {
    var admin = layui.admin

    exports('fileUpload', Vue.component("fileUpload",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            },
            fileType:{
                type: String,
                default:"image",
            },
            showFileList:{
                type: Boolean,
                default: false,
            },
            // fileList:{
            //     type: Array,
            //     default: function(){ return [] },
            // },
            uploadAccept:{
                type: String,
                default:"image/*",
            },
            // uploadInfo:{
            //     type: Object,
            //     default:function(){ return {} },
            // },
            uploadSuccess:{
                type: Function,
                default:function(res){},
            },
            uploadChange:{
                type: Function,
                default:function(res){},
            },
        },
        data:function(){
            return {
                fileList: [],
                upload_info:{},
                uploadLoadingInstance:undefined,
            }
        },
        mounted:function(){
            console.log('----fileupload','mounted')
            this.getUploadInfo()
        },
        computed : {
            get_upload_data:function (){
                const upload_info_data = this.upload_info.data||{};
                return Object.assign(upload_info_data,{type:this.fileType})
            },
        },
        methods: {
            setFileList:function(fileList){
                this.fileList = fileList
            },
            //初始化图片上传
            getUploadInfo: function () {
                const that = this
                admin.uploadInfo({}, function (upload_info) {
                    that.upload_info = upload_info
                })
            },
            onBeforeUploadFile:function(){
                console.log('----------')
                this.uploadLoadingInstance = this.$loading({text:'上传中 ....'})

            },
            onProgress:function(event, file, fileList){
                var percent = event.percent.toFixed(2)
                this.uploadLoadingInstance.text="上传进度（"+percent+"%）"
            },
            handleSuccessFile:function(res){
                var that = this
                this.uploadLoadingInstance.text="上传完成"
                setTimeout(function(){ that.uploadLoadingInstance.close() },500)
                console.log('handleSuccessFile',res)
                // this.$emit('handleSuccessFile',res)
                this.uploadSuccess(res)
            },
            handleImgChange:function(file,fileList){
                var img = []
                fileList.map(function(item){
                    if(item.key){
                        img.push(item.key)
                    }else{
                        const response = item.response||{}
                        const data = response.data||{}
                        if(data.key) img.push(data.key)
                    }
                })
                this.uploadChange(img)
            },
        },
        template:  ' <el-upload\n' +
            '        ref="el-upload"\n' +
            '        class="avatar-uploader"\n' +
            '        :action="upload_info.url||\'\'"\n' +
            '        :data = "get_upload_data"\n' +

            '        :file-list="fileList"\n' +
            '        :show-file-list="showFileList"\n' +
            '        :accept ="uploadAccept"\n' +
            '        :before-upload="onBeforeUploadFile"\n' +
            '        :on-remove="handleImgChange"\n' +
            '        :on-change="handleImgChange"\n' +
            '        :on-progress="onProgress"\n' +
            '        :on-success="handleSuccessFile"\n' +
            '        list-type="picture-card"\n' +
            '>\n' +

            '    <i v-if="showFileList" class="el-icon-plus avatar-uploader-icon"></i>\n' +
            '   <slot />\n' +
            '</el-upload>'
    }))
})
