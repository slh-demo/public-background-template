layui.extend({

}).define(function(exports) {
    var admin = layui.admin
    exports('mySwitchStatus', Vue.component("mySwitchStatus",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            },
            trueValue:{
                type:String,
                default:"1",
            },
            falseValue:{
                type:String,
                default:"2",
            },
            switchField:{
                type:String,
                default:"status",
            },
            activeText:{
                type:String,
                default:"启用",
            },
            inactiveText:{
                type:String,
                default:"禁用",
            },
            item:{
                type: Object,
                default: function(){  return {} },
            },
            url:{
                type: String,
                default:"",
            },

        },
        data:function(){
            return {
            }
        },
        mounted:function(){

        },
        computed : {
            get_state: function () {
                return this.item[this.switchField] - 0 === 1
            }
        },
        methods: {
            handleSwitchChange:function(bool,field){
                var obj = {id:this.item.id}

                this.item[this.switchField] = bool  ? this.trueValue :  this.falseValue
                obj[this.switchField] = this.item[this.switchField]
                admin.post({
                    url:this.url
                    ,data: obj
                    ,success: function (res) {

                    }
                })
            },

        },
        template:  '<div>' +
            '         <el-switch\n' +
            '                 class="switchStyle"\n' +
            '                 :value="get_state"\n' +
            '                 active-color="#13ce66"\n' +
            '                 :active-text="activeText"\n' +
            '                 :inactive-text="inactiveText"\n' +
            '                 @change="handleSwitchChange($event)"\n' +
            '         >\n' +
            '         </el-switch>\n' +
            '     </div>'
    }))
})
