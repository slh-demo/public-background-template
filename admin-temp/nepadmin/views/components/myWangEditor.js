layui.extend({
    wangEditor: 'lay/extends/wangEditor',
}).define(['wangEditor'],function(exports) {
    var admin = layui.admin
    var wangEditor = layui.wangEditor
    exports('myWangEditor', Vue.component("myWangEditor",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            },
            fileType:{
                type: String,
                default:"image",
            },
            content:{
                type: String,
                default:"",
            },

        },
        data:function(){
            return {
                editorId:'editor-'+(new Date()).getTime()+'-'+Math.ceil(Math.random()*10),
                editor:{},
                upload_info:{},
                uploadLoadingInstance:undefined,
            }
        },
        mounted:function(){

            this.getUploadInfo()
        },
        computed : {
            get_upload_data:function (){
                const upload_info_data = this.upload_info.data||{};
                return Object.assign(upload_info_data,{type:this.fileType})
            },
        },
        methods: {

            //初始化图片上传
            getUploadInfo: function () {
                var that = this
                admin.uploadInfo({}, function (upload_info) {
                    console.log('-----------',upload_info,that.upload_info)

                    that.upload_info = upload_info
                    //初始化編輯器
                    that.initEditor()

                })
            },
            initEditor:function(){
                console.log('--initEditor--')
                this.editor = admin.wangEditor(wangEditor, '.'+this.editorId )


            },
            setContent:function(content){
                var that = this
                setTimeout(function(){
                    that.editor.txt.html(content) // 重新设置编辑器内容
                },500)
            },
            getContent:function(){
               return this.editor.txt.html()
            }
        },
        template:  '<div :class="editorId">\n' +
            '</div>'
    }))
})
