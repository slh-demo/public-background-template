layui.define(function(exports) {
    var admin = layui.admin

    exports('mapGd', Vue.component("mapGd",{
        name:'mapGd',
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            },
            addr:{
                type: String,
                default:"string",
            },
            showSearch:{
                type: Boolean,
                default: true,
            },

            mapLngLat:{
                type: Function,
                default: function(lng,lat){}
            }
        },
        data:function(){
            return {
                mapId : 'map-'+(new Date()).getTime(),
                lng:'',
                lat:'',
                locationAddr:'',
                marker_search_arr: [],
                marker: [],
                map:undefined,
                placeSearch:undefined,
            }
        },
        mounted:function(){
            this.initMap()
        },
        computed : {
            get_lng_lat:function(){
                return this.lng+','+this.lat
            }
        },
        methods: {
            setLocation:function(lng,lat){
                this.lng = lng
                this.lat = lat

                if( lng && lat && lng-0 !== 0 && lat -0 !== 0 ){
                    var position = new AMap.LngLat(lng, lat);//标准写法
                    var map = new AMap.Map(this.mapId,{
                        zoom: 15, //初始地图级别
                        center: position, //初始地图中心点
                    });
                    this.bindSearch(map)

                    var marker = new AMap.Marker({
                        position: position,   // 经纬度对象，也可以是经纬度构成的一维数组[116.39, 39.9]
                        title: this.addr||''
                    });
                    // 将创建的点标记添加到已有的地图实例：
                    map.add(marker);
                    this.marker.push(marker)
                }
            },
            initMap() {
                var that = this
                var marker;
                var lng = "114.077347";
                var lat ="22.549401"
                if(this.lng && this.lat){
                    lng = this.lng;
                    lat =this.lat
                }
                var position = new AMap.LngLat(lng, lat);//标准写法
                var map = new AMap.Map(this.mapId,{
                    zoom: 15, //初始地图级别
                    center: position, //初始地图中心点
                });
                this.bindSearch(map)
            },

            bindSearch:function(map){
                var that = this
                this.placeSearch = new AMap.PlaceSearch()




                map.on('click', function(e){
                    map.remove(that.marker_search_arr)

                    that.marker && map.remove(that.marker)
                    that.marker = new AMap.Marker({
                        position: e.lnglat,   // 经纬度对象，也可以是经纬度构成的一维数组[116.39, 39.9]
                        title: ""
                    });
                    // 将创建的点标记添加到已有的地图实例：
                    map.add(that.marker);
                    var text = '您在 [ '+e.lnglat.getLng()+','+e.lnglat.getLat()+' ] 的位置单击了地图！'
                    console.log(text)

                    // $("input[name='lng_lat']").val(e.lnglat.getLng() + "," +e.lnglat.getLat())
                    that.lng = e.lnglat.getLng()
                    that.lat = e.lnglat.getLat()

                    that.mapLngLat(that.lng, that.lat)
                })
                this.map = map
            },

            locationSearch:function(location){
                var searchLocation = location
                if(this.showSearch){
                    searchLocation = this.locationAddr
                }
                let that = this
                this.marker && this.map.remove(this.marker)
                this.map.remove(this.marker_search_arr);
                if(searchLocation.length>0){
                    this.placeSearch.search(searchLocation, function (status, result) {
                        // 查询成功时，result即对应匹配的POI信息
                        console.log('+++++',status,result,that.marker_search_arr)
                        var pois = result.poiList.pois;
                        for(var i = 0; i < 3; i++){
                            var poi = pois[i];
                            if(poi && poi.hasOwnProperty('location')){
                                that.marker_search_arr[i]= new AMap.Marker({
                                    position: poi.location,   // 经纬度对象，也可以是经纬度构成的一维数组[116.39, 39.9]
                                    title: poi.name
                                });
                                // 将创建的点标记添加到已有的地图实例：
                                that.map.add(that.marker_search_arr[i]);
                            }

                        }
                        // 将创建的点标记添加到已有的地图实例：
                        that.map.setFitView();

                    })
                }else{
                    alert("请输入地址")
                }
            },
        },
        template:  '<div>' +
            '<el-form-item label-width="0px">' +
            '   <el-input placeholder="坐标" disabled :value="get_lng_lat"> </el-input>\n' +
            '</el-form-item>' +
            '<el-form-item label-width="0px">' +
            '<el-input placeholder="位置搜索" v-model="locationAddr" v-if="showSearch">\n' +
            '   <el-button size="small" type="primary"  slot="append"   @click="locationSearch">搜索</el-button>\n' +
            '</el-input>' +
            '   <div  :id="mapId" style="width:750px; height:300px"></div>' +
            '</el-form-item>' +
            '</div>'
    }))
})
