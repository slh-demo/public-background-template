layui.extend({
    ueditor: 'lay/extends/ueditor/ueditor',
}).define(['ueditor'],function(exports) {
    var admin = layui.admin
    var ueditor = layui.ueditor
    exports('myUeEditor', Vue.component("myUeEditor",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            },
            fileType:{
                type: String,
                default:"image",
            },
            content:{
                type: String,
                default:"",
            },

        },
        data:function(){
            return {
                editorId:'editor-'+(new Date()).getTime()+'-'+Math.ceil(Math.random()*10),
                editor:{},
                upload_info:{},
                uploadLoadingInstance:undefined,
            }
        },
        mounted:function(){

            this.getUploadInfo()
        },
        computed : {
            get_upload_data:function (){
                const upload_info_data = this.upload_info.data||{};
                return Object.assign(upload_info_data,{type:this.fileType})
            },
        },
        methods: {

            //初始化图片上传
            getUploadInfo: function () {
                var that = this
                admin.uploadInfo({}, function (upload_info) {
                    console.log('-----------',upload_info,that.upload_info)

                    that.upload_info = upload_info
                    //初始化編輯器
                    that.initEditor()

                })
            },
            initEditor:function(){
                var that = this
                console.log('--initEditor--')
                this.editor = ueditor.getEditor(this.editorId,{
                    serverUrl: "/admin/upload/editorConfig"
                    ,initialFrameHeight:500  //初始化编辑器高度,默认320
                });
                // this.editor = admin.ueditor(wangEditor, '.'+this.editorId )


            },
            setContent:function(content){
                var that = this
                setTimeout(function(){
                    that.editor.setContent(content) // 重新设置编辑器内容
                },500)
            },
            getContent:function(){
               return this.editor.getContent()
            }
        },
        template:  '<div :id="editorId">\n' +
            '</div>'
    }))
})
