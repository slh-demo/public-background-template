layui.extend({

}).define(function(exports) {
    var admin = layui.admin
    exports('myBtnSubmit', Vue.component("myBtnSubmit",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            },
            btnName:{
                type: String,
                default: "提交",
            },
            msgState:{
                type:Number,
                default: 1
            },
            asyncTime:{
                type:Number,
                default: 500
            },
            complete:{
                type: Function,
                default: function(event,res){  return {} },
            },

            data:{
                type: Object,
                default:{},
            },
            url:{
                type: String,
                default:"",
            },

        },
        data:function(){
            return {
                loading:false,
            }
        },
        mounted:function(){

        },
        computed : {

        },
        methods: {
            handleSubmit:function(e){
                var that = this;
                that.loading = true;
                admin.post({
                    url:this.url
                    ,data:this.data
                    ,success: function (res) {
                        const data = res.data||{}
                        if(that.msgState===1){
                            that.$message({ message: res.msg, type: 'success' });
                        }
                        setTimeout(function(){
                            that.complete(e,res)

                        },that.asyncTime)
                    }
                    ,complete:function(res){
                        that.loading = false
                    }
                })

            },

        },
        template:  ' <el-button  :loading="loading" type="primary" @click="handleSubmit">{{btnName}}</el-button>'
    }))
})
