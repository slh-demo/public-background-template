layui.extend({

}).define(function(exports) {
    var admin = layui.admin
    exports('myElTableColumn', Vue.component("myElTableColumn",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            },
            prop:{
                type: String,
                default:"string",
            },
            editorMode:{
                type: String,
                default:"input",
            },
            inputType:{
                type:String,
                default:'text'
            },
            selectList:{
                type: Array,
                default: function(){return []},
            },
            selectKeyField:{
                type:String,
                default:'id'
            },
            selectNameField:{
                type:String,
                default:'name'
            },
            row:{
                type: Object,
                default: function(){return {}},
            },
            url:{
                type: String,
                default:""
            },
            //构造数据
            generateReqObj:{
                type: Function,
                default: function(e){return {}},
            },
        },
        data:function(){
            return {
                editor: false,
                valueChanged:false,
                oldValue: "",
            }
        },
        mounted:function(){

        },
        computed : {
            get_show_name:function(){
                var name = this.row[this.prop];
                // console.log('-------',this.editorMode,this.selectNameField,this.selectKeyField,this.prop)
                if(this.editorMode==='select'){
                    name = '--'
                    for(var i=0; i <this.selectList.length; i++){
                        var info = this.selectList[i]
                        if(this.row[this.prop]===info[this.selectKeyField]){
                            name = info[this.selectNameField]
                            break;
                        }
                    }
                }
                return name
            }
        },
        methods: {
            columnDbClick:function(){
                console.log('--columnDbClick')
                this.editor = true
                this.oldValue = this.row[this.prop]
                this.$nextTick(function(){
                    if(this.editorMode==='input'){
                        this.$refs.input.focus()
                    }
                })
            },
            changeData:function(){
                var changeValue =  this.row[this.prop];
                if(this.oldValue === changeValue){ // 未发生变化的数据 无法提交
                    return
                }
                const that = this
                var obj = {id:this.row.id}
                var generateObj = this.generateReqObj(changeValue)
                //主动构造数据
                if(Object.keys(generateObj).length>0){
                    obj = Object.assign(obj,generateObj)
                }else{
                    obj[this.prop] = changeValue
                }
                admin.post({
                    url:this.url
                    ,data:obj
                    ,success: function (res) { }
                    ,complete:function(){ console.log('complete') }
                })
            },
            onBlur:function(){
                this.editor = false
                this.changeData()
            }
        },
        template:
            ' <div @dblclick="columnDbClick">      ' +
            '   <div class="cell el-tooltip " v-show="editor && editorMode==\'input\'" ><el-input :type="inputType" ref="input" v-model="row[prop]"   @blur="onBlur"/></div>' +
            '   <div class="cell el-tooltip " v-show="editor && editorMode==\'select\'" >' +
            '       <el-select :type="inputType"  v-model="row[prop]"   @change="onBlur">' +
            '           <template  v-for="(item,index) in selectList">\n' +
            '               <el-option :label="item.name"  :value="item.id"></el-option>\n' +
            '               <el-option :label="child.name"  :value="item.id+\',\'+child.id" v-for="(child,child_index) in item.child_list" :key="child_index" style="margin-left: 20px"></el-option>\n' +
            '           </template>\n' +
            '        </el-select>\n' +
            '   </div>' +
            '   <div class="cell el-tooltip" style="width: 100%;" v-show="!editor">&nbsp;{{get_show_name}}</div>' +
            ' </div>'

    }))
})
