layui.extend({
}).define(function(exports) {
    var admin = layui.admin
    exports('indexShowData', Vue.component("indexShowData",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            },

        },
        data:function(){
            return {
                data:[
                    {name:'访问用户',colorClass:'bg-gradual-red',num:0,field:'user_num'},
                    {name:'待发货订单',colorClass:'bg-gradual-green',num:0,field:'order_send_num',url:'/order/index'},
                    {name:'未支付订单',colorClass:'bg-gradual-purple',num:0,field:'order_wait_pay_num',url:'/order/index'},
                    {name:'成交总额度',colorClass:'bg-gradual-pink',num:0,field:'order_money',url:'/order/index'},
                    {name:'注册用户',colorClass:'bg-gradual-blue',num:0, field:'user_num',url:'/user/index'},
                ],
            }
        },
        mounted:function(){
            this.getData()
        },
        computed : {

        },
        methods: {
            getData:function(){
                const that = this
                admin.post({
                    url:'/admin/index/defInfo'
                    ,data:{type:'show_total_data' }
                    ,success: function (res) {
                        var data = res.data||{}
                        that.data.map(function(item,index){
                            var field = item.field
                            item.num  = data[field]||0
                        })
                    }
                })

            },
            handleItemClick:function(item){
                if(item.url && item.url.length>0){
                    location.href = item.url
                }
            },

        },
        template:  '<div class="layui-row layui-col-space15">\n' +
            '        <div class="layui-col-lg12">\n' +
            '            <div class="layui-row layui-col-space15">\n' +

            '                <a class="layui-col-sm2" v-for="(item,index) in data" :key="index"  :lay-href="item.url">\n' +
            '                    <div class="config-item " :class="[item.colorClass]">\n' +
            '                        <div class="tip-container">\n' +
            '                            <div class="config-item-icon-container"\n' +
            '                                style="margin-top: 30px; position: relative; display: flex;">\n' +
            '                                <div class="item-icon-1"><img src="/admin-temp/nepadmin/images/shopro-icon.png"></div>\n' +
            '                                <div class="item-icon-2"></div>\n' +
            '                            </div>\n' +
            '                            <div class="config-title">\n' +
            '                                <span class="nepadmin-linecard-text">{{item.num}}</span>\n' +
            '                                 <span class="nepadmin-ignore"></span>\n' +
            '                            </div>\n' +
            '                            <div class="config-tip ellipsis-item">{{item.name}}</div>\n' +
            '                            <div class="config-message ellipsis-item"></div>\n' +
            '                        </div>\n' +
            '                        <div class="set-container">\n' +
            '                            <div class="config-item-leaf-container">\n' +
            '                                <div class="item-leaf-1">\n' +
            '                                    <div class="leaf leaf-11"></div>\n' +
            '                                    <div class="leaf leaf-12"></div>\n' +
            '                                    <div class="leaf leaf-13"></div>\n' +
            '                                </div>\n' +
            '                                <div class="item-leaf-2">                                    \n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </a>\n' +

            '            </div>\n' +
            '        </div>\n' +
            '\n' +
            '    </div>'
    }))
})
