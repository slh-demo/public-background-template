layui.define(function(exports) {
    var admin = layui.admin

    exports('createLocationWord', Vue.component("createLocationWord",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            }
        },
        components:{

        },
        data:function() {
            return {
                submitLoading:false,
                dialogVisible:false,
                title: '新增',
                pageObj:undefined,
                type_list:[],
                currentPicker:undefined,
                temp:{
                    id:'',
                    name:'',
                    word:'',
                    content:'',
                    sort:'',
                    status: '1',
                },
                rules: {
                    name: [ { required: true, message: '请输入名字', trigger: 'blur' }],
                    word: [ { required: true, message: '请输入首字母', trigger: 'blur' }],
                },
            }
        },


        created:function(){

        },
        mounted:function(){
        },
        methods: {

            showDialog: function (item,pageObj) {
                console.log('---',item)
                this.pageObj = pageObj
                var that = this;
                if (item && item.id) {
                    this.title = '编辑'
                } else {
                    this.title = '新增'
                }
                for (var key in this.temp) {
                    if (key === 'type') {
                        this.temp[key] = (item[key] || '0') + ''
                    } else if (key === 'status') {
                        this.temp[key] = (item[key] || '1') + ''
                    }else if(key==='sort'){
                        this.temp[key] = (item[key]||100)+''
                    } else {
                        this.temp[key] = item[key] || ''
                    }
                }
                this.dialogVisible = true


            },
            handleSubmit: function () {
                this.submitLoading = true
                const that = this
                admin.post({
                    url: '/admin/platform/platformLocationWordModel_add',
                    data: this.temp,
                    success: function (res) {
                        const data = res.data || {}
                        that.$message({message: res.msg, type: 'success'});
                        setTimeout(function () {
                            that.dialogVisible = false
                            that.pageObj._initData()
                        }, 500)
                    },
                    complete:function(){
                        that.submitLoading = false
                    }
                })
            },

            handleClearLocation: function () {
                console.log('123')
                this.currentPicker.setValue('')
            },
        },
        template: '<el-dialog\n' +
            '    id="components-app"\n' +
            '    :title="title"\n' +
            '    top="1vh"\n' +
            '    :append-to-body="true"\n' +
            '    :close-on-click-modal="false"\n' +
            '    :visible.sync="dialogVisible"\n' +
            '    width="60%"\n' +
            '>\n' +
            '    <el-form ref="form" :model="temp" label-width="120px">\n' +

            '        <el-form-item label="名字" prop="name">\n' +
            '            <el-input v-model="temp.name"></el-input>\n' +
            '        </el-form-item>\n' +
            '        <el-form-item label="首字母" prop="word">\n' +
            '            <el-input v-model="temp.word"></el-input>\n' +
            '        </el-form-item>\n' +

            '\n' +
            '        <el-form-item label="状态" prop="name">\n' +
            '            <el-radio  v-model="temp.status" label="1" >正常</el-radio>\n' +
            '            <el-radio  v-model="temp.status" label="2" >关闭</el-radio>\n' +
            '        </el-form-item>\n' +
            '\n' +
            '\n' +
            '        <el-form-item label="排序" prop="sort">\n' +
            '            <el-input type="number" v-model="temp.sort"></el-input>\n' +
            '        </el-form-item>\n' +
            '\n' +
            '\n' +
            '\n' +
            '    </el-form>\n' +
            '    <span slot="footer" class="dialog-footer">\n' +
            '        <el-button @click="dialogVisible = false">取 消</el-button>\n' +
            '        <el-button :loading="submitLoading" type="primary" @click="handleSubmit">确 定</el-button>\n' +
            '    </span>\n' +
            '</el-dialog>'
    }))
})
