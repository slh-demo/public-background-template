layui.extend({
}).define(["fileUpload","myBtnSubmit"],function(exports) {
    var admin = layui.admin

    // console.log("fileUpload",layui.fileUpload)
    var vue_temp_data = {
        id:'',
        pid: 0,
        name:'',
        url:'',
        sort:'100',
        meta_kw:'',
        meta_des:'',
        node:[],
        img:'',
        status: '1',
    };
    exports('createMenu', Vue.component("createMenu",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            }
        },
        components:{
            'file-upload': layui.fileUpload,
            'my-btn-submit': layui.myBtnSubmit,
        },
        data:function() {
            return {
                dialogVisible:false,
                pageObj:undefined,
                select_list:[],
                temp : Object.assign({},vue_temp_data),
            }

        },
        computed : {

            get_title:function(){
                var title = "新增"
                if(this.temp.id){
                    title = '编辑'
                }
                return title
            },
        },
        created:function(){
            this.getBaseInfo()

        },
        mounted:function(){

        },
        methods: {
            getBaseInfo:function(){
                const that = this
                admin.post({
                    url:'/admin/index/defInfo',
                    data:{type:'web_menu'},
                    success:function(res){
                        const data = res.data||{}
                        that.select_list = data.select_list||[]
                        that.node_list = data.node_list||[]
                    }
                })
            },
            showDialog:function(item,pageObj){
                this.pageObj = pageObj
                this.$tempDataInit(vue_temp_data, this.temp, item)
                this.dialogVisible = true
                this.$nextTick(function(){

                })
            },
            closeDialog:function(e,res){
                this.dialogVisible = false;
                if(res){
                    this.pageObj._initData()
                    // layui.view.tab.refresh()
                }
            },
            onHandleSuccessFile:function(res,payload){
                console.log('onHandleSuccessFile',res,payload)
                var data = res.data||{}
                this.temp.img = data.key||''
            },

        },
        template: '<el-dialog\n' +
            '    id="components-app"\n' +
            '    :title="get_title"\n' +
            '    top="1vh"\n' +
            '    :append-to-body="true"\n' +
            '    :close-on-click-modal="false"\n' +
            '    :visible.sync="dialogVisible"\n' +
            '    width="80%"\n' +
            '>\n' +
            '    <el-form ref="form" :model="temp" label-width="120px">\n' +
            '        <el-row>\n' +
            '            <el-col :span="10">\n' +
            '                <el-form-item label="分类" prop="pid">\n' +
            '                    <el-select v-model="temp.pid" placeholder="请选择顶级分类">\n' +
            '                        <el-option label="顶级分类" :value="0" ></el-option>\n' +
            '                        <template  v-for="(item,index) in select_list">\n' +
            '                            <el-option :label="item.name" :value="item.id" ></el-option>\n' +
            '                            <el-option :label="child.name"  :value="child.id" v-for="(child,child_index) in item.list" :key="child.id" style="margin-left: 20px"></el-option>\n' +
            '                        </template>\n' +
            '                    </el-select>\n' +
            '                </el-form-item>\n' +
            '                <el-form-item label="名称" prop="name">\n' +
            '                    <el-input v-model="temp.name"></el-input>\n' +
            '                </el-form-item>\n' +
            '                <el-form-item label="路径" prop="url">\n' +
            '                    <el-input v-model="temp.url"></el-input>\n' +
            '                </el-form-item>\n' +
            '\n' +
            '                <el-form-item label="排序" prop="sort">\n' +
            '                    <el-input type="number" v-model="temp.sort"></el-input>\n' +
            '                </el-form-item>\n' +
            '\n' +
            '                <el-form-item label="状态" prop="name">\n' +
            '                    <el-radio  v-model="temp.status" label="1" >正常</el-radio>\n' +
            '                    <el-radio  v-model="temp.status" label="2" >关闭</el-radio>\n' +
            '                </el-form-item>\n' +
            '            </el-col>\n' +
            '            <el-col :span="14">\n' +

            '                <el-form-item label="图片(90*90)" prop="intro">\n' +
            '                    <file-upload  :upload-success="onHandleSuccessFile">\n' +
            '                        <img v-if="temp.img" :src="temp.img" fit="fit" class="avatar" >\n' +
            '                        <i v-else class="el-icon-plus avatar-uploader-icon"></i>\n' +
            '                    </file-upload>\n' +

            '                   <div class="layui-text helper-text pointer" @click="temp.img=\'\'">清空上传</div>' +
            '                </el-form-item>\n' +
            '            </el-col>\n' +
            '        </el-row>\n' +
            '\n' +
            '\n' +
            '\n' +
            '    </el-form>\n' +
            '    <span slot="footer" class="dialog-footer">\n' +
            '    <el-button @click="closeDialog">取 消</el-button>\n' +
            '    <my-btn-submit :data="temp"  :complete="closeDialog" url="/admin/index/webMenuModel_add"/>\n' +
            '  </span>\n' +
            '</el-dialog>'
    }))
})
