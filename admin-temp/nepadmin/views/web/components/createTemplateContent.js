layui.extend({
    'myCodemirror':"/views/components/myCodemirror"
}).define(['myCodemirror'], function(exports) {
    var admin = layui.admin
    var myCodemirror = myCodemirror
    // console.log("fileUpload",layui.fileUpload)

    exports('createTemplateContent', Vue.component("createTemplateContent",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            }
        },
        components:{
            'my-codemirror': layui.myCodemirror
        },
        data:function() {
            return {
                dialogVisible:false,
                pageObj:'',
                upload_info:{},
                select_list:[],
                temp:{
                    path:'',
                    content: '',
                }

            }

        },
        computed : {

        },
        created:function(){


        },
        mounted:function(){

        },
        methods: {
            showDialog:function(item,mode,pageObj){
                this.pageObj = pageObj
                var that = this
                console.log(item)
                if(mode==='temp'){
                    this.temp.path = item.tempPath

                }else{
                    this.temp.path = item.path
                }
                this.dialogVisible = true
                this.$nextTick(function(){

                    that.getFileContent(this.temp.path)
                })
            },

            getFileContent:function(path){
                var that = this
                admin.post({
                    url:'/admin/web/templateFileContent'
                    ,data: {path:path}
                    ,success: function (res) {
                        const data = res.data||{}
                        that.temp.content = data.content
                        that.$refs['myCodemirror'].setContent(data.content)
                    }
                })
            },
            handleSubmit:function(){
                const that = this
                this.$confirm("保存将覆盖原来的信息,", '提示', { type: 'error' }).then(function(v){
                    admin.post({
                        url:'/admin/web/templateFileContentSave'
                        ,data: Object.assign(that.temp,{content:that.$refs['myCodemirror'].getContent()})
                        ,success: function (res) {
                            const data = res.data||{}
                            that.$message({ message: res.msg, type: 'success' });
                        }
                    })
                }).catch(function(v) {
                    console.log('cancel',v)
                })
            }
        },
        template: '<el-dialog\n' +
            '    id="components-app-content"\n' +
            '    title="文件处理"\n' +
            '    top="1vh"\n' +
            '    :append-to-body="true"\n' +
            '    :close-on-click-modal="false"\n' +
            '    :visible.sync="dialogVisible"\n' +
            '    width="95%"\n' +
            '>\n' +
            '    <el-form ref="form" :model="temp" label-width="120px">\n' +
            '\n' +
            '        <el-form-item label="文件路径" prop="path">\n' +
            '            <el-input v-model="temp.path" readonly></el-input>\n' +
            '        </el-form-item>\n' +
            '        <el-form-item label="文件内容" prop="content">\n' +
            '            <my-codemirror ref="myCodemirror" /> \n' +
            '        </el-form-item>\n' +
            '\n' +
            '\n' +
            '\n' +
            '\n' +
            '    </el-form>\n' +
            '    <span slot="footer" class="dialog-footer">\n' +
            '    <el-button @click="dialogVisible = false">取 消</el-button>\n' +
            '    <el-button type="primary" @click="handleSubmit">保 存</el-button>\n' +
            '  </span>\n' +
            '</el-dialog>'
    }))
})
