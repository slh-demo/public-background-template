layui.extend({
}).define(function(exports) {
    var admin = layui.admin

    exports('createTemplate', Vue.component("createTemplate",{
        // 在 JavaScript 中是 camelCase 的
        props: {
            name:{
                type: String,
                default:"string",
            }
        },
        components:{

        },
        data:function() {
            return {
                dialogVisible:false,
                editor:'',
                pageObj:'',
                select_list:[],
                temp:{
                    path:'',
                    content: '',
                }

            }

        },
        computed : {

        },
        created:function(){


        },
        mounted:function(){

        },
        methods: {
            showDialog:function(item,pageObj){
                this.pageObj = pageObj
                this.dialogVisible = true
            },
            handleDel:function(){
                this.handleRequest('del',{tip_title:'确定删除【'+this.temp.path+'】模版文件?'})
            },
            handleSubmit:function(){
                this.handleRequest('create',{tip_title:'确定创建【'+this.temp.path+'】模版文件?'})
            },
            handleRequest:function(mode, options){
                var tip_title = options.tip_title||'tip_title'
                const that = this
                this.$confirm(tip_title, '提示', { type: 'error' }).then(function(v){
                    admin.post({
                        url:'/admin/web/templateFile'
                        ,data: Object.assign(that.temp, {mode:mode})
                        ,success: function (res) {
                            const data = res.data||{}
                            that.$message({ message: res.msg, type: 'success' });
                            setTimeout(function(){
                                that.dialogVisible = false
                                layui.view.tab.refresh()
                            },500)
                        }
                    })
                }).catch(function(v) {
                    console.log('cancel',v)
                })
            }
        },
        template: '<el-dialog\n' +
            '    id="components-app"\n' +
            '    title="文件处理"\n' +
            '    top="1vh"\n' +
            '    :append-to-body="true"\n' +
            '    :close-on-click-modal="false"\n' +
            '    :visible.sync="dialogVisible"\n' +
            '    width="60%"\n' +
            '>\n' +
            '    <el-form ref="form" :model="temp" label-width="120px">\n' +
            '\n' +
            '        <el-form-item label="文件路径" prop="path">\n' +
            '            <el-input v-model="temp.path"></el-input>\n' +
            '            <div class="layui-text helper-text"> 路径只能为【英文字母、数字】组成</div>\n' +
            '            <div class="layui-text helper-text"> 示例 分类模板：/index </div>\n' +
            '        </el-form-item>\n' +
            '\n' +
            '\n' +
            '\n' +
            '\n' +
            '    </el-form>\n' +
            '    <span slot="footer" class="dialog-footer">\n' +
            '    <el-button @click="dialogVisible = false">取 消</el-button>\n' +
            '    <el-button type="danger" @click="handleDel">删 除</el-button>\n' +
            '    <el-button type="primary" @click="handleSubmit">新 增</el-button>\n' +
            '  </span>\n' +
            '</el-dialog>'
    }))
})
