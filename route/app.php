<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

Route::rule('/products', 'index/products');
Route::rule('/productscustom', 'index/productscustom');
//Route::rule('/index', 'index')->name('index')->ext('html');
////用户登录
//Route::rule('/index/login', 'index/login');
////注册
//Route::rule('/index/reg', 'index/reg');
////忘记密码
//Route::rule('/index/forget', 'index/forget');
////忘记密码
//Route::rule('/index/forgetVerify', 'index/forgetVerify');
////忘记密码
//Route::rule('/index/forgetMod', 'index/forgetMod');
////发送验证码
//Route::rule('/index/sendSms', 'index/sendSms');
////凭证刷新
//Route::rule('contactSubmit', 'index/contactSubmit');
//Route::rule('submitEmail', 'index/submitEmail');
////
//Route::miss('index/miss')->ext('html');

