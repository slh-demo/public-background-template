
function initJq(){
    $(".gridSwatches li").click(function(){
        var img = $(this).data('img')
        if(img && img.length>0){
            $(this).addClass('active').siblings().removeClass('active')
            $(this).parent().parent().find('.grid-view-item__image').attr('src',img)
        }

    })
}


//请求路由路径
var api_route = "/";

window.initVue = function (page_mixin) {
    //增加网络插件

    Vue.use({
        install:function(){
            //全局属性
            Vue.prototype.globalData = {
                upload_info:undefined,
            }
            //添加网络请求插件
            Vue.prototype.$network = function (url, params,conf) {
                conf = Object.assign({
                    showErrMsg : true,
                },conf||{})
                var that = this
                return new Promise(function (resolve, reject) {
                    $.ajax(api_route + url, {
                        type: "POST",
                        data: params,
                        success: function (res) {
                            var code = res.code
                            var msg = res.msg
                            if (code === 1) {
                                resolve(res)
                            } else {
                                // alert(msg)
                                if(conf.showErrMsg){
                                    that.$message.error(msg)
                                }
                                reject(res)
                            }
                        },
                        error: function (err) {
                            console.log('network-err',err)
                            reject(err)
                        }
                    })
                })
            }
        }
    })

    window.app = new Vue({
        el: '#app-vue',

        mixins: [page_mixin],
        data: {
            cart_num: 0,
            coupon_tip: false,
            account: "",
            password: "",
            email_subscribe: "",

            searchShow: false,
            history_keyword:[],
            search_goods_keyword: "",

            su_email: "",
            su_password: "",
            smart_goods:{},

            MenuShow:'',
            MenuShows:'',
            is_show: false,

        },
        created: function () {
        },
        mounted: function () {
            this.getPageBaseInfo()
            setTimeout(initJq,500)
            this.coupon_tip = localStorage.getItem("coupon_tip") - 0 !== 1
            var history_keyword = localStorage.getItem("history_search_goods_keyword")
            this.history_keyword = history_keyword && history_keyword.length>0 ? history_keyword.split(',') : [];
        },
        methods: {
            getPageBaseInfo:function(){
                const that = this
                this.$network("index/initData", {}).then(function (res) {
                    var data = res.data||{};

                    that.cart_num = data.cart_num - 0
                })
            },
            handleLogin: function () {
                console.log('---')
                const that = this
                this.$network("index/login", {
                    email: this.account,
                    password: this.password,
                }).then(function (res) {
                    var code = res.code
                    var msg = res.msg
                    //登录成功，关闭弹窗
                    document.getElementById('close-btn').click();
                    that.$message.success(msg);
                    location.reload();

                }).catch((res) => {
                    console.error(res)

                })
            },
            handleSubscribeEmail: function () {
                this.$network("index/subscribeEmail", {
                    email: this.email_subscribe
                }).then(function (res) {
                    var code = res.code
                    var msg = res.msg
                    alert(msg)
                })
            },
            handleLogout: function () {
                const that = this
                this.$network("index/logout", {}).then(function (res) {
                    var code = res.code
                    var msg = res.msg
                    //退出成功
                    that.$message.success(msg);
                    //返回首页
                    location.href = "/"
                })
            },
            handleGoodsColl:function(item){
                var that = this
                this.$network("goods/coll", {
                    is_coll:item.is_coll ? 0 : 1,
                    goods_info:[
                        {goods_id:item.id}
                    ],
                }).then(function (res) {
                    const data = res.data||{}
                    item.is_coll = data.is_coll
                    console.log(item)
                    //注册成功，关闭弹窗
                    that.$message.success(res.msg);

                })
            },
            handleRegister: function () {

                const that = this
                this.$network("index/register", {
                    email: this.su_email,
                    password: this.su_password,
                }).then(function (res) {
                    var code = res.code
                    var msg = res.msg
                    //注册成功，关闭弹窗
                    document.getElementById('close-btn').click();
                    that.$message.success(msg);

                })
            },

            focusSearch:function(){
                this.searchShow = true
            },
            blurSearch:function(){
                var that = this;
                setTimeout(function(){
                    that.searchShow = false
                },200)
            },
            handleSearchGoods:function(){
                if(this.search_goods_keyword!=''){
                    var exist_index = this.history_keyword.indexOf(this.search_goods_keyword)
                    if(exist_index!==-1){
                        this.history_keyword.splice(exist_index,1)
                    }
                    this.history_keyword.unshift(this.search_goods_keyword)
                    this.history_keyword= this.history_keyword.slice(0,10)
                    localStorage.setItem("history_search_goods_keyword",this.history_keyword.join(','))
                }
                this.searchKeyword(this.search_goods_keyword)

            },

            searchKeyword:function(keyword){
                this.$network('index/statistics',{  type:'search_keyword',cond_value:keyword })
                window.location.href="/collections?keyword="+keyword
            },
            handleCloseCoupon:function(){
                localStorage.setItem('coupon_tip',1)
                this.coupon_tip = false
                // $("#signin-modal").css('opacity',100).show()
                $('#signin-modal').modal('show');
            },
            handleGoodsAddCart:function(goods_id,sku_id){
                const that = this
                this.$network('goods/cartAdd',{
                    goods_info:[{goods_id:goods_id, sku_id:sku_id, num:1}],
                }).then(function(res){
                    var data = res.data || {}
                    that.cart_num = data.num||0
                    that.$message.success(res.msg)
                })
            },
            handleGoodsSkuItem:function(goods,index,skuname){
                goods['sku_group_name'][index] = skuname
                var sku_name =  goods['sku_group_name']
                sku_name.push()
                var sku_name_str = sku_name.join(',')
                var sku_select_obj = {}
                for(var i =0;i<goods.sku_price.length;i++){
                    var obj = goods.sku_price[i]
                    if(obj.name===sku_name_str){
                        sku_select_obj = obj
                        break;
                    }
                }

                this.$set(goods,'sku_select_obj', obj)
                // console.log('=>>>>>>>>',goods['sku_group_name'] )

            },
            handleGoodsShowCart:function(goods,event,unique_id){

                if(unique_id!==undefined){
                    goods = JSON.parse(decodeURIComponent(event.target.dataset.json))
                    this.$set(this.smart_goods,unique_id,goods)
                }
                var sku_group_name = []
                var sku_select_obj = goods.sku_price[0]||{}
                if (  goods.sku_price.length>0) {

                    sku_group_name = sku_select_obj.name.split(',')
                }
                this.$nextTick(function(){
                    this.$set(goods,'sku_group_name',sku_group_name)
                    this.$set(goods,'sku_select_obj',sku_select_obj)
                    this.$set(goods,'active_cart',true)

                })
            },
            handleGoodsHideCart:function(goods){
                this.$set(goods,'active_cart',false)
            },
            handleMenu:function(type){
                console.log('handleMenu=>>>>>>>>',type)
                if(type==this.MenuShow){
                    this.MenuShow = ""
                }else{
                    this.MenuShow = type

                }
            },

            handheldNavbar:function(type){
                this.is_show = type
                console.log('NavbarShow=>>>>>>>>',this.is_show,type)

                // this.NavbarShow == true
                // if (this.is_show === false) {
                //     return '展开'
                // } else if (this.is_show === true) {
                //     return '收起'
                // } else if (this.is_show === '') {
                //     return null
                // }
            },
            handleMenus:function(type){
                console.log('handleMenu=>>>>>>>>',type)
                if(type==this.MenuShows){
                    this.MenuShows = ""
                }else{
                    this.MenuShows = type

                }
            }

        }
    })
}



