function gm_authFailure() {
	$(".map-section").addClass("map-section--load-error"), $(".map-section__container").remove(), $(
		".map-section__link").remove(), $(".map-section__overlay").after('<div class="errors text-center">' + theme
		.strings.authError + "</div>")
}

function productGridView(e) {
	var t = [],
		i = [];
	productGridElements = $("#" + e + " .grid-products .grid__item"), productGridElements.each(function(e) {
		"none" != $(this).css("clear") && 0 != e && (t.push(i), i = []), i.push(this), productGridElements
			.length == e + 1 && t.push(i)
	}), $.each(t, function() {
		var e = 0;
		$.each(this, function() {
			elHeight = parseInt($(this).find(".grid-view_image").css("height")), elHeight > e && (e =
				elHeight)
		}), $.each(this, function() {})
	})
}
var resizeTimer;
window.theme = window.theme || {}, window.theme = window.theme || {}, theme.Sections = function() {
		this.constructors = {}, this.instances = [], $(document).on("shopify:section:load", this._onSectionLoad.bind(
			this)).on("shopify:section:unload", this._onSectionUnload.bind(this)).on("shopify:section:select", this
			._onSelect.bind(this)).on("shopify:section:deselect", this._onDeselect.bind(this)).on(
			"shopify:block:select", this._onBlockSelect.bind(this)).on("shopify:block:deselect", this
			._onBlockDeselect.bind(this))
	}, theme.Sections.prototype = _.assignIn({}, theme.Sections.prototype, {
		_createInstance: function(e, t) {
			var i = $(e),
				a = i.attr("data-section-id"),
				s = i.attr("data-section-type");
			if (t = t || this.constructors[s], !_.isUndefined(t)) {
				var n = _.assignIn(new t(e), {
					id: a,
					type: s,
					container: e
				});
				this.instances.push(n)
			}
		},
		_onSectionLoad: function(e) {
			var t = $("[data-section-id]", e.target)[0];
			t && this._createInstance(t)
		},
		_onSectionUnload: function(e) {
			this.instances = _.filter(this.instances, function(t) {
				var i = t.id === e.detail.sectionId;
				return i && _.isFunction(t.onUnload) && t.onUnload(e), !i
			})
		},
		_onSelect: function(e) {
			var t = _.find(this.instances, function(t) {
				return t.id === e.detail.sectionId
			});
			!_.isUndefined(t) && _.isFunction(t.onSelect) && t.onSelect(e)
		},
		_onDeselect: function(e) {
			var t = _.find(this.instances, function(t) {
				return t.id === e.detail.sectionId
			});
			!_.isUndefined(t) && _.isFunction(t.onDeselect) && t.onDeselect(e)
		},
		_onBlockSelect: function(e) {
			var t = _.find(this.instances, function(t) {
				return t.id === e.detail.sectionId
			});
			!_.isUndefined(t) && _.isFunction(t.onBlockSelect) && t.onBlockSelect(e)
		},
		_onBlockDeselect: function(e) {
			var t = _.find(this.instances, function(t) {
				return t.id === e.detail.sectionId
			});
			!_.isUndefined(t) && _.isFunction(t.onBlockDeselect) && t.onBlockDeselect(e)
		},
		register: function(e, t) {
			this.constructors[e] = t, $("[data-section-type=" + e + "]").each(function(e, i) {
				this._createInstance(i, t)
			}.bind(this))
		}
	}), window.slate = window.slate || {}, slate.rte = {
		wrapTable: function() {
			$(".rte table").wrap('<div class="rte__table-wrapper"></div>')
		},
		iframeReset: function() {
			var e = $('.rte iframe[src*="youtube.com/embed"], .rte iframe[src*="player.vimeo"]'),
				t = e.add(".rte iframe#admin_bar_iframe");
			e.each(function() {
				$(this).wrap('<div class="video-wrapper"></div>')
			}), t.each(function() {
				this.src = this.src
			})
		}
	}, window.slate = window.slate || {}, slate.a11y = {
		pageLinkFocus: function(e) {
			var t = "js-focus-hidden";
			e.first().attr("tabIndex", "-1").focus().addClass(t).one("blur", function() {
				e.first().removeClass(t).removeAttr("tabindex")
			})
		},
		focusHash: function() {
			var e = window.location.hash;
			e && document.getElementById(e.slice(1)) && this.pageLinkFocus($(e))
		},
		bindInPageLinks: function() {
			$("a[href*=#]").on("click", function(e) {
				this.pageLinkFocus($(e.currentTarget.hash))
			}.bind(this))
		},
		trapFocus: function(e) {
			var t = e.namespace ? "focusin." + e.namespace : "focusin";
			e.$elementToFocus || (e.$elementToFocus = e.$container), e.$container.attr("tabindex", "-1"), e
				.$elementToFocus.focus(), $(document).off("focusin"), $(document).on(t, function(t) {
					e.$container[0] === t.target || e.$container.has(t.target).length || e.$container.focus()
				})
		},
		removeTrapFocus: function(e) {
			var t = e.namespace ? "focusin." + e.namespace : "focusin";
			e.$container && e.$container.length && e.$container.removeAttr("tabindex"), $(document).off(t)
		}
	}, theme.Images = function() {
		return {
			preload: function(e, t) {
				"string" == typeof e && (e = [e]);
				for (var i = 0; i < e.length; i++) {
					var a = e[i];
					this.loadImage(this.getSizedImageUrl(a, t))
				}
			},
			loadImage: function(e) {
				(new Image).src = e
			},
			switchImage: function(e, t, i) {
				var a = this.imageSize(t.src),
					s = this.getSizedImageUrl(e.src, a);
				i ? i(s, e, t) : t.src = s
			},
			imageSize: function(e) {
				var t = e.match(
					/.+_((?:pico|icon|thumb|small|compact|medium|large|grande)|\d{1,4}x\d{0,4}|x\d{1,4})[_\.@]/);
				return null !== t ? t[1] : null
			},
			getSizedImageUrl: function(e, t) {
				if (null == t) return e;
				if ("master" === t) return this.removeProtocol(e);
				var i = e.match(/\.(jpg|jpeg|gif|png|bmp|bitmap|tiff|tif)(\?v=\d+)?$/i);
				if (null != i) {
					var a = e.split(i[0]),
						s = i[0];
					return this.removeProtocol(a[0] + "_" + t + s)
				}
				return null
			},
			removeProtocol: function(e) {
				return e.replace(/http(s)?:/, "")
			}
		}
	}(), theme.Currency = function() {
		var e = "${{amount}}";
		return {
			formatMoney: function(t, i) {
				"string" == typeof t && (t = t.replace(".", ""));
				var a = "",
					s = /\{\{\s*(\w+)\s*\}\}/,
					n = i || e;

				function o(e, t, i, a) {
					if (t = t || 2, i = i || ",", a = a || ".", isNaN(e) || null == e) return 0;
					var s = (e = (e / 100).toFixed(t)).split(".");
					return s[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + i) + (s[1] ? a + s[1] : "")
				}
				switch (n.match(s)[1]) {
					case "amount":
						a = o(t, 2);
						break;
					case "amount_no_decimals":
						a = o(t, 0);
						break;
					case "amount_with_comma_separator":
						a = o(t, 2, ".", ",");
						break;
					case "amount_no_decimals_with_comma_separator":
						a = o(t, 0, ".", ",");
						break;
					case "amount_no_decimals_with_space_separator":
						a = o(t, 0, " ")
				}
				return n.replace(s, a)
			}
		}
	}(), slate.Variants = function() {
		function e(e) {
			this.$container = e.$container, this.product = e.product, this.singleOptionSelector = e
				.singleOptionSelector, this.originalSelectorId = e.originalSelectorId, this.enableHistoryState = e
				.enableHistoryState, this.currentVariant = this._getVariantFromOptions(), $(this.singleOptionSelector,
					this.$container).on("change", this._onSelectChange.bind(this))
		}
		return e.prototype = _.assignIn({}, e.prototype, {
			_getCurrentOptions: function() {
				var e = _.map($(this.singleOptionSelector, this.$container), function(e) {
					var t = $(e),
						i = t.attr("type"),
						a = {};
					if ("radio" === i || "checkbox" === i) return !!t[0].checked && (a.value = t
						.val(), a.index = t.data("index"), a);
					a.value = t.val(), a.index = t.data("index");
					var s = a.value.replace(/'/g, "\\'");
					return $("." + a.index).find(".swatchInput[value='" + s + "']").prop("checked",
						!0), $("." + a.index).find(".slVariant").text(a.value), a
				});
				return e = _.compact(e)
			},
			_getVariantFromOptions: function() {
				var e = this._getCurrentOptions(),
					t = this.product.variants;
				return _.find(t, function(t) {
					return e.every(function(e) {
						return _.isEqual(t[e.index], e.value)
					})
				})
			},
			_onSelectChange: function() {
				var e = this._getVariantFromOptions();
				this.$container.trigger({
					type: "variantChange",
					variant: e
				}), e && (this._updateMasterSelect(e), this._updateImages(e), this._updatePrice(e), this
					._updateSKU(e), this.currentVariant = e, this.enableHistoryState && this
					._updateHistoryState(e))
			},
			_updateImages: function(e) {
				var t = e.featured_image || {},
					i = this.currentVariant.featured_image || {};
				e.featured_image && t.src !== i.src && this.$container.trigger({
					type: "variantImageChange",
					variant: e
				})
			},
			_updatePrice: function(e) {
				e.price === this.currentVariant.price && e.compare_at_price === this.currentVariant
					.compare_at_price || this.$container.trigger({
						type: "variantPriceChange",
						variant: e
					})
			},
			_updateSKU: function(e) {
				e.sku !== this.currentVariant.sku && this.$container.trigger({
					type: "variantSKUChange",
					variant: e
				})
			},
			_updateHistoryState: function(e) {
				if (history.replaceState && e) {
					var t = window.location.protocol + "//" + window.location.host + window.location
						.pathname + "?variant=" + e.id;
					window.history.replaceState({
						path: t
					}, "", t)
				}
			},
			_updateMasterSelect: function(e) {
				$(this.originalSelectorId, this.$container).val(e.id)
			}
		}), e
	}(), window.theme = window.theme || {}, theme.Search = function() {
		var e = {
			search: ".search",
			searchSubmit: ".search__submit",
			searchInput: ".search__input",
			siteHeader: "#shopify-section-header .header-wrap",
			siteHeaderSearchToggle: ".site-header__search-toggle",
			searchDrawer: ".search-bar"
		};
		return {
			init: function() {
				if ($(e.siteHeader).length) {
					$(e.searchSubmit).on("click", function(t) {
						var i = $(t.target),
							a = i.parents(e.search).find(e.searchInput);
						0 === a.val().length && (t.preventDefault(), searchFocus(a))
					}), $(e.siteHeaderSearchToggle).on("click", function() {
						$(e.searchDrawer).addClass("active"), $("body").addClass("searchOn")
					}), $("body").click(function(t) {
						var i = $(t.target);
						i.parents().is(e.searchDrawer) || i.is(e.searchDrawer) || i.parents().is(e
							.siteHeaderSearchToggle) || ($(e.searchDrawer).removeClass("active"), $(
							"body").removeClass("searchOn"))
					}), $(".search-bar__close").on("click", function() {
						$(e.searchDrawer).removeClass("active"), $("body").removeClass("searchOn")
					});
					var t = null;
					$('form[action="/search"]').each(function() {
						var e = $(this).find('input[name="q"]');
						e.position().top, e.innerHeight(), $('<ul class="search-results"></ul>').appendTo($(
							this)).hide(), e.attr("autocomplete", "off").bind("keyup change",
					function() {
							var e = $(this).val(),
								i = $(this).closest("form"),
								a = "/search?type=product&q=" + e,
								s = i.find(".search-results");
							e.length > 3 && e != $(this).attr("data-old-term") && ($(this).attr(
								"data-old-term", e), null != t && t.abort(), t = $.getJSON(
								a + "&view=json",
								function(e) {
									s.empty(), 0 == e.results_count ? s.hide() : ($.each(e
										.results,
										function(e, t) {
											var i = $("<a></a>").attr("href", t
											.url);
											i.append(
													'<span class="thumbnail"><img src="' +
													t.thumbnail + '" /></span>'), i
												.append('<span class="title">' + t
													.title + "</span>"), i.append(
													'<span class="price">' + t
													.price + "</span>"), i.wrap(
													"<li></li>"), s.append(i
												.parent())
										}), e.results_count > 10 && s.append(
										'<span class="view-all-products"><a href="' +
										a + '">' + theme.allresult + " (" + e
										.results_count + ")</a></span>"), s.fadeIn(
										200))
								}))
						})
					})
				}
			}
		}
	}(),
	function() {
		var e = $(".return-link");

		function t(e) {
			var t = document.createElement("a");
			return t.ref = e, t.hostname
		}
		document.referrer && e.length && window.history.length && e.one("click", function(e) {
			e.preventDefault();
			var i = t(document.referrer);
			return t(window.location.href) === i && history.back(), !1
		})
	}(), theme.Slideshow = function() {
		this.$slideshow = null;
		var e = {
			wrapper: "slideshow-wrapper",
			slideshow: "slideshow",
			currentSlide: "slick-current",
			video: "slideshow__video",
			videoBackground: "slideshow__video--background",
			closeVideoBtn: "slideshow__video-control--close",
			pauseButton: "slideshow__pause",
			isPaused: "is-paused"
		};

		function t(t) {
			this.$slideshow = $(t), this.$wrapper = this.$slideshow.closest("." + e.wrapper), this.$pause = this
				.$wrapper.find("." + e.pauseButton), this.settings = {
					accessibility: !0,
					arrows: !0,
					dots: !0,
					fade: !0,
					rtl: theme.rtl,
					draggable: !0,
					touchThreshold: 20,
					autoplay: this.$slideshow.data("autoplay"),
					autoplaySpeed: this.$slideshow.data("speed")
				}, this.$slideshow.on("beforeChange", function(t, a, s, n) {
					var o = a.$slider,
						r = o.find("." + e.currentSlide),
						c = o.find('.slideshow__slide[data-slick-index="' + n + '"]');
					if (i(r)) {
						var l = r.find("." + e.video),
							d = l.attr("id");
						theme.SlideshowVideo.pauseVideo(d), l.attr("tabindex", "-1")
					}
					if (i(c)) {
						var h = c.find("." + e.video),
							u = h.attr("id"),
							p = h.hasClass(e.videoBackground);
						p ? theme.SlideshowVideo.playVideo(u) : h.attr("tabindex", "0")
					}
				}.bind(this)), this.$slideshow.on("init", function(t, i) {
					var a = i.$slider,
						s = i.$list,
						n = this.$wrapper,
						o = this.settings.autoplay;
					s.removeAttr("aria-live"), n.on("focusin", function(e) {
						n.has(e.target).length && (s.attr("aria-live", "polite"), o && a.slick(
							"slickPause"))
					}), n.on("focusout", function(t) {
						if (n.has(t.target).length && (s.removeAttr("aria-live"), o)) {
							if ($(t.target).hasClass(e.closeVideoBtn)) return;
							a.slick("slickPlay")
						}
					}), i.$dots && i.$dots.on("keydown", function(e) {
						37 === e.which && a.slick("slickPrev"), 39 === e.which && a.slick("slickNext"),
							37 !== e.which && 39 !== e.which || i.$dots.find(".slick-active button").focus()
					})
				}.bind(this)), this.$slideshow.slick(this.settings), this.$pause.on("click", this.togglePause.bind(
					this))
		}

		function i(t) {
			return t.find("." + e.video).length
		}
		return $(window).on("load delayed-resize", function(e, t) {
			var i = $(window).height();
			$(".slideshow-wrapper .slideshow--full").height(i - 35)
		}), t.prototype.togglePause = function() {
			var t = "#Slideshow-" + this.$pause.data("id");
			this.$pause.hasClass(e.isPaused) ? (this.$pause.removeClass(e.isPaused), $(t).slick("slickPlay")) : (
				this.$pause.addClass(e.isPaused), $(t).slick("slickPause"))
		}, t
	}(),
	function() {
		var e = $("#BlogTagFilter");
		e.length && e.on("change", function() {
			location.href = $(this).val()
		})
	}(), window.theme = theme || {}, theme.customerTemplates = function() {
		function e() {
			$("#RecoverPasswordForm").toggleClass("hide"), $("#CustomerLoginForm").toggleClass("hide")
		}
		return {
			init: function() {
				var t;
				"#recover" === window.location.hash && e(), $("#RecoverPassword").on("click", function(t) {
					t.preventDefault(), e()
				}), $("#HideRecoverPasswordLink").on("click", function(t) {
					t.preventDefault(), e()
				}), $(".reset-password-success").length && $("#ResetSuccess").removeClass("hide"), (t = $(
					"#AddressNewForm")).length && (Shopify && new Shopify.CountryProvinceSelector(
					"AddressCountryNew", "AddressProvinceNew", {
						hideElement: "AddressProvinceContainerNew"
					}), $(".address-country-option").each(function() {
					var e = $(this).data("form-id"),
						t = "AddressCountry_" + e,
						i = "AddressProvince_" + e,
						a = "AddressProvinceContainer_" + e;
					new Shopify.CountryProvinceSelector(t, i, {
						hideElement: a
					})
				}), $(".address-new-toggle").on("click", function() {
					t.toggleClass("hide")
				}), $(".address-edit-toggle").on("click", function() {
					var e = $(this).data("form-id");
					$("#EditAddress_" + e).toggleClass("hide")
				}), $(".address-delete").on("click", function() {
					var e = $(this),
						t = e.data("form-id"),
						i = e.data("confirm-message");
					confirm(i || "Are you sure you wish to delete this address?") && Shopify.postLink(
						"/account/addresses/" + t, {
							parameters: {
								_method: "delete"
							}
						})
				}))
			}
		}
	}(), window.theme = window.theme || {}, theme.navigationmenu = function() {
		var e = "body",
			t = ".js-mobile-nav-toggle",
			i = ".mobile-nav-wrapper",
			a = "#MobileNav .ad",
			s = ".closemobileMenu";
		$("#siteNav .lvl1 > a").each(function() {
			$(this).attr("href") == window.location.pathname && $(this).addClass("active")
		}), $(t).on("click", function() {
			$(i).toggleClass("active"), $(e).toggleClass("menuOn"), $(t).toggleClass(
				"mobile-nav--open mobile-nav--close")
		}), $(s).on("click", function() {
			$(i).toggleClass("active"), $(e).toggleClass("menuOn"), $(t).toggleClass(
				"mobile-nav--open mobile-nav--close")
		}), $("body").click(function(a) {
			var s = $(a.target);
			s.parents().is(i) || s.parents().is(t) || s.is(t) || ($(i).removeClass("active"), $(e).removeClass(
				"menuOn"), $(t).removeClass("mobile-nav--close").addClass("mobile-nav--open"))
		}), $(a).on("click", function(e) {
			e.preventDefault(), $(this).toggleClass("ad-plus-l ad-minus-l"), $(this).parent().next()
				.slideToggle()
		})
	}(), window.theme = window.theme || {}, theme.Cart = function() {
		var e = {
				edit: ".js-edit-toggle"
			},
			t = {
				showClass: "cart__update--show",
				showEditClass: "cart__edit--active",
				cartNoCookies: "cart--no-cookies"
			};

		function i(i) {
			this.$container = $(i), this.$edit = $(e.edit, this.$container), this.cookiesEnabled() || this.$container
				.addClass(t.cartNoCookies), this.$edit.on("click", this._onEditClick.bind(this)), $(
					"#shipping-calculator").length && Shopify.Cart.ShippingCalculator.show({
					submitButton: theme.strings.shippingCalcSubmitButton,
					submitButtonDisabled: theme.strings.shippingCalcSubmitButtonDisabled,
					customerIsLoggedIn: theme.strings.shippingCalcCustomerIsLoggedIn,
					moneyFormat: theme.strings.moneyFormat
				})
		}
		return i.prototype = _.assignIn({}, i.prototype, {
			onUnload: function() {
				this.$edit.off("click", this._onEditClick)
			},
			_onEditClick: function(e) {
				var i = $(e.target),
					a = $("." + i.data("target"));
				i.toggleClass(t.showEditClass), a.toggleClass(t.showClass)
			},
			cookiesEnabled: function() {
				var e = navigator.cookieEnabled;
				return e || (document.cookie = "testcookie", e = -1 !== document.cookie.indexOf(
					"testcookie")), e
			}
		}), i
	}(), window.theme = window.theme || {}, theme.Filters = function() {
		var e = "sort_by",
			t = {
				sortSelection: ".filters-toolbar__input--sort",
				defaultSort: ".collection-header__default-sort"
			};

		function i(e) {
			var i = this.$container = $(e);
			this.$sortSelect = $(t.sortSelection, i), this.$selects = $(t.filterSelection, i).add($(t.sortSelection,
				i)), this.defaultSort = this._getDefaultSortValue(), this._resizeSelect(this.$selects), this.$selects
				.removeClass("hidden"), this.$sortSelect.on("change", this._onSortChange.bind(this))
		}
		return i.prototype = _.assignIn({}, i.prototype, {
			_onSortChange: function(e) {
				var t = this._sortValue();
				t.length ? window.location.search = t : window.location.href = window.location.href.replace(
					window.location.search, ""), this._resizeSelect($(e.target))
			},
			_getSortValue: function() {
				return this.$sortSelect.val() || this.defaultSort
			},
			_getDefaultSortValue: function() {
				return $(t.defaultSort, this.$container).val()
			},
			_sortValue: function() {
				var t = this._getSortValue(),
					i = "";
				return t !== this.defaultSort && (i = e + "=" + t), i
			},
			_resizeSelect: function(e) {
				e.each(function() {
					var e = $(this),
						t = e.find("option:selected").text(),
						i = $("<span>").html(t);
					i.appendTo("body");
					var a = i.width();
					i.remove(), e.width(a + 10)
				})
			},
			onUnload: function() {
				this.$sortSelect.off("change", this._onSortChange)
			}
		}), i
	}(), window.theme = window.theme || {}, theme.HeaderSection = function() {
		return function() {
			theme.Search.init(), $(".site-header__cart").click(function(e) {
				e.preventDefault(), $("#header-cart").slideToggle()
			}), $("body").click(function(e) {
				var t = $(e.target);
				t.parents().is(".site-cart") || t.is(".site-cart") || $("body").find("#header-cart")
					.slideUp()
			})
		}
	}(), theme.Maps = function() {
		var e = 14,
			t = null,
			i = [],
			a = theme.mapKey ? theme.mapKey : "";

		function s(e) {
			this.$container = $(e), "loaded" === t ? this.createMap() : (i.push(this), "loading" !== t && (t =
				"loading", void 0 === window.google && $.getScript(
					"https://maps.googleapis.com/maps/api/js?key=" + a).then(function() {
					t = "loaded", $.each(i, function(e, t) {
						t.createMap()
					})
				})))
		}
		return s.prototype = _.assignIn({}, s.prototype, {
			createMap: function() {
				var t = this.$container.find(".map-section__container");
				return function(e) {
					var t = $.Deferred(),
						i = new google.maps.Geocoder,
						a = e.data("address-setting");
					return i.geocode({
						address: a
					}, function(e, i) {
						i !== google.maps.GeocoderStatus.OK && t.reject(i), t.resolve(e)
					}), t
				}(t).then(function(i) {
					var a = {
							zoom: e,
							center: i[0].geometry.location,
							disableDefaultUI: !0
						},
						s = this.map = new google.maps.Map(t[0], a),
						n = this.center = s.getCenter();
					new google.maps.Marker({
						map: s,
						position: s.getCenter()
					});
					google.maps.event.addDomListener(window, "resize", $.debounce(250, function() {
						google.maps.event.trigger(s, "resize"), s.setCenter(n)
					}))
				}.bind(this)).fail(function() {
					var e;
					switch (status) {
						case "ZERO_RESULTS":
							e = theme.strings.addressNoResults;
							break;
						case "OVER_QUERY_LIMIT":
							e = theme.strings.addressQueryLimit;
							break;
						default:
							e = theme.strings.addressError
					}
					t.parent().addClass("page-width map-section--load-error").html(
						'<div class="errors text-center">' + e + "</div>")
				})
			},
			onUnload: function() {
				google.maps.event.clearListeners(this.map, "resize")
			}
		}), s
	}(), theme.Product = function() {
		function e(e) {
			var t = this.$container = $(e),
				i = t.attr("data-section-id");
			this.settings = {
				mediaQueryMediumUp: "screen and (min-width: 750px)",
				mediaQuerySmall: "screen and (max-width: 749px)",
				bpSmall: !1,
				enableHistoryState: t.data("enable-history-state") || !1,
				imageSize: null,
				imageZoomSize: null,
				namespace: ".slideshow-" + i,
				sectionId: i,
				sliderActive: !1,
				zoomEnabled: !1
			}, this.selectors = {
				addToCart: "#AddToCart-" + i,
				addToCartText: "#AddToCartText-" + i,
				comparePrice: "#ComparePrice-" + i,
				originalPrice: "#ProductPrice-" + i,
				saveAmount: "#SaveAmount-" + i,
				discountBadge: ".discount-badge",
				SKU: ".variant-sku",
				originalPriceWrapper: ".product-price__price-" + i,
				originalSelectorId: "#ProductSelect-" + i,
				productFeaturedImage: ".FeaturedImage-" + i,
				productImageWrap: ".FeaturedImageZoom-" + i,
				productPrices: ".product-single__price-" + i,
				productThumbImages: ".product-single__thumbnail--" + i,
				productThumbs: ".product-single__thumbnails-" + i,
				saleClasses: "product-price__sale product-price__sale--single",
				saleLabel: ".product-price__sale-label-" + i,
				singleOptionSelector: ".single-option-selector-" + i
			}, thumbnails = $(".product-single__thumbnail-image"), thumbnails.length && thumbnails.bind("click",
				function() {
					var e = $(this).attr("src").split("?")[0].split("."),
						t = e.pop(),
						i = e.pop().replace(/_[a-zA-Z0-9@]+$/, ""),
						a = e.join(".") + "." + i + "." + t;
					void 0 !== variantImages[a] && productOptions.forEach(function(e, t) {
						optionValue = variantImages[a]["option-" + t], null !== optionValue && $(
							".single-option-selector:eq(" + t + ") option").filter(function() {
							return $(this).text() === optionValue
						}).length && ($(".swatch-" + t).find('.swatchInput[value="' + optionValue +
								'"]').prop("checked", !0), $(".single-option-selector:eq(" + t + ")")
							.val(optionValue).trigger("change"))
					})
				}), $(".popup-video").length && $(".popup-video").magnificPopup({
				type: "iframe",
				mainClass: "mfp-fade",
				removalDelay: 160,
				preloader: !1,
				fixedContentPos: !1
			});
			var a = $(".orderMsg").attr("data-user"),
				s = $(".orderMsg").attr("data-time");
			$(".orderMsg .items").text(Math.floor(Math.random() * a + 1)), $(".orderMsg .time").text(Math.floor(Math
				.random() * s + 5));
			var n = $(".userViewMsg").attr("data-user"),
				o = $(".userViewMsg").attr("data-time");
			$(".uersView").text(Math.floor(Math.random() * n)), setInterval(function() {
				$(".uersView").text(Math.floor(Math.random() * n))
			}, o), $(".stickyOptions .selectedOpt").on("click", function() {
				$(".stickyOptions ul").slideToggle("fast")
			}), $(".stickyOptions .vrOpt").on("click", function(e) {
				var t = $(this).attr("data-val"),
					a = $(this).attr("data-no"),
					s = $(this).text();
				$(".selectedOpt").text(s), $(".stickyCart .selectbox").val(t).trigger("change"), $(
						".stickyOptions ul").slideUp("fast"), this.productvariants = JSON.parse(document
						.getElementById("ProductJson-" + i).innerHTML), $(".stickyCart .product-featured-img")
					.attr("src", this.productvariants.variants[a].featured_image.src.replace(/(\.[^\.]*$|$)/,
						"_60x60$&"))
			});
			var r = this.tabs = "#ProductSection-" + i + " .tablink";
			$(r).on("click", function(e) {
				e.preventDefault(), $(this).parent().addClass("active"), $(this).parent().siblings()
					.removeClass("active");
				var t = $(this).attr("href");
				if ($(".tab-content").not(t).css("display", "none"), $(t).fadeIn(), $(window).width() < 767) {
					var i = $(this).offset();
					$("html, body").animate({
						scrollTop: i.top
					}, 700)
				}
			}), $(".product-tabs li:first-child").addClass("active"), $(
				".tab-container h3:first-child + .tab-content").show(), $(".reviewLink").on("click", function(e) {
				e.preventDefault();
				var t = $(this).attr("href");
				$(".product-tabs li").removeClass("active"), $(".tablink[href='" + t + "']").parent().addClass(
					"active"), $(".tab-content").not(t).css("display", "none"), $(t).fadeIn();
				var i = $(t).offset();
				$(window).width() < 767 ? $("html, body").animate({
					scrollTop: i.top - 50
				}, 700) : $("html, body").animate({
					scrollTop: i.top - 100
				}, 700)
			}), $(".readmore").on("click", function(e) {
				e.preventDefault();
				var t = $(this).attr("href");
				$(".product-tabs li").removeClass("active"), $(".tablink[href='" + t + "']").parent().addClass(
					"active"), $(".tab-content").not(t).css("display", "none"), $(t).fadeIn();
				var i = $(t).offset();
				$(window).width() < 767 ? $("html, body").animate({
					scrollTop: i.top - 50
				}, 700) : $("html, body").animate({
					scrollTop: i.top - 250
				}, 700)
			}), $(".sizelink,.emaillink").magnificPopup({
				type: "inline",
				midClick: !0
			});
			var c = t.find(".related-product"),
				l = $(c).attr("data-ds"),
				d = $(c).attr("data-tb"),
				h = $(c).attr("data-mb"),
				u = $(c).attr("proRelated-aniamtion"),
				p = $(c).attr("proRelated-timeout"),
				m = $(c).attr("proRelated-autoplay");
			$(".related-product .productSlider").slick({
				dots: !1,
				infinite: !0,
				slidesToShow: l,
				autoplaySpeed: p,
				speed: u,
				slidesToScroll: 1,
				autoplay: m,
				rtl: theme.rtl,
				responsive: [{
					breakpoint: 1024,
					settings: {
						slidesToShow: d,
						slidesToScroll: 1
					}
				}, {
					breakpoint: 767,
					settings: {
						slidesToShow: h,
						slidesToScroll: 1
					}
				}]
			}), $("#ProductJson-" + i).html() && (this.productSingleObject = JSON.parse(document.getElementById(
					"ProductJson-" + i).innerHTML), this.settings.zoomEnabled = $(this.selectors
					.productFeaturedImage).hasClass("js-zoom-enabled"), this.settings.imageSize = theme.Images
				.imageSize($(this.selectors.productFeaturedImage).attr("src")), this.settings.zoomEnabled && (this
					.settings.imageZoomSize = theme.Images.imageSize($(this.selectors.productImageWrap).data(
						"zoom"))), this._initBreakpoints(), this._stringOverrides(), this._initVariants(), this
				._initImageSwitch(), this._initmainimageSlider(), this._initThumbnailSlider(), this
				._setActiveThumbnail(), theme.Images.preload(this.productSingleObject.images, this.settings
					.imageSize))
		}

		function t(e) {
			var t = e.data("zoom");
			e.zoom({
				url: t
			})
		}

		function i(e) {
			e.trigger("zoom.destroy")
		}
		return e.prototype = _.assignIn({}, e.prototype, {
			_stringOverrides: function() {
				theme.productStrings = theme.productStrings || {}, $.extend(theme.strings, theme
					.productStrings)
			},
			_initBreakpoints: function() {
				var e = this;
				enquire.register(this.settings.mediaQuerySmall, {
					match: function() {
						e.settings.zoomEnabled && i($(e.selectors.productImageWrap)), e.settings
							.bpSmall = !0
					},
					unmatch: function() {
						e.settings.bpSmall = !1
					}
				}), enquire.register(this.settings.mediaQueryMediumUp, {
					match: function() {
						e.settings.zoomEnabled && $(e.selectors.productImageWrap).each(
						function() {
							t($(this))
						})
					}
				})
			},
			_initVariants: function() {
				var e = {
					$container: this.$container,
					enableHistoryState: this.$container.data("enable-history-state") || !1,
					singleOptionSelector: this.selectors.singleOptionSelector,
					originalSelectorId: this.selectors.originalSelectorId,
					product: this.productSingleObject
				};
				this.variants = new slate.Variants(e), this.$container.on("variantChange" + this.settings
						.namespace, this._updateAddToCart.bind(this)), this.$container.on(
						"variantImageChange" + this.settings.namespace, this._updateImages.bind(this)), this
					.$container.on("variantPriceChange" + this.settings.namespace, this._updatePrice.bind(
						this)), this.$container.on("variantSKUChange" + this.settings.namespace, this
						._updateSKU.bind(this))
			},
			_initImageSwitch: function() {
				if ($(this.selectors.productThumbImages).length) {
					var e = this;
					$(this.selectors.productThumbImages).on("click", function(t) {
						t.preventDefault();
						var i = $(this),
							a = i.attr("href"),
							s = i.data("zoom");
						e._switchImage(a, s), e._setActiveThumbnail(a)
					})
				}
			},
			_setActiveThumbnail: function(e) {
				void 0 === e && (e = $(this.selectors.productThumbImages + ".activeSlide").attr("href"));
				var t = $(this.selectors.productThumbImages + '[href="' + e + '"]');
				$(this.selectors.productThumbImages).removeClass("active-thumb"), t.addClass(
				"active-thumb");
				var i = t.parent().data("slide");
				$(".primgSlider").slick("slickGoTo", i)
			},
			_switchImage: function(e, a) {
				if ($(".index-section--featured-product").length) {
					$(this.selectors.productImageWrap + '[data-zoom="' + a + '"]').data("slide");
					$(this.selectors.productFeaturedImage).attr("src", e), this.settings.zoomEnabled && i($(
							this.selectors.productImageWrap)), !this.settings.bpSmall && this.settings
						.zoomEnabled && a && ($(this.selectors.productImageWrap).data("zoom", a), t($(this
							.selectors.productImageWrap)))
				}
			},
			_initmainimageSlider: function() {
				$(".primgSlider").slick({
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: !1,
					rtl: theme.rtl,
					fade: !0,
					asNavFor: this.selectors.productThumbs
				})
			},
			_initThumbnailSlider: function() {
				var e = !0,
					t = 5;
				"bottom" == $(this.selectors.productThumbs).attr("data-slider") && (e = !1, t = 6);
				var i = {
					verticalSwiping: !0,
					vertical: e,
					slidesToShow: t,
					slidesToScroll: 1,
					infinite: !1,
					rtl: theme.rtl,
					asNavFor: ".primgSlider",
					focusOnSelect: !0,
					responsive: [{
						breakpoint: 750,
						settings: {
							slidesToShow: 5,
							vertical: !1,
							swipe: !0,
							verticalSwiping: !1,
							draggable: !1
						}
					}, {
						breakpoint: 640,
						settings: {
							slidesToShow: 4,
							vertical: !1,
							swipe: !0,
							verticalSwiping: !1,
							draggable: !1
						}
					}]
				};
				$(this.selectors.productThumbs).slick(i), this.settings.sliderActive = !0
			},
			_destroyThumbnailSlider: function() {
				$(this.selectors.productThumbs).slick("unslick"), this.settings.sliderActive = !1
			},
			_updateAddToCart: function(e) {
				var t = e.variant;
				if (t)
					if ($(this.selectors.productPrices).removeClass("visibility-hidden").attr("aria-hidden",
							"true"), t.available) {
						$(this.selectors.addToCart).prop("disabled", !1), $(this.selectors.addToCartText)
							.text(theme.strings.addToCart), $(".instock").removeClass("hide"), $(
								".outstock").addClass("hide");
						var i = $("#pvr-" + t.id).text();
						$("#quantity_message").show().find(".items").text(i)
					} else $(this.selectors.addToCart).prop("disabled", !0), $(this.selectors.addToCartText)
						.text(theme.strings.soldOut), $(".instock").addClass("hide"), $(".outstock")
						.removeClass("hide"), $("#quantity_message").hide();
				else $(this.selectors.addToCart).prop("disabled", !0), $(this.selectors.addToCartText).text(
						theme.strings.unavailable), $(this.selectors.productPrices).addClass(
						"visibility-hidden").attr("aria-hidden", "false"), $(".instock").addClass("hide"),
					$(".outstock").removeClass("hide"), $("#quantity_message").hide()
			},
			_updateImages: function(e) {
				var t, i = e.variant,
					a = theme.Images.getSizedImageUrl(i.featured_image.src, this.settings.imageSize);
				this.settings.zoomEnabled && (t = theme.Images.getSizedImageUrl(i.featured_image.src, this
					.settings.imageZoomSize)), this._switchImage(a, t), this._setActiveThumbnail(a)
			},
			_updatePrice: function(e) {
				var t = e.variant;
				if ($(this.selectors.originalPrice).html(theme.Currency.formatMoney(t.price, theme
						.moneyFormat)), t.compare_at_price > t.price) {
					$(this.selectors.comparePrice).html(theme.Currency.formatMoney(t.compare_at_price, theme
							.moneyFormat)).removeClass("hide"), $(this.selectors.originalPriceWrapper)
						.addClass(this.selectors.saleClasses), $(this.selectors.saleLabel).removeClass(
							"hide");
					var i = t.compare_at_price - t.price;
					salecount = 100 * i / t.compare_at_price, $(this.selectors.discountBadge).find(".off")
						.find("span").text(+salecount.toFixed()), $(this.selectors.discountBadge)
						.removeClass("hide"), $(this.selectors.saveAmount).html(theme.Currency.formatMoney(
							i, theme.moneyFormat))
				} else $(this.selectors.comparePrice).addClass("hide"), $(this.selectors.saleLabel)
					.addClass("hide"), $(this.selectors.discountBadge).addClass("hide"), $(this.selectors
						.originalPriceWrapper).removeClass(this.selectors.saleClasses)
			},
			_updateSKU: function(e) {
				var t = e.variant;
				$(this.selectors.SKU).html(t.sku)
			},
			onUnload: function() {
				this.$container.off(this.settings.namespace)
			}
		}), e
	}(), theme.QuickView = void $("body").on("click", ".quick-view", function(e) {
		e.preventDefault(), e.stopPropagation(), $.ajax({
			beforeSend: function() {
				$("body").addClass("loading")
			},
			url: $(this).attr("href"),
			success: function(e) {
				$.magnificPopup.open({
					items: {
						src: '<div class="quick-view-popup" id="content_quickview">' + e +
							"</div>",
						type: "inline"
					},
					removalDelay: 500,
					callbacks: {
						beforeOpen: function() {
							this.st.mainClass = "mfp-move-horizontal"
						},
						close: function() {
							$("#content_quickview").empty()
						}
					}
				})
			},
			complete: function() {
				$("body").removeClass("loading")
			}
		})
	}), theme.Quotes = function() {
		var e = {
				mediaQuerySmall: "screen and (max-width: 749px)",
				mediaQueryMediumUp: "screen and (min-width: 750px)",
				slideCount: 0
			},
			t = {
				accessibility: !0,
				arrows: !0,
				dots: !1,
				autoplay: !1,
				rtl: theme.rtl,
				touchThreshold: 20,
				slidesToShow: 1,
				slidesToScroll: 1
			};

		function i(i) {
			var a = (this.$container = $(i)).attr("data-section-id"),
				s = this.wrapper = ".quotes-wrapper",
				n = this.slider = "#Quotes-" + a,
				o = $(n, s),
				r = !1,
				c = $.extend({}, t, {
					slidesToShow: 1,
					slidesToScroll: 1,
					adaptiveHeight: !0
				});

			function l(e, t) {
				r && (e.slick("unslick"), r = !1), e.slick(t), r = !0
			}
			e.slideCount = o.data("count"), e.slideCount < t.slidesToShow && (t.slidesToShow = e.slideCount, t
				.slidesToScroll = e.slideCount), o.on("init", this.a11y.bind(this)), enquire.register(e
				.mediaQuerySmall, {
					match: function() {
						l(o, c)
					}
				}), enquire.register(e.mediaQueryMediumUp, {
				match: function() {
					l(o, t)
				}
			})
		}
		return i.prototype = _.assignIn({}, i.prototype, {
			onUnload: function() {
				enquire.unregister(e.mediaQuerySmall), enquire.unregister(e.mediaQueryMediumUp), $(this
					.slider, this.wrapper).slick("unslick")
			},
			onBlockSelect: function(e) {
				var t = $(".quotes-slide--" + e.detail.blockId + ":not(.slick-cloned)").data("slick-index");
				$(this.slider, this.wrapper).slick("slickGoTo", t)
			},
			a11y: function(e, t) {
				var i = t.$list,
					a = $(this.wrapper, this.$container);
				i.removeAttr("aria-live"), a.on("focusin", function(e) {
					a.has(e.target).length && i.attr("aria-live", "polite")
				}), a.on("focusout", function(e) {
					a.has(e.target).length && i.removeAttr("aria-live")
				})
			}
		}), i
	}(), theme.slideshows = {}, theme.SlideshowSection = function() {
		return function(e) {
			var t = (this.$container = $(e)).attr("data-section-id"),
				i = this.slideshow = "#Slideshow-" + t;
			$(".slideshow__video", i).each(function() {
				var e = $(this);
				theme.SlideshowVideo.init(e), theme.SlideshowVideo.loadVideo(e.attr("id"))
			}), theme.slideshows[i] = new theme.Slideshow(i)
		}
	}(), theme.SlideshowSection.prototype = _.assignIn({}, theme.SlideshowSection.prototype, {
		onUnload: function() {
			delete theme.slideshows[this.slideshow]
		},
		onBlockSelect: function(e) {
			var t = $(this.slideshow),
				i = $(".slideshow__slide--" + e.detail.blockId + ":not(.slick-cloned)").data("slick-index");
			t.slick("slickGoTo", i).slick("slickPause")
		},
		onBlockDeselect: function() {
			$(this.slideshow).slick("slickPlay")
		}
	}), theme.collectionView = function() {
		return function(e) {
			var t = this.$container = $(e),
				i = t.attr("data-section-id");
			t.attr("data-section-timeout"), ajaxfilter = function(e) {
				var t, a, s = (a = {}, location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(e, t, i) {
					a[t] = i
				}), t ? a[t] : a);
				if (s = $.param(s)) {
					var n = e.split("?");
					e = n.join("?")
				}
				$.ajax({
					type: "GET",
					url: e,
					data: {},
					beforeSend: function() {
						$("body").addClass("loading hideOverly")
					},
					complete: function(t) {
						$(".productList .grid-products").html($(".productList .grid-products", t
								.responseText).html()), $(".productList .grid-products").html($(
								".productList .grid-products", t.responseText).html()), $(
								".filters-toolbar__product-count").html($(
								".filters-toolbar__product-count", t.responseText).html()), $(
								".sidebar_tags").html($(".sidebar_tags", t.responseText).html()),
							check_filters(), $(".pagination").html($(".pagination", t.responseText)
								.html()), $(".pagination", t.responseText).html() ? $(".pagination")
							.show() : $(".pagination").hide(), $(".infinitpaginOuter").html($(
								".infinitpaginOuter", t.responseText).html()), $(
								".infinitpaginOuter", t.responseText).html() || $(".infinitpagin")
							.remove(), theme.mlcurrency && Currency.convertAll(shopCurrency, $(
								"#currencies li.selected").attr("data-currency")), $("body")
							.removeClass("loading hideOverly"), $(theme.ajaxCart), loadMoreBtn(), $(
								".spr-badge").length > 0 && $.getScript(window.location.protocol +
								"//productreviews.shopifycdn.com/assets/v4/spr.js"),
							productGridView(i), setTimeout(function() {
								productGridView(i)
							}, 1e3), history.pushState({
								page: e
							}, e, e)
					}
				})
			}, $(window).on("load delayed-resize", function(e, t) {
				productGridView(i)
			}), infiniteScroll = function() {
				$(window).on("scroll load delayed-resize", function() {
					var e = $(".infinitpagin a").attr("href");
					if ($(".infinitpagin a.infinite").length) {
						var t = $(".infinitpagin").offset();
						$(document).scrollTop() + $(window).height() - 50 > t.top && ($(window).off(
							"scroll load delayed-resize"), loadMore(e))
					}
				})
			}, loadMoreBtn = function() {
				$(".infinitpagin a.loadMore").click(function(e) {
					e.preventDefault();
					var t = $(this).attr("href");
					loadMore(t)
				})
			}, loadMore = function(e) {
				e.length && $.ajax({
					type: "GET",
					dataType: "html",
					url: e,
					beforeSend: function() {
						"button" == $(".infinitpaginOuter").attr("data-type") ? $("body").addClass(
							"loading hideOverly") : $(".infinitpagin a").show()
					},
					complete: function(e) {
						$(".productList .grid-products").length ? $(".productList .grid-products")
							.append($(".productList .grid-products", e.responseText).html()) : $(
								".productList .list-view-items").append($(
								".productList .list-view-items", e.responseText).html()), $(
								".infinitpagin", e.responseText).html() ? $(".infinitpagin").html($(
								".infinitpagin", e.responseText).html()) : $(".infinitpagin")
							.remove(), theme.mlcurrency && Currency.convertAll(shopCurrency, $(
								"#currencies li.selected").attr("data-currency")), $(theme
							.ajaxCart), productGridView(i), $(".spr-badge").length > 0 && $
							.getScript(window.location.protocol +
								"//productreviews.shopifycdn.com/assets/v4/spr.js"), setTimeout(
								function() {
									productGridView(i)
								}, 1e3), $(".infinitpagin a.loadMore").length ? loadMoreBtn() :
							infiniteScroll(), $("body").removeClass("loading hideOverly")
					}
				})
			}, $(document).ready(function() {
				infiniteScroll(), loadMoreBtn()
			})
		}
	}(), theme.carouselSection = {}, theme.carouselSection = function() {
		return function(e) {
			var t = this.$container = $(e),
				i = t.attr("data-section-id"),
				a = $(t).find(".carousel"),
				s = this.tabs = "#carousel-" + i + " .tablink",
				n = this.tabcontent = "#carousel-" + i + " .tab-content";
			if ($(a).slick(), $(s).on("click", function(e) {
					e.preventDefault(), $(this).parent().addClass("active"), $(this).parent().siblings()
						.removeClass("active");
					var t = $(this).attr("href");
					$(n).not(t).css("display", "none"), $(t).fadeIn(), $(a).slick("refresh")
				}), $("#carousel-" + i).each(function() {
					$(this).find(".collection-tabs li:first-child").addClass("active"), $(this).find(
						".tab-container h3:first-child + .tab-content").show(), $(this).find(
						".collection-tabs li:first-child a").attr("href"), $(a).slick("refresh")
				}), "product-recommendations" == i) {
				var o = t.data("baseUrl"),
					r = t.data("productId"),
					c = o + "?section_id=product-recommendations&limit=" + t.data("limit") + "&product_id=" + r +
					" .product-recommendations";
				t.parent().load(c), $("#productSlider-product-recommendations").slick(), setTimeout(function() {
					$("#productSlider-product-recommendations").slick()
				}, 1500)
			}
			$(window).on("load delayed-resize", function(e, t) {
				productGridView("carousel-" + i)
			})
		}
	}(), theme.masonary = function() {
		return function(e) {
			var t = this.$container = $(e),
				i = t.attr("data-section-id"),
				a = this.masonary = $(t).find(".grid-masonary");

			function s(e) {
				$(e).masonry({
					columnWidth: ".grid-sizer-" + i,
					itemSelector: ".ms-item",
					percentPosition: !0
				})
			}
			s(a), setTimeout(function() {
				s(a)
			}, 1e3)
		}
	}(), theme.instagram = {}, theme.instagramSection = function() {
		return function(e) {
			var t = this.$container = $(e),
				i = t.attr("data-section-id"),
				a = t.attr("data-act"),
				s = t.attr("data-count"),
				n = t.attr("data-ds"),
				o = t.attr("data-tb"),
				r = t.attr("data-mb"),
				c =
				"https://graph.instagram.com/me/media?fields=comments_count,like_count,id,media_type,media_url,permalink,thumbnail_url,caption,children&access_token=" +
				a;
			$.ajax({
				url: c,
				type: "GET",
				dataType: "json",
				success: function(e) {
					var t = e.data,
						a = "#instafeed" + i,
						c = "";
					$.each(t, function(e, t) {
						if (e >= s) return 0;
						var i = t.thumbnail_url || t.media_url;
						c += '<div class="insta-img ' + n + " " + o + " " + r +
							'"><a rel="nofollow" class="instagram-" href="' + t.permalink +
							'" target="_blank"><img data-src="' + i +
							'" alt="" class="lazyload" /></a></div>'
					}), $(a).html(c), $("#instafeed" + i + ".carousel").slick()
				}
			})
		}
	}(), theme.ajaxCart = function() {
		var e = null;

		function t(e, t) {
			$("body").addClass("loading"), CartJS.addItem(e, t, {}, {
				success: function(e, t, a) {
					$("#successDrawer").find(".modal-prod-img").attr("src", "");
					var s = e.image.replace(/(\.[^\.]*$|$)/, "_small$&");
					$("#successDrawer").find(".modal-prod-img").attr("src", s), $("#successDrawer").find(
						".modal-prod-name").text(e.product_title), $("body").removeClass(
						"loading showOverly"), i("#successDrawer")
				},
				error: function(e, t, a) {
					var s = JSON.parse(e.responseText).description;
					$("body").removeClass("loading"), $(".error-message").text(s), i("#errorDrawer")
				}
			})
		}

		function i(t) {
			$("body").addClass("loading showOverly"), $(t).fadeIn(500), e = setTimeout(function() {
				$("body").removeClass("loading showOverly"), $(t).fadeOut(200)
			}, 8e3)
		}

		function a() {
			var e = $(".freeShipMsg").attr("data-price") + "00";
			cartTotal = CartJS.cart.total_price, remainfreeship = e - cartTotal, remainfreeship > 0 ? ($(
					"#freeShipMsg .freeShip").html(theme.Currency.formatMoney(remainfreeship, theme.moneyFormat)),
				$("#freeShipMsg").show().next().hide(), theme.mlcurrency && currenciesChange(
					"#freeShipMsg .freeShip sapn.money")) : $("#freeShipMsg").hide().next().show()
		}

		function s() {
			try {
				if (null != $.cookie("wishlistList") && "__" != $.cookie("wishlistList") && "" != $.cookie(
						"wishlistList"))
					for (var e = String($.cookie("wishlistList")).split("__"), t = 0; t < e.length; t++) "" != e[t] && (
						$('.wishlist[rel="' + e[t] + '"]').removeClass("add-to-wishlist").attr("title", theme
							.wlAvailable).find("span").text(theme.wlAvailable), $('.wishlist[rel="' + e[t] +
							'"] .ad').removeClass("ad-heart-l").addClass("ad-heart"))
			} catch (e) {}
		}
		$(document).on("click", ".quickShop", function(e) {
			e.preventDefault(), e.stopImmediatePropagation();
			var t = $(this).attr("href"),
				i = $(this).parents(".grid-view-item").find(".grid-view_image"),
				a = $(this).parents(".grid-view-item").find(".shopWrapper");
			$.ajax({
				url: t,
				dataType: "html",
				type: "GET",
				beforeSend: function() {
					$(i).addClass("showLoading")
				},
				success: function(e) {
					$(".shopWrapper").removeClass("active").html(""), $(a).addClass("active").html(
						e)
				},
				complete: function(e) {
					$(i).removeClass("showLoading"), theme.mlcurrency && currenciesChange(
						".shopWrapper.active .product-single__price span.money"), $(
						".closeShop").click(function() {
						$(this).parents(".shopWrapper").removeClass("active")
					})
				}
			})
		}), $(".add-to-cart").click(function(e) {
			if (e.preventDefault(), e.stopImmediatePropagation(), theme.ajax_cart) {
				var i = $(this).attr("id"),
					a = $(this).attr("rel");
				t(i, a)
			} else $(this).parent().submit()
		}), $(".product-form__cart-submit").click(function(e) {
			if (e.preventDefault(), "disabled" != $(this).attr("disabled"))
				if (theme.ajax_cart) {
					var i = $(this).closest("form").find("select[name=id]").val();
					i || (i = $(this).closest("form").find("input[name=id]").val());
					var a = $(this).closest("form").find("input[name=quantity]").val();
					a || (a = 1);
					var s = {};
					$('[name*="properties"]').each(function() {
						var e = $(this).attr("name").split("[")[1].split("]")[0],
							t = $(this).val();
						s[e] = t
					}), t(i, a)
				} else $(".productForm").submit()
		}), $(document).on("click", ".add-to-wishlist", function(e) {
			e.preventDefault();
			var t = $(this).attr("rel");
			if (null == $.cookie("wishlistList")) var i = t;
			else if (-1 == $.cookie("wishlistList").indexOf(t)) var i = $.cookie("wishlistList") + "__" + t;
			$.cookie("wishlistList", i, {
					expires: 30,
					path: "/"
				}), $(this).find(".ad").removeClass("ad-heart-l").addClass("ad-circle-notch-r ad-spin"),
				setTimeout(function() {
					$('.wishlist[rel="' + t + '"]').removeClass("add-to-wishlist").find("span").text(theme
						.wlAvailable), $('.wishlist[rel="' + t + '"] .ad').removeClass(
						"ad-circle-notch-r ad-spin").addClass("ad-heart"), s()
				}, 2e3)
		}), s(), a(), $(".continue-shopping, .modalOverly, .closeDrawer").click(function() {
			clearTimeout(e), $(".modal").fadeOut(200), $("body").removeClass("loading showOverly")
		}), $(document).on("cart.requestComplete", function(e, t) {
			setTimeout(function() {
				a()
			}, 3e3)
		})
	}, $(theme.ajaxCart), $(document).ready(function() {
		var e = new theme.Sections;
		e.register("cart-template", theme.Cart), e.register("product", theme.Product), e.register(
				"collection-template", theme.Filters), e.register("collection-template", theme.collectionView), e
			.register("product-template", theme.Product), e.register("header-section", theme.HeaderSection), e
			.register("map", theme.Maps), e.register("slideshow-section", theme.SlideshowSection), e.register(
				"carousel", theme.carouselSection), e.register("masonary", theme.masonary), e.register("quotes",
				theme.Quotes), e.register("instagram", theme.instagramSection), $(".selected-currency").click(
				function() {
					$("#currencies").slideToggle()
				}), $(window).width() > 999 && $("#currencies li").click(function() {
				$(this).parent().slideUp()
			}), setTimeout(function() {
				var e = $(".weglot-link--active").html();
				$(".weglot-link--active").length && $(".language-picker .ttl").html(e)
			}, 1e3), $(".language-picker .ttl").click(function() {
				$("#language").slideToggle()
			}), $("#language li a").click(function() {
				$(".language-picker .ttl").html($(this).html()), $(window).width() > 999 && $("#language")
					.slideUp()
			}), $(document).on("click", ".gridSwatches li:not(.noImg)", function(e) {
				var t = $(this),
					i = $(this).attr("rel"),
					a = $(this).parents(".grid-view-item").find(".grid-view-item__link");
				$(a).addClass("showLoading"), $(a).find(".variantImg").css("background-image", "url('" + i +
					"')");
				var s = document.createElement("img");
				return s.src = i, s.onload = function() {
					$(a).removeClass("showLoading").addClass("showVariantImg"), t.siblings().removeClass(
						"active"), t.addClass("active")
				}, !1
			})
	}), $(window).resize(function() {
		clearTimeout(resizeTimer), resizeTimer = setTimeout(function() {
			$(window).trigger("delayed-resize")
		}, 250)
	}), theme.init = function() {
		theme.customerTemplates.init(), slate.rte.wrapTable(), slate.rte.iframeReset(), slate.a11y.pageLinkFocus($(
			window.location.hash)), $(".in-page-link").on("click", function(e) {
			slate.a11y.pageLinkFocus($(e.currentTarget.hash))
		}), $('a[href="#"]').on("click", function(e) {
			e.preventDefault()
		})
	}, $(theme.init),
	function(e) {
		e(document).ready(function() {
			e(".saleTime").each(function() {
				var t = e(this),
					i = e(this).data("date"),
					a = new Date(i).getTime(),
					s = setInterval(function() {
						var i = (new Date).getTime(),
							n = a - i,
							o = Math.floor(n / 864e5),
							r = Math.floor(n % 864e5 / 36e5),
							c = Math.floor(n % 36e5 / 6e4),
							l = Math.floor(n % 6e4 / 1e3);
						o = ("00" + o).substr(o > 99 ? -3 : -2), r = ("00" + r).substr(-2), c = (
							"00" + c).substr(-2), l = ("00" + l).substr(-2), e(t).find(
							".counter").html("<span class='days'>" + o +
							"<span>Days</span></span> <span class='hours'>" + r +
							"<span>HR</span></span> <span class='minutes'>" + c +
							"<span>MIN</span></span> <span class='seconds'>" + l +
							"<span>SC</span>"), n < 0 && (clearInterval(s), e(t).hide())
					}, 1e3)
			}), e(".qtyBtn").on("click", function() {
				var t = e(this).parent(".qtyField"),
					i = e(t).find(".qty").val(),
					a = 1;
				e(this).is(".plus") ? a = parseInt(i) + 1 : i > 1 && (a = parseInt(i) - 1), e(t).find(
					".qty").val(a)
			})
		}), e(window).resize(function() {}), e(".btn-filter").click(function() {
			e(".filterbar").toggleClass("active")
		}), e(".closeFilter").click(function() {
			e(".filterbar").removeClass("active")
		}), e("body").click(function(t) {
			var i = e(t.target);
			i.parents().is(".filterbar") || i.is(".btn-filter") || e(".filterbar").removeClass("active")
		}), e(".product-tags li").eq(10).nextAll().hide(), e(".btnview").click(function() {
			e(".product-tags li").not(".filter--active").show(), e(this).hide()
		});
		new Date;
		if (e("#newsletter-modal").length) {
			var t = e("#newsletter-modal").attr("data-type"),
				i = e("#newsletter-modal").attr("data-time");
			if ("pageload" == t) "true" != e.cookie("cookieSignup") && setTimeout(function() {
				e(".newsletter-wrap").fadeIn(), e("#newsletter-modal").fadeIn()
			}, i);
			else {
				e(document).on("mouseleave", function(t) {
					t.clientY < 0 && "true" != e.cookie("cookieSignup") && (e(".newsletter-wrap").fadeIn(), e(
						"#newsletter-modal").fadeIn())
				})
			}
		}(e(document).mouseup(function(t) {
			var i = e("#newsletter-modal");
			i.is(t.target) || 0 !== i.has(t.target).length || (i.hide(), e(".newsletter-wrap").fadeOut())
		}), e(".closepopup").click(function() {
			1 == e("#dontshow").prop("checked") && e.cookie("cookieSignup", "true", {
				expires: 1,
				path: "/"
			}), e("#newsletter-modal").fadeOut(), e(".newsletter-wrap").fadeOut()
		}), e(".notification-bar").length && "true" != e.cookie("promotion") && e(".notification-bar").show(), e(
			".btn-shop").click(function() {
			e(".products .grid-lb").not(e(this).next()).removeClass("active"), e(this).next().addClass("active")
		}), e(".btn-shop-close").click(function() {
			e(this).parent().removeClass("active")
		}), e(".close-announcement").click(function() {
			e(".notification-bar").slideUp(), e.cookie("promotion", "true", {
				expires: 1
			})
		}), e(".cookiePopup").length && ("accepted" != e.cookie("accept-cookies") && setTimeout(function() {
			e(".cookiePopup").slideDown()
		}, 250), e(".cookieBtn").click(function() {
			e(".cookiePopup").slideUp(), e.cookie("accept-cookies", "accepted", {
				expires: 15,
				path: "/"
			})
		})), e(".user-menu").click(function() {
			e(".customer-links").slideToggle()
		}), e("body").click(function(t) {
			var i = e(t.target);
			i.parents().is(".my-account") || i.is(".my-account") || e("body").find(
				".my-account .customer-links").slideUp()
		}), window.onscroll = function() {
			! function() {
				if (theme.fixedHeader) {
					var t = e(".header-wrap").height();
					e(window).scrollTop() > 145 ? e(".header-wrap").addClass("stickyNav animated fadeInDown")
						.parent("#shopify-section-header").height(t) : e(".header-wrap").removeClass(
							"stickyNav fadeInDown").parent("#shopify-section-header").height("")
				}
				if (e("#AddToCart-product-template").length) {
					var i = e("#AddToCart-product-template").offset();
					e(window).scrollTop() > i.top ? e(".stickyCart").slideDown() : e(".stickyCart").slideUp()
				}
			}()
		}, e(".footer-links .h4").click(function() {
			e(window).width() < 750 && (e(this).find(".ad").toggleClass("ad-angle-down-l ad-angle-up-l"), e(
				this).next().slideToggle())
		}), e("#site-scroll").click(function() {
			return e("html, body").animate({
				scrollTop: 0
			}, 1e3), !1
		}), e(window).scroll(function() {
			e(this).scrollTop() > 300 ? e("#site-scroll").fadeIn() : e("#site-scroll").fadeOut()
		}), e(".hero.parallax").length) && ((!!!/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator
			.userAgent) || e(window).width() > 767) && e.stellar({
			horizontalScrolling: !1,
			verticalOffset: 40
		}))
	}(jQuery);
